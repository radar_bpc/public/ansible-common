#!/usr/bin/python

from ansible.errors import AnsibleFilterError


def should_delete_user(user, project, group_names, inventory_hostname):
    try:
        if 'environments' in user.get('team_group', {}) and project in user['team_group']['environments']:
            if "all" in user['team_group']['environments'][project]:
                return False
            if len(set(user['team_group']['environments'][project]) & set(group_names)) > 0:
                return False
            if inventory_hostname in user['team_group']['environments'][project]:
                return False

        if 'environments' in user.get('inventory_groups', {}) and project in user['inventory_groups']['environments']:
            if "all" in user['inventory_groups']['environments'][project]:
                return False
            if len(set(user['inventory_groups']['environments'][project]) & set(group_names)) > 0:
                return False

        if inventory_hostname in user.get('inventory_hosts', []):
            return False

        for item in user.get('inventory_hosts', []):
            if isinstance(item, dict) and inventory_hostname in item.keys():
                return False

        return True

    except Exception as e:
        raise AnsibleFilterError(f"Error in should_delete_user filter: {str(e)}")


def should_create_user(user, project, group_names, inventory_hostname):
    try:
        if 'environments' in user.get('team_group', {}) and project in user['team_group']['environments']:
            if "all" in user['team_group']['environments'][project]:
                return True
            if len(set(user['team_group']['environments'][project]) & set(group_names)) > 0:
                return True
            if inventory_hostname in user['team_group']['environments'][project]:
                return True

        if 'environments' in user.get('inventory_groups', {}) and project in user['inventory_groups']['environments']:
            if "all" in user['inventory_groups']['environments'][project]:
                return True
            if len(set(user['inventory_groups']['environments'][project]) & set(group_names)) > 0:
                return True

        if inventory_hostname in user.get('inventory_hosts', []):
            return True

        for item in user.get('inventory_hosts', []):
            if isinstance(item, dict) and inventory_hostname in item.keys():
                return True

        return False

    except Exception as e:
        raise AnsibleFilterError(f"Error in should_create_user filter: {str(e)}")


def should_have_sudo(user, group_names, inventory_hostname):
    try:
        if 'bastion' in group_names:
            return False
        
        if 'sudo_rights' in user.get('team_group', {}) and len(user['team_group']['sudo_rights']) > 0:
            return True

        for item in user.get('inventory_hosts', []):
            if isinstance(item, dict) and inventory_hostname in item.keys():
                return True
            
        return False

    except Exception as e:
        raise AnsibleFilterError(f"Error in should_have_sudo filter: {str(e)}")


class FilterModule(object):
    def filters(self):
        return {
            'list_from_dict_keys': lambda x, y: [i[y] for i in x],
            'should_delete_user': should_delete_user,
            'should_create_user': should_create_user,
            'should_have_sudo': should_have_sudo,
        }
