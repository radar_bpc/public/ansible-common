# About
The main purpose of this role is to provide full access to automatic user configuration and group policy, which includes

- groups management
- users management
- user secrets management
- user permissions management


The idea is to combine all aspects of a particular user's configuration into variables in user profile files. And, in the long run, generate this file using some external engine, such as a web interface.

Have a look at **defaults/main.yml** to see all the variables needed for the configuration and their respective values.

Required variables in user profiles:
- `users_main_list.user_name.name`
- `users_main_list.user_name.linux_groups` (If no one, then user will be removed from any groups on the host.)

The presence of a user on a host is formed from three factors combined by the conjunction OR: 
- the user has a list of variables `users_main_list.user_name.inventory_groups` that define the groups of hosts on which the user should have an account
- the user has a list of variables `users_main_list.user_name.inventory_hosts` which specifies the exact hosts the user should have an account on
- the user is in the global list of users `users.names` - this is the default way to assign users to hosts

Files, limits, and sudo permissions are defined through the appropriate lists, see **defaults/main.yml**.

# The special role of the global variable users
The `users` variable is checked along with lists from the user profile. But the "users" variable is a global variable that is set by group variables in inventories. It has a simple structure like:
```
users:
    names:
        - kryukov
        - petrikovskii_pa
```

To inherit this variable from project variables, you can use the following pattern:

vars_project_all
```
c02_tst_global_users: "{{ team_sysadmins + team_network + team_dba + team_devops + team_impl + team_tools }}"
c02_tst_special_users: "{{ team_sysadmins + team_network }}"

team_sysadmins:
    - sergei
    - ertan
team_network:
    - granit
team_dba:
    - dmitriy
team_devops:
    - pavel
    - evgeni
team_impl:
    - patricia
team_tools:
    - zabbix
    - filebeat
team_weblogic:
    - weblogic
```

examle group.yml for databases:
```
users:
    names: "{{ team_dba }}"
```

example group.yml for network tools hosts:
```
users:
    names: "{{ c02_tst_special_users }}"
```

example group.yml for all hosts:
```
users:
    names: "{{ c02_tst_global_users }}"
```

example group.yml for host with one special user:
```
users:
    names: "{{ c02_tst_global_users + team_weblogic }}" 
```

# Usage with the playbook

Example playbook
```
---
- name: Users and group creation
  hosts: all
  become: yes
  gather_facts: no
  roles:
    - users_and_groups
```

Manage single user with `add` tag and `useradd` variable:
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/users_and_groups.yml -bK --tags add -e "useradd=kryukov"
```

Manage list of users:
```
ap playbooks/users_and_groups.yml -i inventories/oci02 -l c02t1-ptrk01 -t add -e "userlist=['melentiev_ol','kivva_ma','slashchev_ig','babloyan_mi']" -bK
```

Manage all users which present in `vars_project_all/defaults/main/users/profiles` catalog:
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/users_and_groups.yml -bK --tags add -e usersall=true
```

Remove single user with tag `remove_one` and `userdel` variable:
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/users_and_groups.yml -bK --tags remove_one -e "userdel=kryukov"
```

Remove all users from remove list `users_remove_list`:
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/users_and_groups.yml -bK --tags remove
```

For further development: 
- We need to consider working with database access, restarting the sshd service, and adding to the sshd configuration file.
- Separation of user rights for different host groups.