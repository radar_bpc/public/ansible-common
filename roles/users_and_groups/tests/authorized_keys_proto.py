import re
import sys
import logging


logger = logging.getLogger(__name__)
def debug(msg, *args, **kwargs):
    logger.debug(msg, *args, **kwargs)
console_handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(levelname)s: %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

# logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

def ssh_key_set(user, project, instance, group_names, authorized_keys_content):
    user_authorized_keys = []
    if user["authorized_keys"] and type(user["authorized_keys"]) is dict:
        key_list = []
        # authorized_keys may be a list in simple case,
        # if it's a dictionary we process it here
        debug("authorized_keys %s", user["authorized_keys"])
        for sub_key, _ in user["authorized_keys"].items():
            # the keys may consist of multiple values separated
            # by commas, so we form a list to process them
            for s in sub_key.split(','):
                key_list.append(s.strip())
        debug("key_list %s", key_list)
        debug("key_list_base %s", user["authorized_keys"].keys())
        
        debug("group_names %s", group_names)
        combined_group_names = []
        for group_name in group_names:
            combined_wide_group = project + '_' + group_name
            combined_group_names.append(combined_wide_group)
            combined_group_names.append(group_name)
        combined_group_names.append(project)
        combined_group_names.append(instance)
        combined_group_names.append(project + '_' + instance)
        debug("combined_group_names %s", combined_group_names)
            
        overlapped_group_names = []
        for group_name in combined_group_names:
            if group_name in key_list:
                overlapped_group_names.append(group_name)
        debug("overlapped_group_names %s", overlapped_group_names)
        
        preprocessed_app_group = []
        for group_name in group_names:
            # in jinja: group_name | regex_replace('^[0-9a-z]+_', '', 1)
            group_name = re.sub(r'^[0-9a-z]+_', '', group_name, count=1)
            preprocessed_app_group.append(group_name)
        debug("preprocessed_app_group %s", set(preprocessed_app_group))

        app_group = []
        # in jinja we will do "sort | unique" here instead of `set()`
        for group_name in sorted(set(preprocessed_app_group)):
            if group_name in group_names:
                app_group.append(group_name)
        debug("app_group %s", app_group)
        
        narrow_group = []
        middle_group_vertical = []
        middle_group_horizontal = []
        wide_group = []
        massive_group = []
        for element in app_group:
            narrow_group.append(project + '_' + instance + '_' + element)
            middle_group_vertical.append(project + '_' + instance)
            middle_group_horizontal.append(project + '_' + element)
            wide_group.append(instance + '_' + element)
            massive_group.append(instance)
        narrow_group = set(narrow_group)
        middle_group_vertical = set(middle_group_vertical)
        middle_group_horizontal = set(middle_group_horizontal)
        wide_group = set(wide_group)
        massive_group = set(massive_group)

        debug("narrow_group %s", narrow_group)
        debug("middle_group_vertical %s", middle_group_vertical)
        debug("middle_group_horizontal %s", middle_group_horizontal)
        debug("wide_group %s", wide_group)
        debug("massive_group %s", massive_group)

        common_narrow = list(narrow_group.intersection(overlapped_group_names))
        common_middle_vertical = list(middle_group_vertical.intersection(overlapped_group_names))
        common_middle_horizontal = list(middle_group_horizontal.intersection(overlapped_group_names))
        common_wide = list(wide_group.intersection(overlapped_group_names))
        common_massive = list(massive_group.intersection(overlapped_group_names))
        
        debug("common_narrow %s", common_narrow)
        debug("common_middle_vertical %s", common_middle_vertical)
        debug("common_middle_horizontal %s", common_middle_horizontal)
        debug("common_wide %s", common_wide)
        debug("common_massive %s", common_massive)
        
        if len(common_narrow) > 0:
            debug("hit narrow_group %s", narrow_group)
            for narrow_group in common_narrow:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if narrow_group == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        elif len(common_middle_vertical) > 0:
            debug("hit middle_group_vertical %s", middle_group_vertical)
            for middle_group_vertical in common_middle_vertical:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if middle_group_vertical == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        elif len(common_middle_horizontal) > 0:
            debug("hit middle_group_horizontal %s", middle_group_horizontal)
            for middle_group_horizontal in common_middle_horizontal:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if middle_group_horizontal == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        elif len(common_wide) > 0:
            debug("hit wide_group %s", wide_group)
            for wide_group in common_wide:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if wide_group == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        elif len(common_massive) > 0:
            debug("hit massive_group %s", massive_group)
            for massive_group in common_massive:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if massive_group == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        elif len(app_group) > 0:
            debug("hit app_group %s", app_group)
            for group in app_group:
                for keyword_list in user["authorized_keys"].keys():
                    for keyword in keyword_list.split(','):
                        if group == keyword.strip():
                            user_authorized_keys.extend(user["authorized_keys"][keyword_list])
        
        if len(user_authorized_keys) == 0:
            # saving privious ssh authorized_keys file content;
            # without it we will destroy any content which is not stored
            # in ansible configuration
            if authorized_keys_content and len(authorized_keys_content) > 0:
                file_lines = authorized_keys_content.split('\n')
                user_authorized_keys.extend(file_lines)
            else:
                user_authorized_keys.extend(["default_key"])

    else:
        user_authorized_keys.extend(user["authorized_keys"])
        
    for entry in user_authorized_keys:
        # in jinja this entry will be writed into a file
        debug("entry is %s", entry)
        pass
    
    return entry # only for tests


user = {"authorized_keys": {
    "exonprem_fe_app": ["key1"],
    "europe_fe_app": ["key2"],
    "radar_fe_app, jenkins": ["key3"],
    "fe_app": ["key4"],
    "tst_fe_app": ["key5"],
    "dev_fe_app": ["key6"],
    "uat_sftp": ["key7"],
    "exonprem_dev_gui_app": ["key8"],
    "exonprem_tst_gui_app": ["key9"],
    "apac_dev_gui_app": ["key10"],
    "apac_tst_gui_app": ["key11"],
    "dev": ["key12"],
    "ips_prod, ips_tst": ["key13"],
    "dev_fe_app": ["key14"],
    "radar_dev_fe_app": ["key15"],
}}

def test_case_1():
    print("#### TEST 1 ####")
    project = "radar"
    instance = "tst"
    group_names = ["fe_app", "tst_app", "tst_environment", "tst_fe_app"]
    authorized_keys_content = None
    assert "key3" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_2():
    print("#### TEST 2 ####")
    project = "radar"
    instance = "tst"
    group_names = ["jenkins", "tst_jenkins", "tst_environment"]
    authorized_keys_content = None
    assert "key3" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_3():
    print("#### TEST 3 ####")
    project = "exonprem"
    instance = "tst"
    group_names = ["fe_app", "oracle_client", "tst_app", "tst_environment", "tst_fe_app"]
    authorized_keys_content = None
    assert "key1" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_4():
    print("#### TEST 4 ####")
    project = "nala"
    instance = "uat"
    group_names = ["sftp", "uat_environment", "uat_sftp", "uat_support"]
    authorized_keys_content = None
    assert "key7" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
   
def test_case_5():
    print("#### TEST 5 ####")
    project = "radar"
    instance = "tst"
    group_names = ["tst_environment", "tst_jenkins", "tst_support"]
    authorized_keys_content = None
    assert "default_key" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_6():
    print("#### TEST 6 ####")
    project = "radar"
    instance = "tst"
    group_names = ["jenkins", "tst_environment", "tst_jenkins", "tst_support"]
    authorized_keys_content = None
    assert "key3" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
    
def test_case_7():
    print("#### TEST 7 ####")
    project = "exonprem"
    instance = "dev"
    group_names = ["gui_app", "dev_gui_app"]
    authorized_keys_content = None
    assert "key8" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
    
def test_case_8():
    print("#### TEST 8 ####")
    project = "exonprem"
    instance = "tst"
    group_names = ["gui_app", "tst_gui_app"]
    authorized_keys_content = None
    assert "key9" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
    
def test_case_9():
    print("#### TEST 9 ####")
    project = "apac"
    instance = "dev"
    group_names = ["gui_app", "dev_gui_app"]
    authorized_keys_content = None
    assert "key10" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
    
def test_case_10():
    print("#### TEST 10 ####")
    project = "apac"
    instance = "tst"
    group_names = ["gui_app", "tst_gui_app"]
    authorized_keys_content = None
    assert "key11" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_11():
    print("#### TEST 11 ####")
    project = "radar"
    instance = "dev"
    group_names = [ "bus_app",
                    "camel_app",
                    "dev_app",
                    "dev_bus_app",
                    "dev_camel_app",
                    "dev_environment",
                    "dev_fm_app",
                    "dev_gate_app",
                    "dev_gui_app",
                    "dev_java",
                    "dev_svip",
                    "dev_svip_backend",
                    "dev_svip_frontend",
                    "dev_svip_general",
                    "dev_weblogic",
                    "fm_app",
                    "gate_app",
                    "gui_app",
                    "java",
                    "ssl_ca",
                    "svip",
                    "svip_app",
                    "svip_backend_app",
                    "svip_frontend_app",
                    "weblogic",
                    "weblogic12_2_app",
                    "weblogic_app_all_cluster",]
    authorized_keys_content = None
    assert "key12" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")
    
def test_case_12():
    print("#### TEST 12 ####")
    project = "ips"
    instance = "tst"
    group_names = [ "bus_app",
                    "camel_app",
                    "fm_app",
                    "gate_app",
                    "gui_app",
                    "java",
                    "rb_app",
                    "ssl_ca",
                    "svip",
                    "svip_app",
                    "svip_backend_app",
                    "svip_frontend_app",
                    "tst_app",
                    "tst_bus_app",
                    "tst_camel_app",
                    "tst_environment",
                    "tst_fm_app",
                    "tst_gate_app",
                    "tst_gui_app",
                    "tst_gui_app1",
                    "tst_java",
                    "tst_rb_app",
                    "tst_rb_base_app",
                    "tst_svip",
                    "tst_svip_backend",
                    "tst_svip_frontend",
                    "tst_svip_general",
                    "tst_weblogic",
                    "weblogic",
                    "weblogic12_2_app",
                    "weblogic_app_all_cluster",]
    authorized_keys_content = None
    assert "key13" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_13():
    print("#### TEST 13 ####")
    project = "radar"
    instance = "dev"
    group_names = [ "dev_app",
                    "dev_environment",
                    "dev_fe_app",
                    "fe_app",
                    "oracle_client",]
    authorized_keys_content = None
    assert "key15" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")

def test_case_14():
    print("#### TEST 14 ####")
    project = "radar"
    instance = "qna"
    group_names = [ "bus_app",
                    "camel_app",
                    "fm_app",
                    "gate_app",
                    "gui_app",
                    "java",
                    "qna_app",
                    "qna_bus_app",
                    "qna_camel_app",
                    "qna_environment",
                    "qna_fm_app",
                    "qna_gate_app",
                    "qna_gui_app",
                    "qna_weblogic",
                    "tst_java",
                    "weblogic",]
    authorized_keys_content = None
    assert "default_key" == ssh_key_set(user, project, instance, group_names, authorized_keys_content)
    print("OK")


if __name__ == "__main__":
    test_case_1()
    test_case_2()
    test_case_3()
    test_case_4()
    test_case_5()
    test_case_6()
    test_case_7()
    test_case_8()
    test_case_9()
    test_case_10()
    test_case_11()
    test_case_12()
    test_case_13()
    test_case_14()
