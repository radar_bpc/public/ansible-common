#!/bin/env python3

""" This script is a standalone tool for deleting indexes in OpenSearch (and Elasticsearch in general).
    You can use it with --url, --days, --indices, --verbose options. By default, the parameters are set
    when deploying the script via Ansible.
"""

import datetime
import subprocess
import argparse
import logging


def config_logger(verbose):

    # Set the logging level based on the verbosity argument
    if verbose:
        logging.basicConfig(
            level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s"
        )
    else:
        logging.basicConfig(
            level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
        )

    logger = logging.getLogger(__name__)

    return logger


def clear_indices(url, days_to_keep, indices_to_clean, logger):

    # Calculate the date N days ago
    cutoff_date = datetime.datetime.utcnow() - datetime.timedelta(days=days_to_keep)
    formatted_cutoff = cutoff_date.strftime("%Y-%m-%d")
    logger.debug(f"Formated cutoff: {formatted_cutoff}")

    actual_indices_to_clean = []
    # Add actual index names to clean up
    for index in indices_to_clean:
        indices_cmd = f"curl -s -XGET '{url}/_cat/indices/{index}*?v&s=index&h=index,creation.date.string' --header 'Content-Type: application/json' | awk '$2 < \"{formatted_cutoff}\" {{'{{print $1}}'}}'"
        indices_output = subprocess.check_output(
            indices_cmd, shell=True, universal_newlines=True
        )
        actual_indices_to_clean.extend(indices_output.splitlines())

    # Iterate over the lines in the output
    for line in actual_indices_to_clean:

        # Get the index name
        index_name = line.strip()
        
        # Delete the index
        delete_cmd = f"curl -s -X DELETE '{url}/{index_name}'"
        delete_output = subprocess.check_output(delete_cmd, shell=True, universal_newlines=True)

        logger.debug(f"Delete request: {delete_cmd}")
        logger.info(f"Deleted index {index_name}")
        logger.debug(f"Deletion output: {delete_output}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Clear OpenSearch indices older than N days."
    )
    parser.add_argument(
        "--url",
        help="OpenSearch internal URL",
        default="{{ open_search_endpoint }}",
    )
    parser.add_argument(
        "--days",
        help="Days older than which indices should be deleted",
        type=int,
        default={{ open_search_days_to_clean }},
    )
    parser.add_argument(
        "--indices",
        type=tuple,
        help="List of indices to be cleared",
        default=({{ open_search_indices_to_clean }}),
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="enable verbose logging"
    )

    args = parser.parse_args()

    url = args.url.rstrip("/")
    days = args.days
    indices = args.indices

    logger = config_logger(args.verbose)

    clear_indices(url, days, indices, logger)
