This role is generally intended for working with OpenSearch tasks.

Currently running a cleanup installation for old indexes.

Run example for setting a cleanup job:
```
ansible-playbook -i inventories/oci04 -l c04d1-wlsa01 playbooks/open-search.yml -bK -e clear_owner=svip -t clear_script
