#!/bin/bash

# Revision 5 [AIX] Errors while rg is down on both nodes - fix quotes in rg_check
# Revision 6 [AIX] Resource group parameter added
# Revision 7 Syntax cleanup
# Revision 8 Add hostname to the output
# Revision 9 Veritas Cluster support
# Revision 10 Veritas Resource Groups exclude option

####### CONFIG SECTION ########
#-----   AIX
#please specify name of resource group
SV_RG_NAME=svbus_rg

#----- Linux
#Linux release file location
LINUX_RELEASE_FILE=/etc/redhat-release
#### END OF CONFIG SECTION ####

#----- Common Config
#Exclude resource groups
VERITAS_RG_EXCLUDE="vxfen|NotifierSNMP|phnicgrp"

case `uname` in

	AIX)
		echo "Starting on AIX assuming PowerHA installed"

		CLUSTER_BINARY=/usr/es/sbin/cluster/clstrmgr

		PROC_CHECK_CMD="lssrc -a | grep clstrmgrES | awk '{print \$4}'"
		PROC_CHECK_EXPECTED="active"

		CLUSTER_CHECK_CMD="lssrc -ls clstrmgrES | grep 'Current state:' | awk '{print \$3}'"
		CLUSTER_CHECK_EXPECTED="ST_STABLE"

		HOSTNAME=`hostname`
		RG_CHECK_CMD="/usr/es/sbin/cluster/utilities/clRGinfo -s | grep ${SV_RG_NAME} | grep ONLINE | cut -d ':' -f 3"

		RG_SUSPENDED_CMD="/usr/es/sbin/cluster/utilities/clRGinfo -m | grep 'ONLINE MONITOR SUSPENDED'"
		;;
	
	Linux)


		if [ -e $LINUX_RELEASE_FILE ]
			then
				LINUX_RELEASE=`cat $LINUX_RELEASE_FILE | grep -oP "release \K[[:digit:]](?=\.[[:digit:]])"`
			else
				LINUX_RELEASE=0
		fi

		echo "Starting on Linux. Checking if RHEL HA addon installed or Veritas Cluster"

		CLUSTER_BINARY=/usr/sbin/rgmanager


		if [ -e $CLUSTER_BINARY ]
			then
				echo "RHEL HA addon detected"
			else
				CLUSTER_BINARY=/opt/VRTSvcs/bin/had
				if [ -e $CLUSTER_BINARY ]
					then
						echo "Veritas cluster detected"
						CLUSTER_VENDOR=VERITAS
				fi
						
		fi






		PROC_CHECK_EXPECTED="cluster is running."
		PROC_CHECK_CMD="/sbin/service cman status"

		CLUSTER_CHECK_CMD="/usr/sbin/clustat | grep 'Member Status:' | awk '{print \$3}'"
		CLUSTER_CHECK_EXPECTED="Quorate"

		if [ -e /usr/sbin/cman_tool ]
			then
	
				HOSTNAME=`/usr/sbin/cman_tool status | grep 'Node name:' | awk '{print \$3}'`
		fi
		RG_CHECK_CMD="/usr/sbin/clustat | grep service: | grep started | awk '{print \$2}'"

		RG_SUSPENDED_CMD="/usr/sbin/clustat | grep service: | grep started | grep '[Z]' > /dev/null"


		if [ "$CLUSTER_VENDOR" == "VERITAS" ]

			then
				if [ $LINUX_RELEASE -lt 7 ]
					then
						PROC_CHECK_EXPECTED="active (running)"
						PROC_CHECK_CMD="/sbin/service vcs status"

					elif [ $LINUX_RELEASE -eq 7 ]
						then	
							PROC_CHECK_EXPECTED="active"
							PROC_CHECK_CMD="systemctl is-active vcs"
				fi

				CLUSTER_CHECK_CMD="/opt/VRTSvcs/bin/haclus -state -localclus"
				CLUSTER_CHECK_EXPECTED="RUNNING"
	
				HOSTNAME=`uname -n`

	
				RG_CHECK_CMD="/opt/VRTSvcs/bin/hagrp -state | egrep -i -v -e '(${VERITAS_RG_EXCLUDE})' | grep ONLINE | awk '{print \$3}'"
				RG_SUSPENDED_CMD="/opt/VRTSvcs/bin/hagrp -display -attribute TFrozen Frozen -localclus| awk '{print $4}' | grep 1 > /dev/null"
		fi
	;;


	SunOS)
		echo "Starting on Solaris assuming Solaris Cluster installed"

		CLUSTER_BINARY=/usr/cluster/bin/cluster
		PROC_CHECK_EXPECTED="online"
		PROC_CHECK_CMD="/usr/bin/svcs -o STATE -H rgm-starter"

		#CLUSTER_CHECK_CMD="my_status=\"\" && for nodes in \`scha_cluster_get -O All_Nodenames\`; do my_status=\${my_status}\`scha_cluster_get -O NODESTATE_NODE \$nodes\`; done && echo \$my_status | grep DOWN || echo RUNNING"
		#CLUSTER_CHECK_EXPECTED="RUNNING"

		CLUSTER_CHECK_CMD="/usr/cluster/bin/cluster status | /usr/sfw/bin/ggrep -A 2 \"Needed\+[[:space:]]\+Present\+[[:space:]]\+Possible\" | tail -1 | awk '{ if (\$2 >= \$1) print \"Quorate\";else print \"Failed\"}'"

		CLUSTER_CHECK_EXPECTED="Quorate"

		HOSTNAME=`hostname`
		RG_CHECK_CMD="/usr/cluster/bin/clrg status | grep Online | awk '{print \$2}'"

		RG_SUSPENDED_CMD="my_status=\"\" && rg_group=\"\" && for rg_group in \`/usr/cluster/bin/scha_cluster_get -O ALL_RESOURCEGROUPS\`; do my_status=\${my_status}\`/usr/cluster/bin/scha_resourcegroup_get -O SUSPEND_AUTOMATIC_RECOVERY -G \$rg_group\`; done && echo \$my_status | grep TRUE > /dev/null"
		;;

	*)
		echo "Cant determine OS. Exiting..."
		;;
esac

function file_check {

	if [ -e $CLUSTER_BINARY ]
		then
			BINARY_EXISTS=yes
		else 
			echo "No Cluster binaries found. Looks like DR server"
			BINARY_EXISTS=no
	fi
} 

function process_check {

	#Check Cluster Processes are running
	PROC_CHECK=`eval $PROC_CHECK_CMD`

	#echo "Result: $PROC_CHECK"

	if [ "$PROC_CHECK" == "$PROC_CHECK_EXPECTED" ]
		then
			echo "Cluster Processes are running"
			PROC_RUN=yes
		else
			echo "Cluster Processes are not running, looks like DR site"
			PROC_RUN=no
	fi
}

function cluster_check {

	#Check Cluster services is running on this node and services are in Stable state
	CLUSTER_CHECK=`eval $CLUSTER_CHECK_CMD`

	if [ "$CLUSTER_CHECK" == "$CLUSTER_CHECK_EXPECTED" ]
		then
			echo "Cluster is running"
			CLUSTER_RUN=yes
		else
			echo "Cluster is in wrong state"
			CLUSTER_RUN=no
	fi
}

function rg_check {

	#Check is Resource Group is running on this Node
	RG_CHECK=`eval $RG_CHECK_CMD`

	if [ "$RG_CHECK" == "$HOSTNAME" ]
		then
			echo "I am the active node"
			hostname 
			RG_RUN=yes
		else
			echo "I am the passive node"
			hostname
			RG_RUN=no
	fi
}

file_check

case $BINARY_EXISTS in
	yes)
		process_check
		case $PROC_RUN in
			yes)
				cluster_check
				case $CLUSTER_RUN in
					yes)
						#Check for resource gruop
						rg_check
						case $RG_RUN in
							yes)
								#Active Cluster Node
								eval $RG_SUSPENDED_CMD
								if [ $? -eq 0 ]
									then
										echo "At least one application monitor suspended. Check cluster configuration"
										EXIT_CODE=255
									else
										EXIT_CODE=0
								fi
								;;
							no)
								#Passive Cluster Node
								EXIT_CODE=1
								;;
							*)
								EXIT_CODE=255
								;;
						esac
						;;
					no)
						#Wrong cluster state
						EXIT_CODE=255
						;;
					*)
						EXIT_CODE=255
						;;
				esac
				;;
			no)
				#Looks like DR site. Service checks are needed.
				EXIT_CODE=2
				;;
			*)
				EXIT_CODE=255
				;;
		esac
		;;
	no)
		#Looks like DR site. Starting service checks
		EXIT_CODE=2
		;;
	*)
		EXIT_CODE=255
		;;
esac

echo "EXIT_CODES:
	0 - Active cluster node
	1 - Passive cluster node
	2 - DR host
        255 - Cant determine status or Resource Group is frozen
"

exit $EXIT_CODE
