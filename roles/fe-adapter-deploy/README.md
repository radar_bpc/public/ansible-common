# Deploy SVFE adapter
```shell
ansible-playbook -i inventories/oci03 -l c03t1-feap04 playbooks/svfe-adapter-deploy.yml -e release=R23.50.352 -e app_path=s3://sv/SV_Patches/R23.50.352/radar/adapter/adapter.sfx
```
