# Freeze/Unfreeze the cluster
## Required
oci cli + configuration on Ansible master host
## Freeze cluster 
```shell
ansible-playbook -i inventories/oci02 playbooks/freeze_cluster.yml
```

## Unfreeze cluster
```shell
ansible-playbook -i inventories/oci02 playbooks/freeze_cluster.yml -e freeze_cluster_action=unfreeze
```
