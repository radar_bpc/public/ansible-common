get-cert-url
===

Get cert by URL

## Arguments

| Argument              | Type   | Description                         |
|-----------------------|--------|-------------------------------------|
| get_cert_url_url      | dict   | List of dict of host and port       |
| get_cert_url_path     | string | Directory for certificate           |
| get_cert_url_filename | string | **Nullable.** Certificate file name |

## Return

| Argument              | Type   | Description           |
|-----------------------|--------|-----------------------|
| get_cert_url_filename | string | Certificate file name |
