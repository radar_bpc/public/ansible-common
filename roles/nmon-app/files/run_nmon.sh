#!/bin/sh
#### Config Section
#Specify folder to store data (inside user's home folder)
MON_FOLDER=/var/perfstat

#Specify time interval for statistics collection
INTERVAL=10
#### End of Config Section

COUNT=`echo "3600 / $INTERVAL" | bc`

HOST=`/bin/hostname`
CURRENT_USER=`/usr/bin/id -un`

PARENT_FOLDER=${MON_FOLDER}

DATE=`/bin/date +'%C%y%m%d_%H%M'`

CURR_DAY=`/bin/date +'%C%y%m%d'`
CURR_FOLDER=${PARENT_FOLDER}/nmon/${CURR_DAY}

if [ ! -d "$CURR_FOLDER" ]
        then
                mkdir -p $CURR_FOLDER
fi

case `uname` in
        AIX)
                nmon -f -w 4 -NN -^ -l 64 -dTAVPMNWS -m $CURR_FOLDER -s $INTERVAL -c $COUNT
        ;;
        Linux)
                nmon -f -NN -l 64 -TMN -m $CURR_FOLDER -s $INTERVAL -c $COUNT
        ;;
esac
