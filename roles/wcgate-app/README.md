# Wirecard application

## Installation guide from developers

The primary build of WirecardGate uploaded to T4:
/home/weblogic/distr/wirecard/com.bpcbt.wirecardgate-master-v1.0.0-23-g9f3e6a3d.war

### Installation How-to

1. Create an application home directory, e.g. /home/weblogic/wirecardgate_home (the application home directory template — wirecardgate_home.zip)
2. Adjust configuration in wirecardgate.properties.
   Check full paths to the logging configuration file and the application log:
   logging.config
   logging.file
   Check full paths to application template files:
   closeCard.applicationTemplate
   closeCustomer.applicationTemplate
   registerCustomer.applicationTemplate
   Virtual card creation parameters:
   createVirtualCard.livetime
   createVirtualCard.productType
   createVirtualCard.serviceCardNumber
   Customer registration parameters:
   registerCustomer.accountRestrictionCode
   SVAP communcation parameters:
   svap.address
   EPAY communication parameters:
   svfe.epay.host
   svfe.epay.port
   svfe.epay.merchantType
   svfe.epay.terminalId
   svfe.epay.merchantId
3. Add property com.bpcbt.wirecardgate.home to the managed server startup arguments.
   -Dcom.bpcbt.wirecardgate.home=/home/weblogic/wirecardgate_home
4. Create tables used by the application with the attached script create_tables.sql.
5. Configure the following datasources:
   jdbc/wirecardgate/db — application DB;
   jdbc/wirecardgate/svfe — SVFE;
   jdbc/wirecardgate/svbo — SVBO.
6. Since basic HTTP-authentication is performed by the application itself it's required to set enforce-valid-basic-auth-credentials flag to false. Add the following element into security-configuration of WebLogic domain's config.xml:
   ...
   <enforce-valid-basic-auth-credentials>false</enforce-valid-basic-auth-credentials>
   </security-configuration>
   ...
7. Deploy the application.
   The provided version supports the customer registration request — /wirecardgate/v2/register-customer.
   Basic authentication credentials configured in the default wirecardgate.properties: user/secret
