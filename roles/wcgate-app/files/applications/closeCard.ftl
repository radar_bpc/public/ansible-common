<application xmlns="http://sv.bpc.in/SVAP/iss">
    <application_type>APTPISSA</application_type>
    <application_flow_id>1007</application_flow_id>
    <application_status>APST0006</application_status>
    <operator_id>OPERATOR1</operator_id>
    <institution_id>7023</institution_id>
    <agent_id>60000013</agent_id>
    <customer_type>ENTTPERS</customer_type>
    <customer>
        <command>CMMDEXPR</command>
        <customer_number>${customerNumber}</customer_number>
        <contract id="contract_1">
            <command>CMMDEXPR</command>
            <card id="card_1">
                <command>CMMDEXRE</command>
                <card_id>${cardId}</card_id>
            </card>
        </contract>
    </customer>
</application>
