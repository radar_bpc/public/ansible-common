<application xmlns="http://sv.bpc.in/SVAP/iss">
    <application_type>APTPISSA</application_type>
    <application_flow_id>1011</application_flow_id>
    <application_status>APST0006</application_status>
    <operator_id>OPERATOR1</operator_id>
    <institution_id>7023</institution_id>
    <agent_id>60000013</agent_id>
    <customer_type>ENTTPERS</customer_type>
    <customer>
        <command>CMMDEXUP</command>
        <customer_number>${customerNumber}</customer_number>
        <customer_status>CTST0020</customer_status>
        <contract id="contract_1">
            <command>CMMDEXRE</command>
            <end_date>${contractEndDate}</end_date>
        </contract>
        <contact>
            <command>CMMDEXRE</command>
            <contact_type>CNTTPRMC</contact_type>
            <contact_data>
                <commun_method>CMNM0001</commun_method>
                <commun_address>${phoneNumber}</commun_address>
            </contact_data>
        </contact>
    </customer>
</application>
