<application xmlns="http://sv.bpc.in/SVAP/iss">
    <application_type>APTPISSA</application_type>
    <application_flow_id>1004</application_flow_id>
    <application_status>APST0006</application_status>
    <operator_id>OPERATOR1</operator_id>
    <institution_id>7023</institution_id>
    <agent_id>60000013</agent_id>
    <customer_type>ENTTPERS</customer_type>
    <customer id="customer_1">
        <command>CMMDEXUP</command>
        <customer_number>${customerNumber}</customer_number>
        <#if nationality??>
            <nationality>${nationality}</nationality>
        </#if>

        <#if lastName?? || firstName?? || birthDate??>
            <person>
                <command>CMMDCRUP</command>
                <#if lastName?? || firstName??>
                    <person_name language="LANGENG">
                        <#if lastName??>
                            <surname>${lastName}</surname>
                        </#if>
                        <#if firstName??>
                            <first_name>${firstName}</first_name>
                        </#if>
                    </person_name>
                </#if>
                <#if birthDate??>
                    <birthday>${birthDate}</birthday>
                </#if>
            </person>
        </#if>

        <#if oldPhoneNumber?? && newPhoneNumber??>
            <contact>
                <command>CMMDEXRE</command>
                <contact_type>CNTTPRMC</contact_type>
                <preferred_lang>LANGENG</preferred_lang>
                <contact_data>
                    <commun_method>CMNM0001</commun_method>
                    <commun_address>${oldPhoneNumber}</commun_address>
                </contact_data>
            </contact>

            <contact>
                <command>CMMDCRUP</command>
                <contact_type>CNTTPRMC</contact_type>
                <preferred_lang>LANGENG</preferred_lang>
                <contact_data>
                    <commun_method>CMNM0001</commun_method>
                    <commun_address>${newPhoneNumber}</commun_address>
                </contact_data>
            </contact>
        </#if>
    </customer>
</application>
