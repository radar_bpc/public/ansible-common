<application xmlns="http://sv.bpc.in/SVAP/iss">
    <application_type>APTPISSA</application_type>
    <application_flow_id>1004</application_flow_id>
    <application_status>APST0006</application_status>
    <operator_id>OPERATOR1</operator_id>
    <institution_id>7023</institution_id>
    <agent_id>60000013</agent_id>
    <customer_type>ENTTPERS</customer_type>
    <customer>
        <command>CMMDEXUP</command>
        <customer_number>${customerNumber}</customer_number>
        <customer_category>${customerCategory}</customer_category>
    </customer>
</application>
