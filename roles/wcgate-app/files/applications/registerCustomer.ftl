<application xmlns="http://sv.bpc.in/SVAP/iss">
    <application_type>APTPISSA</application_type>
    <application_flow_id>1001</application_flow_id>
    <application_status>APST0006</application_status>
    <operator_id>OPERATOR1</operator_id>
    <institution_id>7023</institution_id>
    <agent_id>60000013</agent_id>
    <customer_type>ENTTPERS</customer_type>
    <customer id="customer_1">
        <command>CMMDCREX</command>
        <#if customerCategory??>
            <customer_category>${customerCategory}</customer_category>
        </#if>
        <#if nationality??>
            <nationality>${nationality}</nationality>
        </#if>

        <contract>
            <command>CMMDCREX</command>
            <contract_type>CNTPPRPD</contract_type>
            <product_id>60000057</product_id>

            <service value="60000063">
                <service_object ref_id="account_1">
                </service_object>
            </service>

            <account id="account_1">
                <command>CMMDCREX</command>
                <#if accountNumber??>
                    <account_number>${accountNumber}</account_number>
                </#if>
                <currency>702</currency>
                <account_type>ACTP0100</account_type>
            </account>
        </contract>

        <#if lastName?? || firstName?? || birthDate??>
            <person>
                <command>CMMDCRUP</command>
                <#if lastName?? || firstName??>
                    <person_name language="LANGENG">
                        <#if lastName??>
                            <surname>${lastName}</surname>
                        </#if>
                        <#if firstName??>
                            <first_name>${firstName}</first_name>
                        </#if>
                    </person_name>
                </#if>
                <#if birthDate??>
                    <birthday>${birthDate}</birthday>
                </#if>
            </person>
        </#if>

        <#if phoneNumber??>
            <contact>
                <command>CMMDCRUP</command>
                <contact_type>CNTTPRMC</contact_type>
                <preferred_lang>LANGENG</preferred_lang>
                <contact_data>
                    <commun_method>CMNM0001</commun_method>
                    <commun_address>${phoneNumber}</commun_address>
                </contact_data>
            </contact>
        </#if>
    </customer>
</application>
