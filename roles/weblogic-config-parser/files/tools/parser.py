#!/bin/env python3

import xmltodict, sys, json, re

from pathlib import Path


def describe_servers(config):
    servers = []
    for i in config["domain"]["server"]:
        if i["name"] != "AdminServer":
            try:
                listen_ssl_port = i["ssl"]["listen-port"]
            except KeyError:
                listen_ssl_port = 7002
            s = {
                "server_name": i["name"],
                "listen_port": i["listen-port"],
                "listen_ssl_port": listen_ssl_port,
                "jvm_args": i["server-start"]["arguments"],
            }
            servers.append(s)
    return servers


def describe_applications(config):
    applications = []
    try:
        for i in config["domain"]["app-deployment"]:
            a = {
                "name": i["name"],
                "targets": i["target"],
                "artifact": i["source-path"],
            }
            applications.append(a)
    except TypeError:
        a = {
            "name": config["domain"]["app-deployment"]["name"],
            "targets": config["domain"]["app-deployment"]["target"],
            "artifact": config["domain"]["app-deployment"]["source-path"],
        }
        applications.append(a)
    return applications


def describe_libraries(config):
    libraries = []
    if "library" in config["domain"]:
        if type(config["domain"]["library"]) is list:
            for i in config["domain"]["library"]:
                l = {
                    "name": i["name"].split('@')[0],
                    "targets": i["target"],
                    "artifact": i["source-path"],
                }
                if "jsf" in l["name"]:
                    l["stage"] = "nostage"
                libraries.append(l)
        else:
            l = {
                    "name": config["domain"]["library"]["name"].split('@')[0],
                    "targets": config["domain"]["library"]["target"],
                    "artifact": config["domain"]["library"]["source-path"],
                }
            if "jsf" in l["name"]:
                l["stage"] = "nostage"
            libraries.append(l)
    return libraries


def describe_datasources(d_sources_list):
    datasources = []
    for element in d_sources_list:
        if generic_or_mds(element) == "ds":
            pass
        else:
            continue
        try:
            host = element["jdbc-data-source"]["jdbc-driver-params"]["url"].split("/")[
                2
            ]
        except:
            host = (
                element["jdbc-data-source"]["jdbc-driver-params"]["url"]
                .split("@")[1]
                .split("/")[0]
            )
        try:
            db_service = element["jdbc-data-source"]["jdbc-driver-params"]["url"].split(
                "/"
            )[3]
        except IndexError:
            try:
                db_service = (
                    element["jdbc-data-source"]["jdbc-driver-params"]["url"]
                    .split("@")[1]
                    .split("/")[1]
                )
            except IndexError:
                db_service = ( element["jdbc-data-source"]["jdbc-driver-params"]["url"] )
        try:
            for i in config["domain"]["jdbc-system-resource"]:
                if i["name"] == element["jdbc-data-source"]["name"]:
                    if "target" in i and i["target"] is not None:
                        targets = i["target"].split(",")
                    else:
                        targets = []
                else:
                    targets = []
        except TypeError:
            if config["domain"]["jdbc-system-resource"]["name"] == element["jdbc-data-source"]["name"]:
                if "target" in i and i["target"] is not None:
                    targets = i["target"].split(",")
                else:
                    targets = []
            else:
                    targets = []
        try:
            jndi = element["jdbc-data-source"]["jdbc-data-source-params"][
                "jndi-name"
            ].split(",")
        except AttributeError:
            jndi = element["jdbc-data-source"]["jdbc-data-source-params"]["jndi-name"]
        source = {
            "name": element["jdbc-data-source"]["name"],
            "user": element["jdbc-data-source"]["jdbc-driver-params"]["properties"][
                "property"
            ]["value"],
            "jndi": jndi,
            # "driver_name": element["jdbc-data-source"]["jdbc-driver-params"][
            #     "driver-name"
            # ],
            "host": host,
            "db_service": db_service,
            "targets": targets,
            "db_password": "TODO",
        }
        # if "jdbc-connection-pool-params" in element["jdbc-data-source"]:
        #     source["connection-pool-params"] = element["jdbc-data-source"][
        #         "jdbc-connection-pool-params"
        #     ]
        if any(x in ["jdbc/SVWeb", "jdbc/SVWeb1", "jdbc/SVWeb2"] for x in source["jndi"]):
            source["connection-pool-params"] = "{{ wl_domain_ds_olap_param }}"
        else:
            source["connection-pool-params"] = "{{ wl_domain_ds_oltp_param }}"
        # if (
        #     "global-transactions-protocol"
        #     in element["jdbc-data-source"]["jdbc-data-source-params"]
        # ):
        #     source["phase_commit"] = element["jdbc-data-source"][
        #         "jdbc-data-source-params"
        #     ]["global-transactions-protocol"]
        datasources.append(source)
    return datasources


def describe_multisources(m_dsources):
    multisources = []
    for element in m_dsources:
        if generic_or_mds(element) == "mds":
            pass
        else:
            continue
        for i in config["domain"]["jdbc-system-resource"]:
            if i["name"] == element["jdbc-data-source"]["name"]:
                if i["target"] is not None:
                    targets = i["target"].split(",")
                else:
                    continue
        try:
            jndi = element["jdbc-data-source"]["jdbc-data-source-params"][
                "jndi-name"
            ].split(",")
        except AttributeError:
            jndi = element["jdbc-data-source"]["jdbc-data-source-params"]["jndi-name"]
        try:
            msource = {
                "name": element["jdbc-data-source"]["name"],
                "jndi": jndi,
                "targets": targets,
                "dsorses": element["jdbc-data-source"]["jdbc-data-source-params"][
                    "data-source-list"
                ].split(","),
            }
        except UnboundLocalError:
            targets = ["TODO"]
        if "jdbc-connection-pool-params" in element["jdbc-data-source"]:
            msource["connection-pool-params"] = element["jdbc-data-source"][
                "jdbc-connection-pool-params"
            ]
        try:
            multisources.append(msource)
        except UnboundLocalError:
            multisources.append("TODO")
    return multisources


def generic_or_mds(d):
    try:
        if "datasource-type" in d["jdbc-data-source"]:

            if d["jdbc-data-source"]["datasource-type"] == "GENERIC":
                return "ds"

            elif d["jdbc-data-source"]["datasource-type"] == "MDS":
                return "mds"

            else:
                print(f"Not processed: {d['jdbc-data-source']['name']}")

        elif "algorithm-type" in d["jdbc-data-source"]["jdbc-data-source-params"]:
            return "mds"

        else:
            return "ds"

    except Exception as e:
        print(f"Not processed: {d['jdbc-data-source']['name']}")
        print(e)
        if "targets" in str(e):
            print(
                "If you see 'targets' referenced before the assignment, it means that the"
            )
            print(
                "targets are not defined for this data source and it should not be copied."
            )
        return "not_proc"


def pythonize_true_false(text):
    text = text.replace('"true"', "True")
    text = text.replace('"false"', "False")
    return text


def remove_quotes_if_number_between_quotes(text):
    text = pythonize_true_false(text)
    pattern = r'"(-?\d+)"'
    matches = re.findall(pattern, text)
    for match in matches:
        if text.startswith('"' + match + '"'):
            text = text.replace('"' + match + '"', match, 1)
        elif text.find(' "' + match + '" ') != -1:
            text = text.replace(' "' + match + '" ', " " + match + " ")
        elif text.find('"' + match + '"') != -1:
            text = text.replace('"' + match + '"', match)
    return text


if __name__ == "__main__":

    dir_src = sys.argv[1]
    dir_dest = sys.argv[2]
    new_file_name = sys.argv[3]
    domain_path = sys.argv[4]

    new_file_name = new_file_name + "_" + Path(domain_path).name + "_tmp.yml"
    middleware_home = Path(domain_path).parent.parent.parent

    d_sources_files = [
        f
        for f in Path(dir_src, "jdbc").iterdir()
        if f.name.endswith(".xml") and f.name != "config.xml"
    ]

    d_sources_list = []
    for file in d_sources_files:
        with open(Path(file).resolve(), "r") as f:
            xml_content = f.read()
            dict_content = xmltodict.parse(xml_content)
            d_sources_list.append(dict_content)

    with open(Path(dir_src, "config.xml").resolve(), "r") as f:
        config = xmltodict.parse(f.read())

    servers = describe_servers(config)
    applications = describe_applications(config)
    libraries = describe_libraries(config)
    datasources = describe_datasources(d_sources_list)
    multisources = describe_multisources(d_sources_list)

    header = f"""---
{config["domain"]["name"].lower()}:
    wl_user_home: "{{{{ wl_home }}}}"
    middleware_home: "{middleware_home}"
    domain_base_template: "{{{{ wl_domain_base_template }}}}"
    domain_name: "{config["domain"]["name"]}"
    admin_server_port: "{{{{ wl_domain_admin_server_port }}}}"
    admin_server_ssl_port: "{{{{ wl_domain_admin_server_ssl_port }}}}"
    ssl_enabled: "{{{{ wl_domain_admin_ssl_enabled }}}}"
    admin_user: "{{{{ wl_domain_admin_user }}}}"
    admin_password: "TODO"
    wl_endpoint: "{{{{ wl_domain_endpoint }}}}"
    user_config_file: "{{{{ wl_home }}}}/scripts122/{config["domain"]["name"]}.configfile.secure122"
    user_key_file: "{{{{ wl_home }}}}/scripts122/{config["domain"]["name"]}.keyfile.secure122"
    artifacts_path: "{{{{ wl_domain_artifacts_path }}}}"
    
"""

    with open(Path(dir_dest, new_file_name).resolve(), "w") as f:
        f.write(header)

    with open(Path(dir_dest, new_file_name).resolve(), "a") as f:
        f.write("\n    servers:\n        ")
        f.write(json.dumps(servers))
        f.write("\n\n    applications:\n        ")
        f.write(json.dumps(applications))
        f.write("\n\n    libraries:\n        ")
        f.write(json.dumps(libraries))
        f.write("\n\n    datasources:\n        ")
        f.write(json.dumps(datasources))
        f.write("\n\n    multisources:\n        ")
        f.write(json.dumps(multisources))

    with open(Path(dir_dest, new_file_name).resolve(), "r") as f:
        text = f.read()

    with open(Path(dir_dest, new_file_name).resolve(), "w") as f:
        f.write(remove_quotes_if_number_between_quotes(text))
