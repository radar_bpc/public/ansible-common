## SMS GATE APP

In this application, the variability and number of settings is so great that it was decided not to transfer them from yaml to xml, since this is a ton of work, but to store them immediately in xml in the project files `files/app/<project>/<instance>/<sms_gate_app_name>/`. Thus, to add a configuration file, you just need to put it in the appropriate directory in the project and start casting the role on the host. If you need to fix the parameter in the configs, then you need to do it in the xml file and recast the role.

In order to define a set of `smsgate` applications on a particular host, you need to set the `sms_gate_app_names` variable for the group, and the names of the directories containing the settings must match these names. The role will create home directories with the same name on the host and place the configuration files in the `config` directory in the corresponding home directory.


## Note that

Although the file configuration is stored in an almost unmodified xml format, nevertheless, in order for the passwords not to be in clear text, they are encrypted using ansible methods, which makes it necessary to use the `template` module instead of the `copy` module. The `template` module is very limited in its ability to bulk work with files, because of which it was necessary to introduce a variable listing the names of the files, instead of simply copying all the files in the corresponding directory. This variable `sms_gate_config_files` is in the `defaults` of the role and does not need to be copied to project variables. It just needs to list all potential filenames. If any of the files is missing for a particular configuration, then an exception will simply occur during its copying, and the work of the role will not be interrupted.


## USAGE

Here we have the `install` and `config` tags. The installation will create a directory structure and can do any other environment setup. The config task only copies the configuration. Running without any tags will always run all steps of the role.
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/smsgate-app.yml -bK
```
or
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/smsgate-app.yml -bK --tags install
```
or
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/smsgate-app.yml -bK --tags config
```

Choose specific application
```shell
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/smsgate-app.yml -bK --tags config -e "app_name=smsgate2"
```

## TODO

- Get all config files from all environments
- Remove templates and files from this common role


## EXAMPLES

Example of directory configuration for multiple environments and projects:
```
└── radar
    ├── prod
    │   ├── smsgate2
    │   │   ├── binToSrcAddressName.config
    │   │   ├── inst_id_decorator.xml
    │   │   ├── institution-to-smtp-mapping.xml
    │   │   ├── logback.xml
    │   │   ├── smscommands.properties
    │   │   ├── smsgate.properties
    │   │   └── smslib.properties
    │   └── smsgate_singtel
    │       ├── binToSrcAddressName.config
    │       ├── inst_id_decorator.xml
    │       ├── logback.xml
    │       ├── smscommands.properties
    │       ├── smsgate.properties
    │       └── smslib.properties
    └── tst
        ├── smsgate-gsm
        │   ├── binToSrcAddressName.config
        │   ├── inst_id_decorator.xml
        │   ├── institution-to-smtp-mapping.xml
        │   ├── logback.xml
        │   ├── smscommands.properties
        │   ├── smsgate.properties
        │   └── smslib.properties
        ├── smsgate_rnd
        │   ├── binToSrcAddressName.config
        │   ├── inst_id_decorator.xml
        │   ├── institution-to-smtp-mapping.xml
        │   ├── logback.xml
        │   ├── smscommands.properties
        │   ├── smsgate.properties
        │   └── smslib.properties
        └── smsgate_singtel
            ├── binToSrcAddressName.config
            ├── inst_id_decorator.xml
            ├── institution-to-smtp-mapping.xml
            ├── logback.xml
            ├── smscommands.properties
            ├── smsgate.properties
            └── smslib.properties
```
