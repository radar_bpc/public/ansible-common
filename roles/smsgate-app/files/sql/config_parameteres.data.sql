insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.request.url', 'http://localhost:8095/services/pushSmsService');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.request.user', 'smsuser');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.request.code', 'smscode123');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.request.moduleId', '999999');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.request.timeout', '10000');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.auth.user', 'admin');
insert into CONFIG_PARAMETERS
values ('smsgate', 'ibbl.auth.pswd', 'pass');
insert into CONFIG_PARAMETERS
values ('smsgate', 'abu.request.requestUrl', 'http://localhost:8096/commgw/message');
insert into CONFIG_PARAMETERS
values ('smsgate', 'abu.request.requestTimeout', '5000');
