create sequence S_MSG_FWD_REDLVR_ID
    /

create sequence SET_PARAMETER_SEQ
    maxvalue 99999999
    nocache
/

create sequence S_UFD_MESSAGE_STATUS_ID
    minvalue 0
    /

create sequence S_UFD_MESSAGE_ID
    minvalue 0
    /

create table COM_VERSION
(
    VERSION_NUMBER VARCHAR2(200 char),
    BUILD_DATE DATE,
    INSTALL_DATE DATE,
    PART_NAME VARCHAR2(8 char),
    REVISION NUMBER(12),
    GIT_REVISION VARCHAR2(40 char),
    RELEASE VARCHAR2(8 char)
)
    /

comment on table COM_VERSION is 'List of system versions.'
/

comment on column COM_VERSION.VERSION_NUMBER is 'Number of system version.'
/

comment on column COM_VERSION.BUILD_DATE is 'Date of build.'
/

comment on column COM_VERSION.INSTALL_DATE is 'Release installation date.'
/

comment on column COM_VERSION.PART_NAME is 'Part of build.'
/

comment on column COM_VERSION.REVISION is 'SVN revision.'
/

comment on column COM_VERSION.GIT_REVISION is 'GIT revision'
/

comment on column COM_VERSION.RELEASE is 'PA DSS release number'
/

create table MESSAGE_LOG
(
    MESSAGE_ID NUMBER(10) not null
        constraint MESSAGE_LOG_PK
        primary key,
    PHONE VARCHAR2(14 char) not null,
    QUERY_TEXT VARCHAR2(1024 char),
    RESPONSE_TEXT VARCHAR2(1024 char),
    RECEIVED_AT DATE not null,
    SENT_AT DATE,
    DELIVERED_AT DATE,
    PROCESSING_STATUS VARCHAR2(15 char) not null,
    STATUS_DETAILS VARCHAR2(255 char)
)
    /

create table T_LOCALIZATION
(
    ID NUMBER(10) not null
        constraint T_LOCALIZATION_PK
        primary key,
    KEY VARCHAR2(255 char) not null,
    VALUE VARCHAR2(2000 char),
    LANG VARCHAR2(8 char) not null
)
/

create index T_LOCALIZATION_AT
	on T_LOCALIZATION (KEY)
/

create table SET_PARAMETER
(
    ID NUMBER(8) not null
        constraint SET_PARAMETER_PK
        primary key,
    PARAM_NAME VARCHAR2(200 char)
        constraint SET_PARAMETER_UK
        unique,
    PARAM_VALUE VARCHAR2(200 char)
)
/

comment on table SET_PARAMETER is 'Parameter defined values.'
/

comment on column SET_PARAMETER.ID is 'Primary key'
/

comment on column SET_PARAMETER.PARAM_NAME is 'Unique parameter system name.'
/

comment on column SET_PARAMETER.PARAM_VALUE is 'Value of parameter.'
/

create table MESSAGES
(
    ID NUMBER(10) not null
        constraint MESSAGES_PK
        primary key,
    DATA BLOB,
    LAST_ERROR NUMBER(10),
    LAST_MODIFIED TIMESTAMP(6),
    LAST_SUCCESSFUL_STEP VARCHAR2(255 char),
    PRIORITY NUMBER(10),
    STEP VARCHAR2(255 char),
    STATE NUMBER(10),
    TRIGGER_MODULE_ID VARCHAR2(255 char),
    TRIGGER_MODULE_TYPE NUMBER(10),
    TRIGGER_REFNUM VARCHAR2(255 char)
)
/

create table T_MESSAGE_FORWARD_REDELIVERY
(
    ID NUMBER(19) not null
        constraint PK_MSD_FWD_RD_ID
        primary key,
    INTERNAL_MESSAGE_ID NUMBER(19),
    PHONE VARCHAR2(32 char) not null,
    TEXT VARCHAR2(1024 char) not null,
    RE_DELIVERY_AT TIMESTAMP(6) not null,
    ATTEMPT NUMBER(2) not null
)
/

create index I_MSD_FWD_RD_REDELIVERY_AT
	on T_MESSAGE_FORWARD_REDELIVERY (RE_DELIVERY_AT)
/

create table CONFIG_PARAMETERS
(
    CONFIG_NAME VARCHAR2(200 char) not null,
    PARAM_NAME VARCHAR2(200 char) not null,
    PARAM_VALUE VARCHAR2(200 char),
    constraint SYS_PK_CONFIG_PARAMETERS
        primary key (CONFIG_NAME, PARAM_NAME)
)
/

comment on table CONFIG_PARAMETERS is 'Configuration parameters defined values.'
/

comment on column CONFIG_PARAMETERS.CONFIG_NAME is 'The column containing the name of the configuration.'
/

comment on column CONFIG_PARAMETERS.PARAM_NAME is 'The column containing the keys of the configuration.'
/

comment on column CONFIG_PARAMETERS.PARAM_VALUE is 'the column containing the values of the configuration.'
/

create table T_MSG_QUEUE_REF_NUM
(
    MESSAGE_ID NUMBER not null,
    REF_NUM VARCHAR2(256 char) not null,
    constraint T_MSG_QUEUE_REF_NUM_PK
        primary key (REF_NUM, MESSAGE_ID)
)
/

create table T_UFD_MESSAGE_QUEUE
(
    ID NUMBER not null
        constraint T_UFD_MESSAGE_QUEUE_PK
            primary key,
    FIRE_AT NUMBER not null,
    IS_PROC_FINISHED NUMBER(1),
    PROC_FINISH_ATTEMPT NUMBER(2) default 1,
    SEND_ATTEMPT NUMBER(2) default 1,
    REF_NUM VARCHAR2(32 char),
    RESEND_AT TIMESTAMP(6)
)
/

create index IND_UFD_MESSAGE_QUEUE_SEND_AT
	on T_UFD_MESSAGE_QUEUE (FIRE_AT)
/

create index I_MESSAGE_QUEUE_RESEND_AT
	on T_UFD_MESSAGE_QUEUE (RESEND_AT)
/

create table T_UFD_MESSAGE_LOG
(
    ID NUMBER not null
        constraint T_UFD_MESSAGE_LOG_PK
            primary key,
    DATE_TIME DATE not null,
    DIRECTION VARCHAR2(16 char) not null,
    DESTINATION VARCHAR2(265 char) not null,
    TYPE VARCHAR2(8 char) not null,
    ATTEMPT_NUMBER NUMBER,
    TEXT VARCHAR2(1024 char) not null,
    EXTERNAL_ID VARCHAR2(32 char),
    REF_NUM VARCHAR2(100 char)
)
    /

create table T_UFD_MESSAGE_STATUS_LOG
(
    ID NUMBER not null
        constraint T_UFD_MESSAGE_STATUS_LOG_PK
            primary key,
    MESSAGE_ID NUMBER not null
        constraint TUFDMESSAGESTATUSLOGMESSAGE_ID
            references T_UFD_MESSAGE_LOG,
    STATUS VARCHAR2(32 char) not null,
    STATUS_TIME DATE not null
)
    /

create index IND_UFD_MESSAGE_STATUS_LOG
	on T_UFD_MESSAGE_STATUS_LOG (MESSAGE_ID, STATUS_TIME)
/

create index UFD_MESSAGE_STATUS_LOG_NDX
	on T_UFD_MESSAGE_STATUS_LOG (MESSAGE_ID)
/

create or replace trigger TR_UFD_MESSAGE_STATUS_LOG_BI
	before insert
	on T_UFD_MESSAGE_STATUS_LOG
	for each row
begin
    if :new.id is null
    then
select s_ufd_message_status_id.nextval
into :new.id
from dual;
end if;
end;
/

create index IND_T_UFD_MESSAGE_LOG_DATE
	on T_UFD_MESSAGE_LOG (DATE_TIME)
/

create index T_UFD_EXTERNAL_ID_IND
	on T_UFD_MESSAGE_LOG (EXTERNAL_ID)
/

create or replace trigger TR_UFD_MESSAGE_LOG_BI
	before insert
	on T_UFD_MESSAGE_LOG
	for each row
begin
        if :new.id is null
        then
select s_ufd_message_id.nextval
into :new.id
from dual;
end if;
end;
/



create or replace view COM_VERSION_VW as
select version_number
     , build_date
     , install_date
     , part_name
     , revision
     , git_revision
     , release
from com_version
    /

create or replace view COM_UI_VERSION_VW as
select a.version_number
     , a.build_date
     , a.install_date
     , a.revision
     , a.part_name
     , a.git_revision
     , a.release
from com_version a
    /

create or replace package com_api_type_pkg as
    /****************************************************************
    *
    * The common types and constants                           <br />
    * Created by Filimonov A.(filimonov@bpc.ru)  at 08.07.2009 <br />
    * Last changed by $Author$                        <br />
    * $LastChangedDate::                           $           <br />
    * Revision: $LastChangedRevision: 6015$                    <br />
    * Module: COM_API_TYPE_PKG                                 <br />
    * @headcom                                                 <br />
    *****************************************************************/

    subtype t_ref_cur           is sys_refcursor;

    subtype t_boolean           is number(1);
    subtype t_agent_id          is number(8);
    subtype t_inst_id           is number(4);
    subtype t_sign              is number(1);
    subtype t_money             is number(22,4);
    subtype t_long_id           is number(16);
    subtype t_medium_id         is number(12);
    subtype t_short_id          is number(8);
    subtype t_tiny_id           is number(4);
    subtype t_curr_code         is varchar2(3);
    subtype t_curr_name         is varchar2(3);
    subtype t_name              is varchar2(200);
    subtype t_short_desc        is varchar2(200);
    subtype t_full_desc         is varchar2(2000);
    subtype t_port              is varchar2(7);
    subtype t_remote_adr        is varchar2(127);
    subtype t_dict_value        is varchar2(8);
    subtype t_module_code       is varchar2(3);
    subtype t_oracle_name       is varchar2(60);
    subtype t_attr_name         is varchar2(30);
    subtype t_person_id         is number(12);
    subtype t_account_id        is t_medium_id;
    subtype t_account_number    is varchar2(32);
    subtype t_balance_id        is t_medium_id;
    subtype t_card_number       is varchar2(24);
    subtype t_bin               is varchar2(24);
    subtype t_desc_id           is t_medium_id;
    subtype t_seqnum            is number(4);
    subtype t_mcc               is varchar2(4);
    subtype t_rrn               is varchar2(36);
    subtype t_auth_code         is varchar2(6);
    subtype t_merchant_number   is varchar2(15);
    subtype t_terminal_number   is varchar2(16);
    subtype t_postal_code       is varchar2(10);
    subtype t_network_id        is number(4);
    subtype t_country_code      is varchar2(3);
    subtype t_semaphore_name    is varchar2(30);
    subtype t_cmid              is varchar2(12);
    subtype t_param_value       is varchar2(2000);
    subtype t_raw_data          is varchar2(4000);
    subtype t_text              is varchar2(4000);
    subtype t_rate              is number;
    subtype t_date_long         is varchar2(16);
    subtype t_date_short        is varchar2(8);
    subtype t_pin_block         is varchar2(16);
    subtype t_key               is varchar2(2048);
    subtype t_exponent          is varchar2(256);
    subtype t_region_code       is varchar2(11);
    subtype t_tag               is varchar2(6);
    subtype t_lob_data          is varchar2(32767);
    subtype t_sql_statement     is varchar2(32767);
    subtype t_geo_coord         is number(10,7);
    subtype t_auth_amount       is varchar2(23);
    subtype t_auth_long_id      is varchar2(16);
    subtype t_auth_medium_id    is varchar2(12);
    subtype t_auth_date_id      is varchar2(14);
    subtype t_auth_date         is varchar2(14);
    subtype t_original_data     is varchar2(42);
    subtype t_byte_id           is number(3);
    subtype t_byte_char         is varchar2(2);
    subtype t_count             is simple_integer; -- for counters, not null
    subtype t_uuid              is varchar2(36);
    subtype t_hash_value        is varchar2(128);

    type    t_sttl_day_rec      is record (
        sttl_day                number
        , sttl_date               date
    );
    type    t_sttl_day_tab          is table of t_sttl_day_rec index by binary_integer;

    type    t_rowid_tab             is table of rowid index by binary_integer;
    type    t_inst_id_tab           is table of t_inst_id index by binary_integer;
    type    t_agent_id_tab          is table of t_agent_id index by binary_integer;
    type    t_network_tab           is table of t_network_id index by binary_integer;
    type    t_number_tab            is table of number index by binary_integer;
    type    t_date_tab              is table of date index by binary_integer;
    type    t_timestamp_tab         is table of timestamp index by binary_integer;
    type    t_dict_tab              is table of t_dict_value index by binary_integer;
    type    t_name_tab              is table of t_name index by binary_integer;
    type    t_desc_tab              is table of t_full_desc index by binary_integer;
    type    t_integer_tab           is table of pls_integer index by binary_integer;
    type    t_boolean_tab           is table of t_boolean index by binary_integer;
    type    t_oracle_name_tab       is table of t_oracle_name index by binary_integer;
    type    t_curr_code_tab         is table of t_curr_code index by binary_integer;
    type    t_account_number_tab    is table of t_account_number index by binary_integer;
    type    t_mcc_tab               is table of t_mcc index by binary_integer;
    type    t_rrn_tab               is table of t_rrn index by binary_integer;
    type    t_merchant_number_tab   is table of t_merchant_number index by binary_integer;
    type    t_terminal_number_tab   is table of t_terminal_number index by binary_integer;
    type    t_auth_code_tab         is table of t_auth_code index by binary_integer;
    type    t_postal_code_tab       is table of t_postal_code index by binary_integer;
    type    t_card_number_tab       is table of t_card_number index by binary_integer;
    type    t_varchar2_tab          is table of varchar2(4000) index by binary_integer;
    type    t_country_code_tab      is table of t_country_code index by binary_integer;
    type    t_cmid_tab              is table of t_cmid index by binary_integer;
    type    t_raw_tab               is table of t_raw_data index by binary_integer;
    type    t_XMLType_tab           is table of XMLType index by binary_integer;
    type    t_param_tab             is table of t_param_value index by t_name;
    type    t_tag_value_tab         is table of t_param_value index by t_tag;
    type    t_lob_tab               is table of t_lob_data index by binary_integer;
    type    t_lob2_tab              is table of t_lob_data index by t_name;
    type    t_clob_tab              is table of clob index by binary_integer;
    type    t_param2d_tab           is table of t_param_tab index by t_dict_value;

    type    t_tiny_tab              is table of t_tiny_id index by binary_integer;
    type    t_short_tab             is table of t_short_id index by binary_integer;
    type    t_medium_tab            is table of t_medium_id index by binary_integer;
    type    t_long_tab              is table of t_long_id index by binary_integer;
    type    t_auth_amount_tab       is table of t_auth_amount index by binary_integer;
    type    t_auth_medium_tab       is table of t_auth_medium_id index by binary_integer;
    type    t_auth_date_tab         is table of t_auth_date index by binary_integer;
    type    t_auth_long_tab         is table of t_auth_long_id index by binary_integer;
    type    t_money_tab             is table of t_money index by binary_integer;

TRUE             constant t_boolean     := 1;
FALSE            constant t_boolean     := 0;

    CREDIT           constant t_sign       := 1;
    DEBIT            constant t_sign       := -1;
    NONE             constant t_sign       := 0;

end com_api_type_pkg;
/

create or replace package com_ui_version_pkg as

function get_last_version(
    i_part          in      com_api_type_pkg.t_dict_value := 'PARTCORE'
) return com_api_type_pkg.t_name
    result_cache;

function get_release(
    i_part          in      com_api_type_pkg.t_dict_value := 'PARTCORE'
) return com_api_type_pkg.t_name
    result_cache;

function get_description(
    i_major         in      com_api_type_pkg.t_tiny_id
  , i_minor         in      com_api_type_pkg.t_tiny_id
  , i_maintenance   in      com_api_type_pkg.t_tiny_id
  , i_build         in      com_api_type_pkg.t_tiny_id
  , i_extension     in      com_api_type_pkg.t_dict_value
  , i_revision      in      com_api_type_pkg.t_short_id
) return com_api_type_pkg.t_name;

procedure register_version(
    i_version       in      com_api_type_pkg.t_name
  , i_build_date    in      date
  , i_part_name     in      com_api_type_pkg.t_dict_value
  , i_git_revision  in      com_api_type_pkg.t_name
);

end com_ui_version_pkg;
/

create or replace package body com_ui_version_pkg as

function get_last_version(
    i_part          in      com_api_type_pkg.t_dict_value := 'PARTCORE')
return  com_api_type_pkg.t_name
        result_cache
        relies_on (com_version)
is
    l_version    com_api_type_pkg.t_name := '';
begin

for rec in (
              select version_number
                from (
                    select version_number
                      from com_version
                     where part_name = i_part order by build_date desc)
               where rownum = 1
        )
    loop
        l_version := l_version || rec.version_number;
end loop;

return l_version;

end get_last_version;

function get_release(
    i_part          in      com_api_type_pkg.t_dict_value := 'PARTCORE'
) return com_api_type_pkg.t_name
        result_cache
        relies_on (com_version)
is
    l_version    com_api_type_pkg.t_name := '';
begin
for rec in (
        select release
          from (
              select release
                from com_version
               where part_name = i_part order by build_date desc)
         where rownum = 1
    )
    loop
        l_version := l_version || rec.release;
end loop;

return l_version;

end get_release;

function get_description(
    i_major          in      com_api_type_pkg.t_tiny_id
  , i_minor          in      com_api_type_pkg.t_tiny_id
  , i_maintenance    in      com_api_type_pkg.t_tiny_id
  , i_build          in      com_api_type_pkg.t_tiny_id
  , i_extension      in      com_api_type_pkg.t_dict_value
  , i_revision       in      com_api_type_pkg.t_short_id
) return com_api_type_pkg.t_name is
begin
return to_char(i_major)||'.'||to_char(i_minor)||'.'||to_char(i_maintenance)||'.'||to_char(i_build)||to_char(i_extension)||'['||to_char(i_revision)||']';
end get_description;

procedure register_version(
      i_version      in      com_api_type_pkg.t_name
    , i_build_date   in      date
    , i_part_name    in      com_api_type_pkg.t_dict_value
    , i_git_revision in      com_api_type_pkg.t_name
) is
    l_release                com_api_type_pkg.t_name := '2.2.10';
begin
insert into com_version (
                          version_number
                        , build_date
                        , install_date
                        , part_name
                        , git_revision
                        , release
) values (
             i_version
         , i_build_date
         , sysdate
         , i_part_name
         , i_git_revision
         , l_release
         );
end register_version;

end;
/

create or replace function get_last_message_status(
    pmessageid in t_ufd_message_log.id%type
)
    return t_ufd_message_status_log.status%type
is
    vstatus t_ufd_message_status_log.status%type;
begin
select t.status
into vstatus
from t_ufd_message_status_log t
where t.message_id = pmessageid
  and t.id = (select MAX(t1.id)
              from t_ufd_message_status_log t1
              where t1.message_id = pmessageid);

return vstatus;
end get_last_message_status;
/

create or replace procedure ufd_report_error_message(
    pid            in t_ufd_message_queue.id%type,
    prefnum        in t_ufd_message_queue.ref_num%type,
    pfireatnowtime in t_ufd_message_queue.fire_at%type)
is
    vsendattempt   t_ufd_message_queue.send_attempt%type;
    vfinishattempt t_ufd_message_queue.proc_finish_attempt%type;
    vrefnum        t_ufd_message_queue.ref_num%type;
begin

select
    proc_finish_attempt,
    send_attempt,
    ref_num
into vfinishattempt, vsendattempt, vrefnum
from t_ufd_message_queue
where id = pid;

if (vrefnum is null)
    then

        if (vfinishattempt = vsendattempt)
        then
update t_ufd_message_queue
set fire_at = pfireatnowtime
where id = pid;
else
update t_ufd_message_queue
set proc_finish_attempt = (vfinishattempt + 1)
where id = pid;
end if;

else

        if (vrefnum = prefnum)
        then
update t_ufd_message_queue
set fire_at = pfireatnowtime
where id = pid;
else
update t_ufd_message_queue
set proc_finish_attempt = (vfinishattempt + 1)
where id = pid;
end if;

end if;

exception
    when others then null;

end ufd_report_error_message;
/

create or replace function ufd_add_new_message_log(
      pdatetime    in t_ufd_message_log.date_time%type
    , pdirection   in t_ufd_message_log.direction%type
    , pdestination in t_ufd_message_log.destination%type
    , ptype        in t_ufd_message_log.type%type
    , pstatus      in t_ufd_message_status_log.status%type
    , pstatustime  in t_ufd_message_status_log.status_time%type
    , ptext        in t_ufd_message_log.text%type
    , pexternalid  in t_ufd_message_log.external_id%type default null)
    return t_ufd_message_log.id%type
is
    lmessageid t_ufd_message_log.id%type;
begin

    lmessageid := s_ufd_message_id.nextval;

insert into t_ufd_message_log (id, date_time, direction, destination, type, text, external_id)
values (lmessageid, pdatetime, pdirection, pdestination, ptype, ptext, pexternalid);

insert into t_ufd_message_status_log (message_id, status, status_time)
values (lmessageid, pstatus, pstatustime);

return lmessageid;
end ufd_add_new_message_log;
/

create or replace procedure ufd_finish_message(
    pid     in t_ufd_message_queue.id%type,
    prefnum in t_ufd_message_queue.ref_num%type)
is
    vsendattempt   t_ufd_message_queue.send_attempt%type;
    vfinishattempt t_ufd_message_queue.proc_finish_attempt%type;
    vrefnum        t_ufd_message_queue.ref_num%type;
begin

select
    proc_finish_attempt,
    send_attempt,
    ref_num
into vfinishattempt, vsendattempt, vrefnum
from t_ufd_message_queue
where id = pid;

if (vrefnum is null)
    then
        if (vfinishattempt = vsendattempt)
        then
update t_ufd_message_queue
set is_proc_finished = 1
where id = pid;
else
update t_ufd_message_queue
set proc_finish_attempt = (vfinishattempt + 1)
where id = pid;
end if;
else
        if (vrefnum = prefnum)
        then
update t_ufd_message_queue
set is_proc_finished = 1
where id = pid;
else
update t_ufd_message_queue
set proc_finish_attempt = (vfinishattempt + 1)
where id = pid;
end if;
end if;

exception
    when others then null;

end ufd_finish_message;
/

