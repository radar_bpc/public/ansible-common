insert into T_LOCALIZATION
values (1, 'label.date', 'Date', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (2, 'label.destination', 'Destination', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (3, 'label.type', 'Type', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (4, 'label.direction', 'Direction', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (5, 'label.retry', 'Retry', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (6, 'label.message-text', 'Message text', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (7, 'label.status', 'Status', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (8, 'label.messages', 'Messages', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (9, 'label.status-history', 'Status history', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (11, 'text.show-status-history', 'Show status history', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (12, 'label.close', 'Close', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (13, 'text.empty', 'Empty', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (14, 'label.messages-log', 'Message log', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (15, 'label.administration', 'Administration', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (16, 'label.localization', 'Localization', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (17, 'label.key', 'Key', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (18, 'label.value', 'Value', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (19, 'label.reset', 'Reset', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (20, 'text.reset', 'Reset all grid filters', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (21, 'label.show-filter', 'Show filters', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (22, 'label.hide-filter', 'Hide filters', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (23, 'label.external-id', 'External id', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (24, 'label.enter-value', 'Enter value', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (25, 'label.search', 'Search', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (26, 'label.select-value', 'Select value', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (27, 'label.date-from', 'Date from', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (28, 'label.date-to', 'Date to', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (29, 'label.export-xlsx', 'Export XLSX', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (30, 'label.import-xlsx', 'Import XLSX', 'LANGENG');
insert into T_LOCALIZATION (ID, KEY, VALUE, LANG)
values (31, 'label.logout', 'Logout', 'LANGENG');
