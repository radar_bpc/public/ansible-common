# Weblogic action

## Usage
```shell
ansible-playbook -i inventories/test -l tst_gui_app  playbooks/wl-server-action.yml -e "app_name=API-Gate wl_action=stop_server" --ask-vault-password -v

```
## Parameters
* wl_action: The name of action. List of actions: stop_server, start_server, restart_server, status_server, status_ds
* app_name: The name of the managed server
