# Deployment single weblogic application

## Usage
```shell
ansible-playbook -i inventories/test -l tst_gui_app  playbooks/deploy-wl-single.yml -e "deploy_wl_app_name=API-Gate 
deploy_wl_app_path=s3://sv/SV_Patches/R23.30.319/onprem/apigate/ru_bpc_apigate_master-jdk8-v1.1.27-2-g6d12cc02_radar.ear 
deploy_wl_release=R23.30 deploy_wl_cleanup_cache=Y" --ask-vault-password -v

```
## Parameters
* deploy_wl_app_name: The name of the managed server
* deploy_wl_app_path: The full path to the application on Oracle Object Storage, for example: ```s3://sv/distr/smsgate/smsgate.ear```
* deploy_wl_release: The release number (only for application name in Weblogic console)
* deploy_wl_cleanup_cache: Remove cache, tmp and stage folders on Weblgoic managed server
