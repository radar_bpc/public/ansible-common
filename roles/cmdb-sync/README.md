cmdb-sync
===
Add certificate info to `devops-gui`

## Usage
```yaml
- name: "Get cert info"
  community.crypto.x509_certificate_info:
    path: "{{ cert_filename }}"
  register: result

- name: "Add cert to cmdb"
  include_role:
    name: cmdb-sync
  vars:
    cmdb_sync_object_type: 'certificate'
    cmdb_sync_vars:
      certificate_name: "{{ result.subject.commonName }}"
      certificate_expiry_date: "{{ (result.not_after | ansible.builtin.to_datetime('%Y%m%d%H%M%SZ')).strftime('%Y-%m-%dT%H:%M:%S') }}"
      certificate_fingerprint: "{{ result.fingerprints.sha256 }}"
      keystore_host: "{{ ansible_facts['default_ipv4']['address'] }}"
      keystore_path: "{{ trust_keystore_jks_path }}"
```