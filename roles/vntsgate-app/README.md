# VNTS gate install and configure

```bash
ansible-playbook -i inventories/oci02 -l prod_dmz_gate_app playbooks/vntsgate-app.yml --check -t config --diff -e "app_name=vntsjson" -v
```

Parameters:
# app_name - vnts, vntsjson
