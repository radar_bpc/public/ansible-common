# S3cmd client install and configure

## Install

```bash
ansible-playbook -i inventories/oci02 -l c02t1-wlsa01 playbooks/s3cmd.yml -e "s3cmd_user=svfe" --ask-vault-pass
```
Default parameters:
*   s3cmd_access_key: ''
*   s3cmd_secret_key: ''
*   s3cmd_bucket_location: 'eu-frankfurt-1'
*   s3cmd_host_base: 'frryjp3o23cl.compat.objectstorage.eu-frankfurt-1.oraclecloud.com'
*   s3cmd_user: 'weblogic'

# Usage 

```bash
  # Get file from OOS
    s3cmd get s3://sv/config/radar/2212070000065633.tar.gz /home/weblogic/sql/
  # List objects or buckets
    s3cmd ls [s3://BUCKET[/PREFIX]]
  # List all object in all buckets
      s3cmd la 
  # Put file into bucket
      s3cmd put FILE [FILE...] s3://BUCKET[/PREFIX]
  # Put folder into bucket or use sync operation
      s3cmd put --recursive LOCAL_DIR s3://BUCKET[/PREFIX] --progress --stats
      # or
      s3cmd sync LOCAL_DIR s3://BUCKET[/PREFIX] --progress --stats
  # Get file from bucket
      s3cmd get s3://BUCKET/OBJECT LOCAL_FILE
  # Delete file from bucket
      s3cmd del s3://BUCKET/OBJECT
  # Delete file from bucket (alias for del)
      s3cmd rm s3://BUCKET/OBJECT
  # Restore file from Archive storage ??? not tested yet
      s3cmd restore s3://BUCKET/OBJECT
  # Synchronize a directory tree to S3 (checks files freshness using size and md5 checksum, unless overridden by options, see below)
      s3cmd sync LOCAL_DIR s3://BUCKET[/PREFIX] or s3://BUCKET[/PREFIX] LOCAL_DIR or s3://BUCKET[/PREFIX] s3://BUCKET[/PREFIX]
  # Disk usage by buckets
      s3cmd du [s3://BUCKET[/PREFIX]]
  # Get various information about Buckets or Files
      s3cmd info s3://BUCKET[/OBJECT]
  # Copy object
      s3cmd cp s3://BUCKET1/OBJECT1 s3://BUCKET2[/OBJECT2]
  # Modify object metadata
      s3cmd modify s3://BUCKET1/OBJECT
  # Move object
      s3cmd mv s3://BUCKET1/OBJECT1 s3://BUCKET2[/OBJECT2]
  # Show multipart uploads
      s3cmd multipart s3://BUCKET [Id]
  # Abort a multipart upload
      s3cmd abortmp s3://BUCKET/OBJECT Id
  # List parts of a multipart upload
      s3cmd listmp s3://BUCKET/OBJECT Id
  # Enable/disable bucket access logging
      s3cmd accesslog s3://BUCKET
  # Sign an S3 URL to provide limited public access with expiry
      s3cmd signurl s3://BUCKET/OBJECT <expiry_epoch|+expiry_offset>
  # Fix invalid file names in a bucket
      s3cmd fixbucket s3://BUCKET[/PREFIX]
  # Create Website from bucket
      s3cmd ws-create s3://BUCKET
  # Delete Website
      s3cmd ws-delete s3://BUCKET
  # Info about Website
      s3cmd ws-info s3://BUCKET
  # Set or delete expiration rule for the bucket
      s3cmd expire s3://BUCKET
  # Upload a lifecycle policy for the bucket
      s3cmd setlifecycle FILE s3://BUCKET
  # Get a lifecycle policy for the bucket
      s3cmd getlifecycle s3://BUCKET
  # Remove a lifecycle policy for the bucket
      s3cmd dellifecycle s3://BUCKET
  # Upload a notification policy for the bucket
      s3cmd setnotification FILE s3://BUCKET
  # Get a notification policy for the bucket
      s3cmd getnotification s3://BUCKET
  # Remove a notification policy for the bucket
      s3cmd delnotification s3://BUCKET
```
