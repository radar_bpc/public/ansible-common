---

- name: Set sudo user
  ansible.builtin.set_fact:
    sudo_user: "{{ ansible_env.SUDO_USER | default('') }}"

- name: Removing users
  ansible.builtin.user:
    name: "{{ item }}"
    state: absent
  when: users_remove is defined
  with_items: "{{ users_remove }}"
  tags: ["remove"]

- name: Create primary group for user
  ansible.builtin.include_role:
    name: groups
  vars:
    role_groups:
      - name: "{{ item.group | default(item.name) }}"
        gid: "{{ item.uid | default(omit) }}"
        system: "{{ item.system | default(omit) }}"
  with_items: "{{ users }}"


- name: Create users
  ansible.builtin.user:
    name: "{{ item.name }}"
    group: "{{ item.group | default(item.name) }}"
    groups: "{{ item.groups | default('') }}"                               # if empty then remove from every group except primary group
    append: "no"                                                            # set group to jush the list in groups variable
    password: "{{ item.pass | default(omit) }}"
    createhome: "{{ item.createhome | default('yes') }}"                    # no - do nothing, or create homedir
    home: "{{ item.homedir | default('/home/' + item.name) }}"              # set homedir location
    shell: "{{ item.shell | default('/bin/bash') }}"
    uid: "{{ item.uid | default(omit) }}"
    system: "{{ item.system | default(omit) }}"
    update_password: always
  when:
    - item.name != sudo_user   # do not run for executing user: we can't change uid for existing processes
    - item.name != 'grid'
    - item.name != 'oracle'
    - item.name != 'root'
  #  ignore_errors: true
  with_items: "{{ users }}"

- name: Getting database passwd information
  ansible.builtin.getent:
    database: "passwd"
  tags: always


- name: Fixing file owner in homedir
  ansible.builtin.file:
    path: "{{ getent_passwd[item.name][4] }}"
    group: "{{ item.name }}"
    owner: "{{ item.name }}"
    state: "directory"
    recurse: true
  when: item.uid is defined and item.fix_homedir_owner is defined and item.fix_homedir_owner
  with_items: "{{ users }}"

- name: Fixing file SELinux context for home
  community.general.sefcontext:
    target: "{{ getent_passwd[item.name][4] }}"
    ftype: d
    seuser: "unconfined_u"
    setype: "user_home_dir_t"
  when:
    - item.uid is defined
    - ansible_selinux is defined and ansible_selinux and ansible_selinux.status == 'enabled'
  with_items: "{{ users }}"
  ignore_errors: true
  tags: selinux

- name: Fixing file SELinux context for .ssh
  community.general.sefcontext:
    target: "{{ getent_passwd[item.name][4] }}/.ssh"
    ftype: d
    seuser: "unconfined_u"
    setype: "ssh_home_t"
  when:
    - item.uid is defined
    - ansible_selinux is defined and ansible_selinux and ansible_selinux.status == 'enabled'
  with_items: "{{ users }}"
  ignore_errors: true
  tags: selinux

- name: Fixing file SELinux context for .ssh
  community.general.sefcontext:
    target: "{{ getent_passwd[item.name][4] }}/.ssh/"
    ftype: a
    seuser: "unconfined_u"
    setype: "ssh_home_t"
  when:
    - item.uid is defined
    - ansible_selinux is defined and ansible_selinux and ansible_selinux.status == 'enabled'
  with_items: "{{ users }}"
  ignore_errors: true
  tags: selinux

- name: Creating .ssh dir only for item.ssh_key_file or item.authorized_keys
  ansible.builtin.file:
    path: "{{ getent_passwd[item.name][4] }}/.ssh"
    owner: "{{ item.name }}"
    group: "{{ item.group | default(item.name) }}"
    setype: "ssh_home_t"
    state: "directory"
    mode: "0700"
  with_items: "{{ users }}"
  when:
    - item.ssh_key_file is defined or item.authorized_keys is defined
  ignore_errors: true

- ansible.builtin.import_tasks: "authorized_keys.yml"

- name: Creating subset users with ssh_key_file
  ansible.builtin.set_fact:
    users_with_ssh_key: "{{ users_with_ssh_key | default([]) + [item] }}"
  with_items: "{{ users }}"
  when: item.ssh_key_file is defined

- name: Template item.ssh_key_file
  ansible.builtin.template:
    src: "id_pub.j2"
    dest: "{{ getent_passwd[item.0.name][4] }}/.ssh/{{ item.1.name }}"
    owner: "{{ item.0.name }}"
    group: "{{ item.0.name }}"
    mode: "0600"
    backup: true
  with_subelements:
    - "{{ users_with_ssh_key }}"
    - ssh_key_file
  when: users_with_ssh_key is defined
  ignore_errors: true

#- import_tasks: "known_hosts.yml"
- ansible.builtin.import_tasks: "files.yml"
- ansible.builtin.import_tasks: "limits.yml"
