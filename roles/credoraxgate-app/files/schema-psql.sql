create table RESPONSE_LOG(
    T   DATE              not null,
    A1  VARCHAR(32) not null constraint RESPONSE_LOG_PK primary key,
    G2  VARCHAR(32),
    Z1  VARCHAR(32),
    Z4  VARCHAR(10),
    Z6  VARCHAR(3),
    Z13 VARCHAR(32),
    Z14 VARCHAR(1),
    Z30 VARCHAR(1),
    Z31 VARCHAR(4),
    Z33 VARCHAR(255),
    Z34 VARCHAR(255),
    Z39 VARCHAR(255),
    Z41 VARCHAR(255),
    Z50 VARCHAR(15),
    Z55 VARCHAR(32))
/
