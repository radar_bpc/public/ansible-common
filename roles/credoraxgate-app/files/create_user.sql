CREATE TABLESPACE CREDORAX LOGGING DATAFILE
    '+DATA'
    SIZE 200M
    REUSE
    AUTOEXTEND ON NEXT 20M
    MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL
    SEGMENT SPACE MANAGEMENT AUTO
    ENCRYPTION USING 'AES256'
    DEFAULT STORAGE(ENCRYPT);

create user credoraxgate
    identified by "credoraxgate1"
    default tablespace CREDORAX
    temporary tablespace TEMP
    profile APP_PROFILE
    account unlock
/

GRANT "CONNECT" TO credoraxgate;
GRANT "RESOURCE" TO credoraxgate;


GRANT UNLIMITED TABLESPACE TO credoraxgate;


GRANT SELECT ON "SYS"."V_$SESSION" TO credoraxgate;
GRANT SELECT ON "SYS"."DBA_PROFILES" TO credoraxgate;
GRANT SELECT ON "SYS"."DBA_USERS" TO credoraxgate;
GRANT EXECUTE ON "SYS"."DBMS_LOCK" TO credoraxgate;
GRANT EXECUTE ON "SYS"."DBMS_PIPE" TO credoraxgate;


ALTER USER credoraxgate DEFAULT ROLE ALL;
grant create view to credoraxgate;
grant create table to credoraxgate;
grant create sequence to credoraxgate;
grant create trigger to credoraxgate;
