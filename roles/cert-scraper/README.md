cert-scraper
===

Scrap certificates information from JKS

## Arguments

| Argument              | Type   | Default | Description     |
|-----------------------|--------|---------|-----------------|
| cert_scraper_jks_path | sring  |         | Path of JKS     |
| cert_scraper_jks_pass | string |         | Password of JKS |

## Example
```yaml
- name: Scrap certificates information
  hosts: c02t1-wlsa01
  vars:
    cert_scraper_jks_path: "/home/weblogic/OCI-Certs/trust.jks"
    cert_scraper_jks_pass: "notsosecret"
  roles:
    - cert-scraper
```