cert-issuer
===

Get, issue or reissue certificate using OCI CA

## Arguments

| Argument                          | Type    | Default | Description            |
|-----------------------------------|---------|---------|------------------------|
| cert_issuer_certificate_name      | sring   |         | Certificate name       |
| cert_issuer_certificate_not_after | string  |         | Certificate valid date |
| cert_issuer_compartment_name      | string  |         | CA compartment         |
| cert_issuer_reissue               | boolean | no      | Reissue certificate?   |

## Example

```yaml
- name: Certificate Issuer
  hosts: localhost
  gather_facts: no
  become: no

  vars:
    project: "RADAR"
    env_name: "DEV"

    cert_issuer_certificate_name: "pak-test-com-2"
    cert_issuer_certificate_not_after: "2025-06-05T00:00:00.000Z"
    cert_issuer_compartment_name: "RADAR-TST"
    cert_issuer_reissue: no

  roles:
    - cert-issuer
```

## Return values

| Variable                                   | Description        |
|--------------------------------------------|--------------------|
| certificate_issuer_ca_certificate          | CA certificate PEM |
| certificate_issuer_certificate             | Certificate PEM    |
| certificate_issuer_certificate_private_key | Private key PEM    |
