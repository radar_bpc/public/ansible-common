---

- name: Check required variables
  ansible.builtin.fail:
    msg: "Variable '{{ item }}' is not defined"
  when: item not in vars
  run_once: true
  loop: "{{ cert_issuer_create_required_variables }}"

- ansible.builtin.set_fact:
    certificate_object: "{{ crt_output.certificates[0] }}"

- name: Get list of CA
  oracle.oci.oci_certificates_management_certificate_authority_facts:
    compartment_id: "{{ cert_issuer_compartment_id }}"
    region: "{{ cert_issuer_region }}"
  register: ca_output

- name: Get key
  oracle.oci.oci_key_management_key_facts:
    compartment_id: "{{ cert_issuer_compartment_id }}"
    service_endpoint: "{{ cert_issuer_vault_management_endpoint }}"
  register: key_output

- name: Set facts
  ansible.builtin.set_fact:
    cert_issuer_ca_id: "{{ ca_output | json_query(cert_issuer_ca_query) | first }}"
    cert_issuer_key_id: "{{ key_output | json_query(cert_issuer_key_query) | first }}"

- name: Gather secret
  oracle.oci.oci_secrets_secret_bundle_facts:
    vault_id: "{{ cert_issuer_vault_id }}"
    secret_name: "{{ cert_issuer_oci_private_key }}"
    stage: CURRENT
    region: "{{ cert_issuer_region }}"
  register: secret_output

- name: Set private key fact
  ansible.builtin.set_fact:
    certificate_issuer_certificate_private_key: "{{ secret_output.secret_bundle.secret_bundle_content.content | b64decode }}"

- name: Generate certificate sign request
  community.crypto.openssl_csr:
    path: "{{ cert_issuer_csr_path }}"
    privatekey_content: "{{ certificate_issuer_certificate_private_key }}"
    common_name: "{{ cert_issuer_certificate_name }}"
    organization_name: "{{ cert_issuer_certificate_org_name }}"
    country_name: "{{ cert_issuer_certificate_country_name }}"
    state_or_province_name: "{{ cert_issuer_certificate_state_or_province_name }}"
    organizational_unit_name: "{{ cert_issuer_certificate_organizational_unit_name }}"
    subject_alt_name: "{{ cert_issuer_certificate_subject_alt_name }}"

- name: Reissue certificate
  oracle.oci.oci_certificates_management_certificate:
    certificate_id: "{{ certificate_object.id }}"
    certificate_config:
      issuer_certificate_authority_id: "{{ cert_issuer_ca_id }}"
      config_type: MANAGED_EXTERNALLY_ISSUED_BY_INTERNAL_CA
      csr_pem: "{{ lookup('file', cert_issuer_csr_path) }}"
      validity:
        time_of_validity_not_after: "{{ cert_issuer_certificate_not_after }}"
    region: "{{ cert_issuer_region }}"
    description: "Generates by Ansible"
  register: cert_issue_output

- name: Get a certificate_bundle
  oracle.oci.oci_certificates_certificate_bundle_facts:
    certificate_id: "{{ cert_issue_output.certificate.id }}"
    region: "{{ cert_issuer_region }}"
  register: crt_versions_output

- name: Set facts
  ansible.builtin.set_fact:
    certificate_issuer_ca_certificate: "{{ crt_versions_output.certificate_bundle.cert_chain_pem }}"
    certificate_issuer_certificate: "{{ crt_versions_output.certificate_bundle.certificate_pem }}"

- name: Cleanup
  ansible.builtin.file:
    path: "{{ cert_issuer_csr_path }}"
    state: absent
