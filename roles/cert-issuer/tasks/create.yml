---

- name: Check required variables
  ansible.builtin.fail:
    msg: "Variable '{{ item }}' is not defined"
  when: item not in vars
  run_once: true
  loop: "{{ cert_issuer_create_required_variables }}"

- name: Get list of CA
  oracle.oci.oci_certificates_management_certificate_authority_facts:
    compartment_id: "{{ cert_issuer_compartment_id }}"
    region: "{{ cert_issuer_region }}"
  register: ca_output

- name: Get key
  oracle.oci.oci_key_management_key_facts:
    compartment_id: "{{ cert_issuer_compartment_id }}"
    service_endpoint: "{{ cert_issuer_vault_management_endpoint }}"
  register: key_output

- name: Set facts
  ansible.builtin.set_fact:
    cert_issuer_ca_id: "{{ ca_output | json_query(cert_issuer_ca_query) | first }}"
    cert_issuer_key_id: "{{ key_output | json_query(cert_issuer_key_query) | first }}"

- name: Generate private key
  community.crypto.openssl_privatekey:
    path: "{{ cert_issuer_private_key_path }}"
    size: "{{ cert_issuer_private_key_size }}"
    type: RSA

- name: Generate certificate sign request
  community.crypto.openssl_csr:
    path: "{{ cert_issuer_csr_path }}"
    privatekey_path: "{{ cert_issuer_private_key_path }}"
    common_name: "{{ cert_issuer_certificate_name }}"
    organization_name: "{{ cert_issuer_certificate_org_name }}"
    country_name: "{{ cert_issuer_certificate_country_name }}"
    state_or_province_name: "{{ cert_issuer_certificate_state_or_province_name }}"
    organizational_unit_name: "{{ cert_issuer_certificate_organizational_unit_name }}"
    subject_alt_name: "{{ cert_issuer_certificate_subject_alt_name }}"

- name: Issue certificate
  oracle.oci.oci_certificates_management_certificate:
    name: "{{ cert_issuer_oci_certificate_name }}"
    compartment_id: "{{ cert_issuer_compartment_id }}"
    certificate_config:
      issuer_certificate_authority_id: "{{ cert_issuer_ca_id }}"
      config_type: MANAGED_EXTERNALLY_ISSUED_BY_INTERNAL_CA
      csr_pem: "{{ lookup('file', cert_issuer_csr_path) }}"
      validity:
        time_of_validity_not_after: "{{ cert_issuer_certificate_not_after }}"
    region: "{{ cert_issuer_region }}"
    description: "Generates by Ansible"
  register: cert_issue_output

- name: Get a certificate_bundle
  oracle.oci.oci_certificates_certificate_bundle_facts:
    certificate_id: "{{ cert_issue_output.certificate.id }}"
    region: "{{ cert_issuer_region }}"
  register: crt_versions_output

- name: Set facts
  ansible.builtin.set_fact:
    certificate_issuer_ca_certificate: "{{ crt_versions_output.certificate_bundle.cert_chain_pem }}"
    certificate_issuer_certificate: "{{ crt_versions_output.certificate_bundle.certificate_pem }}"
    certificate_issuer_certificate_private_key: "{{ lookup('file', cert_issuer_private_key_path) }}"

- name: Save private key in OCI Vault
  oracle.oci.oci_vault_secret:
    compartment_id: "{{ cert_issuer_compartment_id }}"
    secret_name: "{{ cert_issuer_oci_private_key }}"
    vault_id: "{{ cert_issuer_vault_id }}"
    secret_content:
      content_type: BASE64
      content: "{{ certificate_issuer_certificate_private_key | b64encode }}"
    key_id: "{{ cert_issuer_key_id }}"
    region: "{{ cert_issuer_region }}"

- name: Cleanup
  ansible.builtin.file:
    path: "{{ item }}"
    state: absent
  loop:
    - "{{ cert_issuer_private_key_path }}"
    - "{{ cert_issuer_csr_path }}"
