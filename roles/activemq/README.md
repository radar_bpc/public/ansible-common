# ActiveMQ

## Install
## Configuration  - use tag config or start_script
## Restart - use tag restart

Examples:
```shell
# Install
ansible-playbook -i inventories/oci02 -l prod_bus_app playbooks/activemq.yml

# Configuration 
ansible-playbook -i inventories/oci02 -l prod_bus_app playbooks/activemq.yml -t config --diff

# Update starting scripts
ansible-playbook -i inventories/oci02 -l prod_bus_app playbooks/activemq.yml -t start_script --diff

# Configuration and restart
ansible-playbook -i inventories/oci02 -l prod_bus_app playbooks/activemq.yml -t config,restart --diff


# Just restart (with cluster freeze/unfreeze)
ansible-playbook -i inventories/oci02 -l prod_bus_app playbooks/activemq.yml -t restart
```
