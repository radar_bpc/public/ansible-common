# Apply the logging configuration

For this config to work for BO-GUI and FE-GUI, you need to specify a new location for the configuration files in the internal GUI variables.

## BO GUI
For BO-GUI this is "Administration -> Setting -> Setting -> Tracing -> External log configuration file".

## BO-KAFKA
Add this line to the Server Start Arguments:
```bash
-Dlogging.config=/home/weblogic/sv_logs/conf/logback.xml
```
