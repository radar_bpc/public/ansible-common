trust-keystore
===

Create trusted keystore and import certificates from:

* OCI CA
* Additional S3
* Additional by URL

## Arguments

| Argument                            | Type         | Default                        | Description                                                  |
|-------------------------------------|--------------|--------------------------------|--------------------------------------------------------------|
| trust_keystore_general_path         | string       | "{{ wl_home }}/OCI-Certs"      | Root directory for keystore                                  |
| trust_keystore_owner                | string       | "{{ wl_user }}"                | Owner of keystore                                            |
| trust_keystore_group                | string       | "{{ wl_group }}"               | Group of keystore                                            |
| trust_keystore_jks_file             | string       | trust.jks                      | Filename for keystore                                        |
| trust_keystore_jks_password         | string       | "{{ wl_trust_keystore_pass }}" | Password for keystore                                        |
| trust_keystore_additional_s3_certs  | list(string) | []                             | List of additional certificates from OOS/S3 (s3://sv/CA/ssl) |
| trust_keystore_additional_url_certs | list(object) | []                             | List of host/port to retrieve certificate                    |
| trust_keystore_cmdb_sync_enabled    | bool         | false                          | Add certificate to DevOps-GUI                                |
