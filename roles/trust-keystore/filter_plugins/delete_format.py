#!/usr/bin/python

class FilterModule(object):

    DELIMITER = '.'

    def filters(self):
        return {
            'delete_format': self.delete_format
        }

    def delete_format(self, file_name):
        return self.DELIMITER.join(file_name.split(self.DELIMITER)[:-1])
