## DNS ZONES CONFIGURATION

This role communicates with the Oracle Cloud and adds, modifies or deletes DNS records in the internal private zone. The compartment id is taken from the metadata of the host being deployed.

This role is primarily intended to be called from other roles. For direct usage, use the following example, it also shows how to call this role from an external role:
```
- name: DNS configuration
  become: yes
  hosts: all
  gather_facts: no
  tasks:
  - name: Only test run
    include_role:
      name: dns-zones-config
    loop: "{{ dns_config }}"
    vars:
      dns_config:
      - zone_name: biz.lo
        fully_qualified_domain_name: mycooldomain.biz.lo
        target_ip_address: 8.8.8.8
        view_id: "ocid1.dnsview.oc1.eu-frankfurt-1.aaaaaaaa2eguonyzeww6l3atgd2knsst6pn6h7diqtrscjnhvfkefauj7kbq"
```

Since this is an embeddable role, execution control via tags is not suitable for it. Instead, variables `zone_record_update`, `zone_record_delete` and `zone_record_create` are used. The default is `zone_record_update`.

Behavior when creating an entry: If the entry did not exist, it will be created; if the entry did exist, a new entry of the same type will be created in addition to the existing one. This is the behavior of this OCI module.

Update behavior: If the entry existed, it will be deleted, then re-created with the new parameters. If the entry did not exist, an error occurred on deletion, but execution continues. Therefore, this behavior is used by default.

Deletion behavior: Only the delete step is executed, only the `fully_qualified_domain_name` variable is used.


## USAGE
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/dns-config -bK -e zone_record_update=yes
```
or
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/dns-config -bK -e zone_record_delete=yes
```
or
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/dns-config -bK
```
same
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/dns-config -bK -e zone_record_create=yes
```

Opportunities of OCI also allow you to delete and edit not only records of the zone, but also the zones themselves. If required, it can be implemented through the same Oracle collection tools. 


## TODO

Unfortunately, it has not yet been possible to implement obtaining VIEW_ID, since the Oracle module responsible for obtaining facts about VNIC simply does not work. So for now, you will also need to add the ID for VIEW, along with the IP address, zone, and FQDN, as you need to add the private zone to the correct network.

Over time, when new information about the operation of the failed module is received or Oracle fixes this module, it will be possible to implement automatic obtaining of VIEW_ID from the host VNICs metadata.

The `Get the views facts` step will show you all the views available for that section, along with its metadata. And the next step will pause the execution for a while, so you can check for the correct VIEW_ID. If this is not the case, then `Ctrl+C then 'A' = abort`, fix the variable and restart the play.


## EXAMPLES

Config for the cbsgate-app role:

Task in the role
```
- name: Make DNS record for cbs-remote
  include_role:
    name: dns-zones-config
  loop: "{{ dns_config }}"
  vars:
    dns_config: "{{ cbs_dns_config }}"
```

Variable in group_vars
```
cbs_dns_config: "{{ uat_cbs_dns_config }}"
```

Defenition in vars_project_all
```
uat_cbs_dns_config:
  - dns_conf_user: weblogic
    zone_name: hbl.ch
    fully_qualified_domain_name: sebfibaprd01.hbl.ch
    target_ip_address: "192.168.219.74"
    view_id: "ocid1.dnsview.oc1.eu-frankfurt-1.aaaaaaaaj3dr2qmkzqesuiuuq2hqhcwyblq37ucetwbz63iqihzpm6jsywca"
```
