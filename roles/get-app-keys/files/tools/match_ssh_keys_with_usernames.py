#!/bin/env python3

import yaml
import os

def parse_yaml_keys(file_path):
    """Load SSH keys from a YAML file."""
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
    return {value.split()[1]: key for key, value in data['users_ssh_keys'].items()}

def parse_authorized_keys(path):
    """Read SSH keys from the authorized_keys file and strip comments."""
    with open(path, 'r') as file:
        keys = []
        for line in file:
            if line.startswith('ssh-') or line.startswith(' ssh-'):
                line = line.strip()
                key_cipher = line.split(' ')[0].strip()
                key = line.split(' ')[1].strip()
                if key:
                    keys.append(f"{key_cipher} {key}")
        return keys

def save_to_yaml(data, filename='output.yaml'):
    """Save the data to a YAML file."""
    with open(filename, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)

def main(directory='/tmp/ssh_keys/', yaml_path='roles/vars_project_all/defaults/main/users_and_groups/ssh_keys.yml'):
    yaml_keys = parse_yaml_keys(yaml_path)
    results = {}
    for root, _, files in os.walk(directory):
        for file in files:
            if file == 'authorized_keys':
                path = os.path.join(root, file)
                keys = parse_authorized_keys(path)
                parts = path.split('/')
                project = parts[3]
                instance = parts[4]
                user = parts[6]
                for key in keys:
                    username = yaml_keys.get(key.split()[1], '')
                    if username:
                        if user not in results:
                            results[user] = {}
                        if project not in results[user]:
                            results[user][project] = {}
                        if instance not in results[user][project]:
                            results[user][project][instance] = []
                        if username not in results[user][project][instance]:
                            results[user][project][instance].append(username)
    
    save_to_yaml({'user': results})

if __name__ == '__main__':
    main()