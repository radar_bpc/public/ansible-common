CREATE TYPE _timescaledb_catalog.ts_interval AS (is_time_interval BOOLEAN, time_interval INTERVAL, integer_interval BIGINT);
ALTER TYPE _timescaledb_catalog.ts_interval OWNER TO zabbix;

CREATE OR REPLACE VIEW _timescaledb_config.bgw_policy_compress_chunks AS
SELECT job_id,(config->>'hypertable_id')::INTEGER AS hypertable_id,
       (FALSE,NULL,config->>'compress_after')::_timescaledb_catalog.ts_interval AS older_than
FROM timescaledb_information.jobs WHERE proc_name='policy_compression';
ALTER VIEW _timescaledb_config.bgw_policy_compress_chunks OWNER TO zabbix;

CREATE OR REPLACE FUNCTION add_compress_chunks_policy
(hypertable regclass,older_than anyelement,if_not_exists BOOLEAN DEFAULT FALSE)
    RETURNS INTEGER LANGUAGE sql AS $$
SELECT add_compression_policy(hypertable,older_than,if_not_exists)
    $$;
ALTER FUNCTION add_compress_chunks_policy OWNER TO zabbix;

CREATE OR REPLACE FUNCTION drop_chunks
(older_than anyelement DEFAULT NULL::UNKNOWN,table_name NAME DEFAULT NULL::NAME)
    RETURNS SETOF TEXT LANGUAGE sql AS $$
SELECT drop_chunks(table_name::regclass,older_than,NULL,FALSE)
    $$;
ALTER FUNCTION drop_chunks(anyelement,NAME) OWNER TO zabbix;
