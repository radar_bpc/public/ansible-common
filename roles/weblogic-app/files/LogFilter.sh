#!/bin/bash

# Filter Weblogic logs with STUCK threads, Deadlocks or Unchecked Exception.
# Usages:
#To run per log file run like below:
#./LogFilter.sh AdminServer.log
#
#Not Recommended(but will work):
#./LogFilter.sh AdminServer.log ManagedServer.log ManagedServer1.log


LOGNAMES=$@

Stuck() {
    grep -A4 "" "${LOGNAME}" | grep -E -A4 "WL-000337|BEA-000337|WL-101020|WL-101017|WL-000802|WL-101020|BEA-101017"
}

DeadLock() {
    grep -E -A4 "|" "$LOGNAME" | grep -E -A4 "WL-000394|BEA-000394"
}

UncheckedException() {
    grep -A4 "" "$LOGNAME" | grep -E -A4 "WL-000337|BEA-000337"
}

Main() {
    Stuck
    DeadLock
    UncheckedException
}
for i in $@; do
    export LOGNAME=$i
    if test "${LOGNAME}"; then
        Main >> Diagnostics.log
        cat Diagnostics.log | uniq > tmpfile && mv tmpfile Diagnostics.log
    else
        echo "Please provide the logfile name to search."
    fi
done
