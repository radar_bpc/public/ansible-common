#!/bin/bash

#####################################################
# The script runs several java utilities, for
# automatic assembly of java diagnostic information
# on the Weblogic server.
# Loaded in background: jstack, jstat, jmap.
#
# Options:
# PID - required first parameter (process number)
# -l - do not pack logs along with dumps
# -d --debug - show values (for debugging)
# -k --kill - kill script processes
# --no-dump - no heap dump instruction for jmap
# --no-rm - do not delete temporary files after creating the archive
# --no-upload - do not upload files to OOS
# --dry-run - run without actually collecting dumps, only
#              creates files and archive
# v3.3
#####################################################

#-----------CONFIG-----------#
#---environment---#

PROJECT='{{ project }}'
HD='/appdata/diagnostic_info'
WD="${HD}/tmp"
DUMP_DIR="${HD}/dumps"
JAVA_HOME='{{ wl_java_home }}'
WL_HOME="{{ wl_version.wl_oracle_home }}/user_projects/domains/{{ domain.domain_name }}"
CONTROLLER_LOG="${WL_HOME}/servers/AdminServer/logs"
SERVERS_LOG="${WL_HOME}/servers/"
wasted=0
host=$(uname -n)
date=$(date +'%d%m%Y')
dry_run=0
zip_log=1

#---prompt---#
java_pid=$1

if [[ $1 == "-k" ]]; then
  pkill -f $0
elif [[ $1 == "--kill" ]]; then
  pkill -f $0
fi

for i in {$2,$3,$4,$5,$6,$7}; do
  case $i in
  "--no-dump")
    jmap_dump=1
    ;;
  "-l")
    zip_log=0
    ;;
  "--no-rm")
    no_rm=1
    ;;
  "-d")
    debug_mode=1
    ;;
  "--debug")
    debug_mode=1
    ;;
  "--dry-run")
    dry_run=1
    ;;
  "--no-upload")
    no_upload=1
    no_rm=1
    ;;
  *)
    echo "Wrong parameter! Exit."
    exit 1
    ;;
  esac
done

# Describe active server
jvm_server=$(ps x | grep $java_pid)
for i in ${jvm_server[@]}; do if [[ $i == *"weblogic.Name"* ]]; then jvm_server=$i; fi; done
jvm_server=$(echo ${jvm_server#*D} | cut -f2 -d=)
if [ "x${jvm_server}" = "x" ]; then
  jvm_server=$(ps x | grep $java_pid)
  jvm_server=$(echo ${jvm_server} | cut -f2 -d'[' | cut -d' ' -f1)
  jvm_server=$(echo "${jvm_server}" | awk '{print tolower($0)}')
fi
file_format="$java_pid.$jvm_server.$host.$date"
if [[ "$dry_run" == 1 ]]; then
  attempts=1
else
  attempts=20
fi
#---jstack-vars---#
jstack_file="$WD/jstack.threads_of_$file_format"
part_file="$WD/part.jstack.threads_of_$file_format"
if [[ "$dry_run" == 1 ]]; then
  jstack_itr_time=1
else
  jstack_itr_time=60
fi
jstack_count=4

#---jstat-vars---#
jstat_file="${WD}/jstat.GCanalize_of_$file_format"

if [[ "$dry_run" == 1 ]]; then
  jstat_log_time=1
else
  jstat_log_time=200
fi

if [[ "$dry_run" == 1 ]]; then
  jstat_chk_time=1
else
  jstat_chk_time=60
fi

#---jmap-vars---#
jmap_file="$WD/jmap.history_of_$file_format"
jdump_file="${DUMP_DIR}/heapdump.hprof_of_$file_format"

if [[ "$dry_run" == 1 ]]; then
  jmap_itr_time=1
else
  jmap_itr_time=60
fi

jmap_count=4
rows_in_histo=200

if [[ "$dry_run" == 1 ]]; then
  dont_start_dump_till=1
else
  dont_start_dump_till=120
fi

#---finalization---#

archive="${HD}/metrics_of_${file_format}.tar.bz2"

#----------------------------#

#----------Colors----------#
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)
#--------------------------#

#----------Strings----------#
line_1='==================='
line_2='*******************'
inf="${GREEN}INFO:${NORMAL}"
err="${RED}ERROR:${NORMAL}"
dbg="${WHITE}DEBUG:${NORMAL}"
ps_msg="   PID    TID %%CPU %%MEM STAT START     TIME CMD \n"
#---------------------------#

clear
if [[ "$debug_mode" == 1 ]]; then
  printf "$dbg Script working directory: ${WHITE}$WD${NORMAL}\n"
  printf "$dbg Java home directory: ${WHITE}$JAVA_HOME${NORMAL}\n"
  printf "$dbg PID of the process under study: ${WHITE}$java_pid${NORMAL}\n"
  printf "$dbg Server process name: ${WHITE}$jvm_server${NORMAL}\n"
  printf "$dbg Jstack statistics file: ${WHITE}$jstack_file${NORMAL}\n"
  printf "$dbg Interval between taking jstack statistics: ${WHITE}$jstack_itr_time${NORMAL}\n"
  printf "$dbg Jstat statistics file: ${WHITE}$jstat_file${NORMAL}\n"
  printf "$dbg Period for collecting jstat statistics (sec): ${WHITE}$jstat_log_time${NORMAL}\n"
  printf "$dbg Number of jstack passes: ${WHITE}$jstack_count${NORMAL}\n"
  printf "$dbg Jmap statistics file: ${WHITE}$jmap_file${NORMAL}\n"
  printf "$dbg Interval between taking jmap statistics (sec): ${WHITE}$jmap_itr_time${NORMAL}\n"
  printf "$dbg Number of attempts to withdraw if the socket is not opened: ${WHITE}$attempts${NORMAL}\n"
  printf "$dbg Number of jmap passes: ${WHITE}$jmap_count${NORMAL}\n"
  printf "$dbg Number of lines in history jmap -histo: ${WHITE}$rows_in_histo${NORMAL}\n"
  printf "$dbg Full process memory dump file: ${WHITE}$jdump_file${NORMAL}\n"
  printf "$dbg Waiting for statistics collection to finish before dumping (sec): ${WHITE}$dont_start_dump_till${NORMAL}\n"
  printf "$dbg The archive into which the resulting files will be packed, except for the full dump: ${WHITE}$archive${NORMAL}\n"
  printf "$dbg dry_run = ${WHITE}$dry_run${NORMAL}\n"
fi

mkdir -p {$WD,$DUMP_DIR} 2>/dev/null

[[ -z "$java_pid" ]] && printf "$err No Java PID specified! Exit...\n" && exit 1

function get_server_log() {
  for i in $(ls -1 ${SERVERS_LOG}/${jvm_server}/logs/*${jvm_server}.log*); do
    cp $i $WD/${file_format}_$(basename $i); done
  if [[ $? == 0 ]]; then
    for i in $(ls -1 ${SERVERS_LOG}/${jvm_server}/logs/*${jvm_server}.log*); do
      printf "$inf Log copied $i\n"; done
  fi
  for i in $(ls -1 ${SERVERS_LOG}/${jvm_server}/logs/${jvm_server}.out*); do
    cp $i $WD/${file_format}_$(basename $i); done
  if [[ $? == 0 ]]; then
    for i in $(ls -1 ${SERVERS_LOG}/${jvm_server}/logs/${jvm_server}.out*); do
      printf "$inf Log copied $i\n"; done
  fi
}

function get_controller_log() {
  cp $CONTROLLER_LOG/AdminServer.log $WD/${file_format}.log
  if [[ $? == 0 ]]; then
    printf "$inf Log copied $WD/$file_format.log\n"
  fi
}

function check() {
  if [[ $? == 1 ]]; then
    if [[ $wasted -eq $attempts ]]; then
      printf "$err Completed when the attempt counter was reached.\n"
      exit 1
    fi
    let wasted+=1
    let i-=1
    let remained=$attempts-$wasted
    printf "$err Cannot open process socket, process is not responding...\n"
    printf "$inf Remaining attempts: $remained\n"
    return 1
  else
    return 0
  fi
}

if [[ "$zip_log" == 1 ]]; then
  if [ $jvm_server = "host" ]; then
    get_controller_log
  elif [ $jvm_server = "process" ]; then
    get_controller_log
  else
    get_server_log
  fi
fi

(
  printf "$inf ${MAGENTA}jstack${NORMAL} started working in the background...\n"
  printf "$inf ${MAGENTA}jstack${NORMAL} interval is $jstack_itr_time sec, ${RED}WL_PID=$java_pid${NORMAL}\n"
  for ((i = 1; i <= $jstack_count; i++)); do
    printf "$inf ${MAGENTA}jstack${NORMAL} is doing a threaddump $i of $jstack_count...\n"
    date >>$jstack_file
    printf "$ps_msg" >>$jstack_file
    if [[ "$dry_run" == 1 ]]; then
      echo "dry-run" >>$jstack_file
    else
      ps --no-headings -p $java_pid -Lo pid,tid,%cpu,%mem,stat,start_time,time,ucmd | sort -nrk 3 | head -20 >>$jstack_file
    fi
    printf "$line_1$line_1$line_1\n" >>$jstack_file
    if [[ "$dry_run" == 1 ]]; then
      echo "dry-run" >>$jstack_file
    else
      $JAVA_HOME/bin/jstack -l $java_pid >>$jstack_file 2>/dev/null
    fi
    check
    if [[ $? == 0 ]]; then
      time=$(date +'%H:%M:%S')
      if [[ "$dry_run" == 1 ]]; then
        echo "dry-run" >>$part_file.$time
      else
        $JAVA_HOME/bin/jstack -l $java_pid >>$part_file.$time 2>/dev/null
      fi
      if [[ $? == 0 ]]; then
        printf "$inf A part file has been saved for use in analyzers: $part_file.$time\n"
      fi
    fi
    printf "$line_1$line_1$line_1\n" >>$jstack_file
    sleep $jstack_itr_time
  done
  printf "$inf ${MAGENTA}jstack${NORMAL} finished, threaddump file created: $jstack_file\n"
) &

(
  printf "$inf ${YELLOW}jstat${NORMAL} started writing GC statistics in the background...\n"
  date >>$jstat_file
  printf "$line_1$line_1$line_1\n" >>$jstat_file
  if [[ "$dry_run" == 1 ]]; then
    echo "dry-run" >>$jstat_file
  else
    $JAVA_HOME/bin/jstat -gccause $java_pid 2000 0 >>$jstat_file
  fi
) &

(
  k=1
  sleep $jstat_chk_time
  jstat_is_alive=$(pgrep jstat)
  if [[ "$debug_mode" == 1 ]]; then
    printf "$dbg jstat_is_alive = ${WHITE}$jstat_is_alive${NORMAL}\n"
  fi
  while [[ ! ( -z "$jstat_is_alive" ) ]]; do
    mod=$(expr $k \* 6000)
    percent=$(expr $mod / $jstat_log_time)
    printf "$inf ${YELLOW}jstat${NORMAL} is dumping GC stats: $percent%%\n"
    if [[ "$debug_mode" == 1 ]]; then
      printf "$dbg mod = ${WHITE}$mod${NORMAL}\n"
      printf "$dbg percent = ${WHITE}$percent${NORMAL}\n"
    fi
    let k+=1
    sleep $jstat_chk_time
    jstat_is_alive=$(pgrep jstat)
  done
) &

(
  sleep $jstat_log_time
  kill $(pgrep jstat)
  printf "$inf ${YELLOW}jstat${NORMAL} completed, file created: $jstat_file\n"
) &

(
  printf "$inf ${BLUE}jmap${NORMAL} started writing a log of heap objects in the background...\n"
  for ((i = 1; i <= $jmap_count; i++)); do
    printf "$inf ${BLUE}jmap${NORMAL} is collecting heap-top history, pass $i of $jmap_count...\n"
    date >>$jmap_file
    printf "$line_1$line_1$line_1\n" >>$jmap_file
    if [[ "$dry_run" == 1 ]]; then
      echo "dry-run" >>$jmap_file
    else
      $JAVA_HOME/bin/jmap -histo $java_pid | head -$rows_in_histo >>$jmap_file 2>/dev/null
    fi
    check
    sleep $jmap_itr_time
  done
  printf "$inf ${BLUE}jmap${NORMAL} completed, file created: $jmap_file\n"
) &

if [[ "$jmap_dump" == 1 ]]; then
  :
else
  (
    sleep $dont_start_dump_till
    for ((i = 0; i < 1; i++)); do
      printf "$inf ${BLUE}jmap${NORMAL} started writing a full ${POWDER_BLUE}heap dump${NORMAL} to a binary file...\n"
      if [[ "$dry_run" == 1 ]]; then
        echo "dry-run" >>$jdump_file
      else
        $JAVA_HOME/bin/jmap -dump:format=b,file=$jdump_file $java_pid 1>/dev/null 2>/dev/null
      fi
      check
    done
    test $jdump_file
    if [[ $? == 1 ]]; then
      printf "$err Full dump failed.\n"
    else
      printf "$inf ${BLUE}jmap${NORMAL} finished doing a ${POWDER_BLUE}heap dump${NORMAL}. Dump file: $jdump_file\n"
    fi
  ) &
fi

wait
cd $WD

list=$(ls $WD/*$file_format*)

if [[ "$debug_mode" == 1 ]]; then
  for i in $list; do
    printf "$dbg ${WHITE}$i${NORMAL}\n"
  done
fi
printf "$inf Packing archive...\n"
tar -cjf $archive $list

if [[ "$no_rm" != 1 ]]; then
  rm -f $list
  if [[ $? == 0 ]]; then
    printf "$inf Temporary files removed.\n"
  fi
fi

printf "$line_2\n"
test $archive
if [[ $? == 0 ]]; then
  IFS='/' read -ra ARCH_PATH <<< $archive
  file_name=${ARCH_PATH[-1]}
  if [[ "$debug_mode" == 1 ]]; then
    printf "$dbg Name in OCI bucket ${WHITE}$PROJECT/$file_name${NORMAL}"
    printf "$dbg Full name to upload ${WHITE}$archive${NORMAL}"
  fi
  if [[ "$no_upload" != 1 ]]; then
    oci os object put -bn dump_metrics --name "$PROJECT/$file_name" --file $archive
    oci os preauth-request create --access-type ObjectRead --bucket-name dump_metrics --name "$PROJECT/$file_name" --time-expires $(date -d "+14 days" +"%Y-%m-%d")
    printf "$inf Metrics has been uploaded to $PROJECT/$file_name\n"
    if [[ "$no_rm" != 1 ]]; then
      rm -f $archive
    else
      printf "$inf Metrics collected and archived $archive\n"
    fi
  fi
fi
if [[ "$jmap_dump" == 1 ]]; then
  :
else
  test $jdump_file
  if [[ $? == 0 ]]; then
    IFS='/' read -ra DUMP_PATH <<< $jdump_file
    file_name=${DUMP_PATH[-1]}
    if [[ "$debug_mode" == 1 ]]; then
      printf "$dbg Name in OCI bucket ${WHITE}$PROJECT/$file_name${NORMAL}"
      printf "$dbg Full name to upload ${WHITE}$jdump_file${NORMAL}"
    fi
    if [[ "$no_upload" != 1 ]]; then
      oci os object put -bn dump_metrics --name "$PROJECT/$file_name" --file $jdump_file
      oci os preauth-request create --access-type ObjectRead --bucket-name dump_metrics --name "$PROJECT/$file_name" --time-expires $(date -d "+14 days" +"%Y-%m-%d")
      printf "$inf ${POWDER_BLUE}heap dump${NORMAL} has been uploaded to $PROJECT/$file_name\n"
      if [[ "$no_rm" != 1 ]]; then
        rm -f $jdump_file
      else
        printf "$inf Full dump saved to $jdump_file\n"
      fi
    fi
  else
    printf "$err Full dump not saved.\n"
  fi
fi
printf "$line_2\n"

exit 0
