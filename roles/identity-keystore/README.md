identity-keystore
===

Create identity keystore and import certificates

## Arguments

| Argument                                  | Type   | Default                                                                                                                 | Description                   |
|-------------------------------------------|--------|-------------------------------------------------------------------------------------------------------------------------|-------------------------------|
| identity_keystore_path                    | string | "{{ wl_home }}/OCI-Certs"                                                                                               | Root directory for keystore   |
| identity_keystore_identity_pass           | string | "{{ wl_identity_keystore_pass }}"                                                                                       | Owner of keystore             |
| identity_keystore_type                    | string | jks                                                                                                                     | Type of keystore              |
| identity_keystore_certificate_common_name | string | for singlenode: "{{ inventory_hostname }}"<br/>for cluster: "{{ inventory_hostname \| regex_replace('[0-9]{2}$','') }}" | Certificate common name       |
| identity_keystore_certificate_alias       | string | weblogic                                                                                                                | Certificate alias in keystore |