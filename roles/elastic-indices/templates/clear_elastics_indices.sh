#!/bin/bash

ARR=($(curl -sk {{ elasticsearch_opensearch_endpoint }}/_cat/indices  | awk '{print $3}' | grep svip))

TODATE=$(date +%s)

for i in ${ARR[@]}; do
  DATE="$(echo $i | rev | cut -c 1-10 | rev)"
  COND=$(date "-d $DATE {{ elasticsearch_indices_days_remind }} days" +%s)

  if [ $TODATE -ge $COND ]; then
    curl -sk -X DELETE {{ elasticsearch_opensearch_endpoint }}/$i
    echo DELETE $i
  else
    echo REMIND $i
  fi

done