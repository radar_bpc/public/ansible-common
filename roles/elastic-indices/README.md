## ELASTICSEARCH INDICES

This role is expected to manage various operations related to ElasticSearch indexes.

At the moment, only a simple cleanup of stale indexes is used.

### Usage
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/elastic-indices.yml -bK
```

But it is more preferable to include this role in other playbooks that prepare ElasticSearch for use.