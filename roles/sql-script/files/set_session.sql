declare
    l_session number;
begin
    com_ui_user_env_pkg.set_user_context(
         i_user_name   => 'admin'
        , io_session_id => l_session
    );
    dbms_output.put_line('Session ' || l_session);
end;
/
commit;

