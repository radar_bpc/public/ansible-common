DECLARE
  my_sql_id    VARCHAR2(30) := &sql_id;
  my_task_name VARCHAR2(30);
BEGIN
  begin
     DBMS_SQLTUNE.DROP_TUNING_TASK(my_sql_id);
     exception when others then NULL;
  end;
  my_task_name := DBMS_SQLTUNE.CREATE_TUNING_TASK(
          sql_id      => my_sql_id,
        --sql_text    => 'select ...', --if SQL not in shared pool--
          scope       => 'COMPREHENSIVE',
          time_limit  => 600,
          task_name   => my_sql_id,
          description => 'SQL analysis for SQL_ID=' || my_sql_id);
  DBMS_SQLTUNE.EXECUTE_TUNING_TASK(task_name => my_task_name);
END;

set pagesize 5000
set linesize 120
set long 50000
set longchunksize 500000

SELECT DBMS_SQLTUNE.REPORT_TUNING_TASK( &sql_id ) FROM DUAL;

begin
dbms_sqltune.accept_sql_profile(task_name => &sql_id,
                                replace => TRUE,
                                force_match => true);
end;
/
