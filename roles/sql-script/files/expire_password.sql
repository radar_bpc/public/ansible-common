set pagesize 500
set linesize 200
set trimspool on
column "EXPIRE DATE" format a20
select username as "USER NAME", expiry_date as "EXPIRE DATE", account_status
from dba_users
where expiry_date < sysdate+190
and account_status IN ( 'OPEN', 'EXPIRED(GRACE)' )
order by account_status, expiry_date, username
/
