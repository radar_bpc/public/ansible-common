SET TERMOUT OFF
DEFINE prompt = "SQL> "
COLUMN global_name NEW_VALUE prompt
SELECT LOWER(USER) || '@' ||
       DECODE (SIGN(INSTR(global_name, '.')),
          1, LOWER(SUBSTR(global_name, 1, INSTR(global_name,'.')-1)),
          /* else */ LOWER(global_name)
       )||'> ' global_name
FROM global_name;

SET SQLPROMPT '&prompt'
UNDEFINE prompt
COLUMN global_name CLEAR
SET TERMOUT ON
