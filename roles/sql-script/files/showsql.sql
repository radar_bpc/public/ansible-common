COLUMN status format a10
SET feedback off
SET serveroutput on

SELECT
  username,
  sid,
  serial#,
  process,
  status
FROM v$session
WHERE username IS NOT NULL
/

COLUMN username format a20
COLUMN sql_text format a55 word_wrapped

SET serveroutput on size 1000000
DECLARE
  x NUMBER;
BEGIN
  FOR x IN
  ( SELECT
      username || '(' || sid || ',' || serial# ||
      ') ospid = ' || process ||
      ' program = ' || program            username,
      to_char(LOGON_TIME, ' Day HH24:MI') logon_time,
      to_char(sysdate, ' Day HH24:MI')    current_time,
      sql_address,
      LAST_CALL_ET
    FROM v$session
    WHERE status = 'ACTIVE'
          AND rawtohex(sql_address) <> '00'
          AND username IS NOT NULL
    ORDER BY last_call_et )
  LOOP
    FOR y IN ( SELECT max(decode(piece, 0, sql_text, NULL)) ||
                      max(decode(piece, 1, sql_text, NULL)) ||
                      max(decode(piece, 2, sql_text, NULL)) ||
                      max(decode(piece, 3, sql_text, NULL))
      sql_text
               FROM v$sqltext_with_newlines
               WHERE address = x.sql_address
                     AND piece < 4)
    LOOP
      IF (y.sql_text NOT LIKE '%listener.get_cmd%' AND
          y.sql_text NOT LIKE '%RAWTOHEX(SQL_ADDRESS)%')
      THEN
        dbms_output.put_line('--------------------');
        dbms_output.put_line(x.username);
        dbms_output.put_line(x.logon_time || ' ' ||
                             x.current_time ||
                             ' last et = ' ||
                             x.LAST_CALL_ET);
        dbms_output.put_line(
            substr(y.sql_text, 1, 250));
      END IF;
    END LOOP;
  END LOOP;
END;
/

COLUMN username format a15 word_wrapped
COLUMN module format a15 word_wrapped
COLUMN action format a15 word_wrapped
COLUMN client_info format a30 word_wrapped

SELECT
  username || '(' || sid || ',' || serial# || ')' username,
  module,
  action,
  client_info
FROM v$session
WHERE module || action || client_info IS NOT NULL;
