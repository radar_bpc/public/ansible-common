select owner,sum(bytes) / 1024/1024 "Size in Mb"
from dba_segments
where owner = upper('&owner')
group by owner 
order by owner;
