declare
    my_task_name varchar2 (30) ;
    my_sqltext clob;
begin
    my_sqltext   := 'SELECT *
FROM (
SELECT E.BALANCE , M.ID MACROS_ID , M.MACROS_TYPE_ID , M.AMOUNT , M.POSTING_DATE , E.STTL_DAY , C.PRODUCT_ID , E.SPLIT_HASH , E.BALANCE_TYPE , A.CURRENCY , A.INST_ID , NVL(T.AVAL_ALGORITHM, :B5 ) AVAL_ALGORITHM
FROM ACC_MACROS M , ACC_ENTRY E , ACC_ACCOUNT A , PRD_CONTRACT C , ACC_PRODUCT_ACCOUNT_TYPE T
WHERE M.MACROS_TYPE_ID = :B4 AND M.ENTITY_TYPE = :B3 AND M.OBJECT_ID = :B2 AND E.MACROS_ID = M.ID AND M.ACCOUNT_ID = A.ID AND A.CONTRACT_ID = C.ID AND T.PRODUCT_ID = C.PRODUCT_ID AND T.ACCOUNT_TYPE = A.ACCOUNT_TYPE ORDER BY DECODE(E.BALANCE_TYPE, :B1 , 1, 2) , E.POSTING_ORDER DESC )
WHERE ROWNUM = 1=';
    my_task_name :=
        dbms_sqltune.create_tuning_task (
            sql_text => my_sqltext
          , bind_list => sql_binds (anydata.convertnumber (123) , anydata.convertnumber (234))
          , user_name => user
          , task_name => 'my_sql_tt'
          , description => 'Example of using create tuning task') ;

    dbms_sqltune.execute_tuning_task ('my_sql_tt') ;
end;
select
    dbms_sqltune.report_tuning_task ('my_sql_tt')
from
    dual begin dbms_sqltune.drop_tuning_task ('my_sql_tt') ;
end;
/
