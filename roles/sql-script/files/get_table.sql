set echo off
set heading off
set feedback off
set verify off
set pagesize 0
set linesize 132

EXECUTE DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'PRETTY',true);
EXECUTE DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'SQLTERMINATOR',true);
SELECT dbms_lob(DBMS_METADATA.GET_DDL ('TABLE', table_name, user)) FROM user_tables WHERE table_name=upper('&1');
