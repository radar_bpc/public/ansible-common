select 'alter ' ||
       decode(object_type, 
           'PACKAGE BODY', 'PACKAGE',
           'TYPE BODY', 'TYPE',
           object_type) || ' ' || 
       object_name || ' compile'||
       decode(object_type, 
           'PACKAGE BODY', ' BODY;',
           'TYPE BODY', ' BODY;',
           ';')  as "Command line"
    from
        user_objects
    where
        object_type in ('PACKAGE' ,'PACKAGE BODY', 'TYPE', 'TYPE BODY', 'VIEW', 'PROCEDURE', 'MATERIALIZED VIEW', 'TRIGGER', 'FUNCTION') 
    and status  != 'VALID'
    order by 1;
