select * from (select 
    substr(sql_text, 1 ,500) "SQL"
  , (cpu_time/1000000) as "CPU_Seconds"
  , disk_reads as "Disk_reads"
  , buffer_gets as "Buffer_gets"
  , executions "Executions"
  , case 
        when rows_processed = 0 then null
        else round((buffer_gets/nvl(replace(rows_processed,0,1),1)))
    end "Buffer_gets/rows_proc"
  , round((buffer_gets/nvl(replace(executions,0,1),1))) "Buffer_gets/executions"
  , (elapsed_time/1000000) "Elapsed_Seconds"
  , module "Module"
from
    V$SQL S
--order by elapsed_time)
order by executions desc nulls last)
where rownum <= 10 
/

