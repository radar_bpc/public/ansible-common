begin
   dbms_stats.gather_schema_stats(
      ownname          => user, 
      estimate_percent => dbms_stats.auto_sample_size, 
      method_opt       => 'for all columns size auto', 
      degree           => 7
   );
end;
/
