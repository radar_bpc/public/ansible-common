column file_name format a50
column tablespace_name format a14
column free_space format 9999999.9999

select ddf.file_name
     , ddf.tablespace_name
     , sum(dfs.bytes)/1024/1024 "free space in mb"
  from dba_data_files ddf, dba_free_space dfs
 where ddf.file_id = dfs.file_id
   and ddf.tablespace_name like upper('&tbs_name')
group by ddf.file_name,ddf.tablespace_name;

