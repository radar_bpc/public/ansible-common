set long 20000;
set pagesize 2000;
set linesize 2000;
spool object.sql
select dbms_metadata.get_ddl(decode(lower('&type'),'spec','PACKAGE','body','PACKAGE_BODY', 'func','FUNCTION','proc', 'PROCEDURE', 'view', 'VIEW' ),upper('&package_name')) as src  from dual;
spool off;

