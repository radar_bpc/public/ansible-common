# INSTALLATION OF MERCHANT PORTAL:
1.  Is a VM created? No -> Architect -> Sysadmins team
1a. Is the host ready after creation? No -> Spill prepare-new-os and users-and-groups roles.
2.  Is PDB created? No -> DBA -> 3.
2a. Are roles and priviliges granted to SVMP_INSTALL? No -> See https://drive.google.com/drive/folders/15usGClwcApIZB5EnPEV8NZp-adt6LEGO -> DBA
2b. Table spaces should have the names MPTBS for SVMP and MPIGTBS form SVMP_IG. Reference -> https://jira.bpcbt.com/browse/B_PSGB-16423
3.  Is subnet from VM to DB opened? No -> Network team

## MERCHANT PORTAL CONFIG ROLE

This role does:
- creates home directory in Weblogic home
- copyes artifacts from OOS
- provides config (which included in release .zip archives)


## TODO

In release .zip archives contain many sql scripts, not all of them need to be migrated in release. Sql file names describes in the task. Need some mechanism to handle migrations.

Then it is possible to implement applications deploy from domain description.
