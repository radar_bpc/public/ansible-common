--
-- module test data
--

INSERT INTO t_authz_role (id                    , code  , system_name             , system, version)
VALUES                   (seq_authz_role.nextval, 'ROOT', 'Role for the root user',      1,       1);

INSERT INTO t_authz_role_i18n (target_id             , locale, name                    , version)
VALUES                        (seq_authz_role.currval,   'en', 'Role for the root user',       1);

INSERT INTO t_authz_role_privilege (role_id               , privilege_id)
  SELECT                            seq_authz_role.currval, privilege.id
  FROM t_authz_privilege privilege;


INSERT INTO t_um_person (id                   , gender_id, title_id, birth_date, prefered_lang, version)
VALUES                  (seq_um_person.nextval,         1,        1,       null,          null,       1);

INSERT INTO t_um_person_i18n (target_id            , locale, first_name, last_name, middle_name, version)
VALUES                       (seq_um_person.currval,   'en', 'Admin'   , 'Root'   ,        null,       1);


INSERT INTO t_um_user (id, type_id, person_id, scope_id, category_id, version)
VALUES (
  seq_um_user.nextval,                      -- id
  (
    SELECT userType.id                      -- type_id
    FROM t_um_user_type userType
    WHERE userType.code = 'USER'
  ),
  seq_um_person.currval,                    -- person_id
  (
    SELECT objectTrait.object_id            -- scope_id
    FROM
      t_hierarchy_object_trait objectTrait
      JOIN t_hierarchy_trait trait
        ON objectTrait.trait_id = trait.id
    WHERE trait.code = 'sys_root'
  ),
  (
    SELECT id                               -- category_id
    FROM t_authz_subject_category
    WHERE code = 'BANK_USER'
  ),
  1                                         -- version
);

INSERT INTO t_um_user_scope (user_id, scope_id)
VALUES (
  seq_um_user.currval,
  (
    SELECT objectTrait.object_id
    FROM
      t_hierarchy_object_trait objectTrait
      JOIN t_hierarchy_trait trait
        ON objectTrait.trait_id = trait.id
    WHERE trait.code = 'sys_root'
  )
);


INSERT INTO t_authz_subject_role (subject_id, role_id , scope_id)
VALUES (
  seq_um_user.currval,
  seq_authz_role.currval,
  (
    SELECT objectTrait.object_id
    FROM
      t_hierarchy_object_trait objectTrait
      JOIN t_hierarchy_trait trait
        ON objectTrait.trait_id = trait.id
    WHERE trait.code = 'sys_root'
  )
);


INSERT INTO t_authc_principal (id                         , blocked, subject_id         )
VALUES                        (seq_authc_principal.nextval, null, seq_um_user.currval);

-- disable OTP for admin user
INSERT INTO t_um_setting (owner_id           , name                              , value , version)
VALUES                   (seq_um_user.currval, 'authentication.confirmation.type', 'NONE',       1);

INSERT INTO t_authc (id, principal, principal_id, type, dir_id, num_fails, valid_from, valid_to)
VALUES (
  seq_authc.nextval,
  'admin',
  seq_authc_principal.currval,
  1,
  (SELECT dir.id FROM t_authc_dir dir WHERE dir.code = 'DEFAULT'),
  0,
  SYSDATE,
  null);

-- password: admin
INSERT INTO t_authc_password (authc_id, hash, salt, hash_algorithm, hash_iterations, temporary, active_from)
VALUES (
  seq_authc.currval,
  'C108EFDAFD993124CF50F11CE9D7FDCDD0AE6E9B8ED29C54831CA63483EB97C1CF7F131C5597E034DB090B430E38FCA1DCEE1AB1E541FF52930D0411A22ADC3C',
  '0AC22A9F918E1B7F0605AAA73C5B82883FD1B9F272ADC770B1A07FFEF9521B6C',
  'SHA-512',
  1000,
  null,
  current_timestamp
);

COMMIT;
