#!/usr/bin/env python3


import yaml
import os
import glob
import requests
import sys
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

#
BASE_ROOT = os.path.dirname(os.path.abspath(__file__))
JENKINS_URL = '{{ jenkins_url }}'
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)


def filewatch():
    with open(os.path.join(BASE_ROOT, 'config.yml')) as fp:
        config = yaml.load(fp, Loader=yaml.FullLoader)
        for rec in config["task_list"]:
            job_name = rec['task']
            job_desc = rec['desc']
            logging.info(f"Check {job_desc}")
            if check_job(job_name):
                logging.info(f"Jenkins job {job_name} already running. Skip.")
                continue
            if find_files(rec['path'], rec['mask']):
                run_job(job_name)


def find_files(path, mask):
    """Find files by mask, exclude TMP """
    full_path = os.path.join(path, mask)
    for file in glob.glob(full_path):
        if os.path.isfile(file):
            logging.info(f"Found file {file}")
            return True
    return False


def check_job(job_name):
    """Check if Jenkins job not running"""
    try:
        response = requests.get(f"{JENKINS_URL}{job_name}/lastBuild/api/json", verify=False,
                                auth=('{{ filewatch_login }}', '{{ jenkins_hash }}'))
    except requests.ConnectionError as e:
        logging.error("URL Error: " + str(e.code))
        logging.error(f"(job name [{job_name}] probably wrong)")
        sys.exit(2)

    logging.info(f"Responce code: {response.status_code}")

    if response.status_code != 200:
        logging.info(f"Wrong responce code")
        sys.exit(2)

    build_status = response.json()

    return build_status["building"]


def run_job(job_name):
    """Run Jenkins job"""
    logging.info(f'Run Jenkins task {job_name}')
    url = f"{JENKINS_URL}{job_name}/build"
    logging.debug(r"Job URL: {url}")
    response = requests.post(url, verify=False,
                             auth=('{{ filewatch_login }}', '{{ jenkins_hash }}'))
    logging.info(response)


if '__main__' in __name__:
    filewatch()
