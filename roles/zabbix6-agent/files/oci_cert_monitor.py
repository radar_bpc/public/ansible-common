import sys
import oci
import multiprocessing
import json
import jsonpath_ng
import time
from jsonpath_ng import jsonpath, parse
from functools import lru_cache
from datetime import datetime
from dateutil import parser

# Global vars
config = oci.config.from_file()
identity_client = oci.identity.IdentityClient(config)
THREAD = multiprocessing.cpu_count() * 2
network_client = oci.core.VirtualNetworkClient(config=config)
compartment = sys.argv[3]

@lru_cache(maxsize=256)

def cert_discovery():
    compartment_id = sys.argv[2]
    certificates_management_client = oci.certificates_management.CertificatesManagementClient(
        config)
    # Get Certificate list
    cert_discovery = certificates_management_client.list_certificates(
        compartment_id=compartment_id)
    # Replace name and ID to zabbix readable macros
    cert_discovery = str(cert_discovery.data)
    value_to_macro = {
        '"name"': '"{#CERT_NAME}"',
        '"id"': '"{#CERT_ID}"'
    }
    for value, macro in value_to_macro.items():
        cert_discovery = cert_discovery.replace(value, macro)

    print(cert_discovery)
    return

def cert_validity():
    certificate_id = sys.argv[2]
    certificates_management_client = oci.certificates_management.CertificatesManagementClient(
        config)
    # Get certificate info in JSON Format
    cert_info = certificates_management_client.list_certificates(
        certificate_id=certificate_id)
    cert_info = str(cert_info.data)
    json_cert_info = json.loads(cert_info)
    # Get validity date from JSON
    json_cert_validity = parse('$.items[0].current_version_summary.validity.time_of_validity_not_after')
    cert_validity = json_cert_validity.find(json_cert_info)
    # Cert validity from ISO 8601 to unixtimestamp
    cert_validity_iso8601 = cert_validity[0].value
    cert_validity_datetime = parser.parse(cert_validity_iso8601)
    cert_validity_unixtime = datetime.timestamp(cert_validity_datetime)
    # Cert validity in days
    current_timestamp = time.time()
    cert_validity_days = ((float(cert_validity_unixtime)-float(current_timestamp))/(60*60*24))
    cert_validity_days = int(cert_validity_days)
    print(cert_validity_days)
    return

if "__main__" in __name__:
    if len(sys.argv) > 1:
        arg = sys.argv[1]
        if arg in "discovery" and compartment == "-c":
            cert_discovery()
        elif arg in "cert-validity" and compartment != "-c":
            cert_validity()
        else:
            print("Error in arguments description:   $1 must be function to be executed  \n 'discovery' - runs the discovery of certificates in the compartment\n 'cert-info' - gets the information of specific certificate\n   $2 must be compartment ID or certificate ID ")
    else:
        print("Error in argument count:   $1 must be function to be executed  \n 'discovery' - runs the discovery of certificates in the compartment\n 'cert-info' - gets the information of specific certificate\n   $2 must be compartment ID or certificate ID")
