import configparser
import json
import os
import sys
import cx_Oracle

os.environ["ORACLE_HOME"] = "/u01/app/oracle/product/19.0.0.0/dbhome_1"
os.environ["LD_LIBRARY_PATH"] = "/u01/app/oracle/gg_19.1:/u01/app/oracle/product/19.0.0.0/dbhome_1/lib"

ROOT_DIR = "/home/zabbix/bin/"

confpath = os.path.join(ROOT_DIR, "oradb.ini")
config = configparser.ConfigParser()
config.read(confpath)
section = "db"
user = config.get(section, 'user')
password = config.get(section, 'password')
db_host = config.get(section, 'db_host')
service_name = config.get(section, 'service_name')
dsn = cx_Oracle.makedsn(db_host, 1521, service_name=service_name)
schema = config.get(section, 'schema')
set_schema = "ALTER SESSION SET CURRENT_SCHEMA = %s" % schema
connection = cx_Oracle.connect(user=user, password=password, dsn=dsn, encoding="UTF-8")
cursor = connection.cursor()

##############################################################
sql_free_p = """
SELECT
    ( ( free.free_space_mb + ( alloc.total_maxspace_mb - alloc.total_allocspace_mb ) ) / alloc.total_maxspace_mb ) * 100 pct_free
FROM
    (
        SELECT
            tablespace_name,
            round(SUM(
                CASE
                    WHEN maxbytes = 0 THEN
                        bytes
                    ELSE
                        maxbytes
                END
            ) / 1048576) total_maxspace_mb,
            round(SUM(bytes) / 1048576) total_allocspace_mb
        FROM
            dba_data_files
        WHERE
            file_id NOT IN (
                SELECT
                    file#
                FROM
                    v$recover_file
            )
        GROUP BY
            tablespace_name
    ) alloc,
    (
        SELECT
            tablespace_name,
            SUM(bytes) / 1048576 free_space_mb
        FROM
            dba_free_space
        WHERE
            file_id NOT IN (
                SELECT
                    file#
                FROM
                    v$recover_file
            )
        GROUP BY
            tablespace_name
    ) free
WHERE
    alloc.tablespace_name = free.tablespace_name (+)
    AND alloc.tablespace_name = :tbs_name
    """
sql_used_p = """
    SELECT
    ( ( alloc.total_allocspace_mb - free.free_space_mb ) / alloc.total_maxspace_mb ) * 100 pct_used
FROM
    (
        SELECT
            tablespace_name,
            round(SUM(
                CASE
                    WHEN maxbytes = 0 THEN
                        bytes
                    ELSE
                        maxbytes
                END
            ) / 1048576) total_maxspace_mb,
            round(SUM(bytes) / 1048576) total_allocspace_mb
        FROM
            dba_data_files
        WHERE
            file_id NOT IN (
                SELECT
                    file#
                FROM
                    v$recover_file
            )
        GROUP BY
            tablespace_name
    ) alloc,
    (
        SELECT
            tablespace_name,
            SUM(bytes) / 1048576 free_space_mb
        FROM
            dba_free_space
        WHERE
            file_id NOT IN (
                SELECT
                    file#
                FROM
                    v$recover_file
            )
        GROUP BY
            tablespace_name
    ) free
WHERE
    alloc.tablespace_name = free.tablespace_name (+)
    AND alloc.tablespace_name = :tbs_name
    """
sql_invalid = "SELECT COUNT(*) FROM dba_objects where status = 'INVALID' and object_type != 'MATERIALIZED VIEW'"
sql_lock = "select count(1) from v$session where blocking_session is null and sid in (select blocking_session from v$session where blocking_session is not null)"

sql_info_instance = "select instance_name from v$instance"
sql_info_host = "select host_name from v$instance"
sql_info_version = "select version_full from v$instance"
sql_info_version11 = "select version from v$instance"
sql_info_status = "select status from v$instance"
sql_info_database = "select database_type from v$instance"
sql_info_database11 = "select instance_role from v$instance"
sql_session_active = "select count(1) active from gv$session where type !='BACKGROUND' and status='ACTIVE'"
sql_session_total = "select count(1) active from gv$session where type !='BACKGROUND'"
sql_backup_status = "select sysdate-max(start_time) from V$RMAN_STATUS where  ROW_TYPE = 'SESSION' and operation = 'RMAN' and status = 'COMPLETED'"
sql_bo_container = """
with check_time as (select to_date(to_char(sysdate - 1/24,'HH24:MI'),'HH24:MI') from dual)
    select (case when (
        select * from  (
            with off_start as (select to_date((select REGEXP_SUBSTR(SWITCH_OFF_PERIOD,'[^-]+',1,1) from prc_task where process_id=70000050),'HH24:MI') from dual),
            off_end as (select to_date((select REGEXP_SUBSTR(SWITCH_OFF_PERIOD,'[^-]+',1,2) from prc_task where process_id=70000050),'HH24:MI') from dual),
            current_time as (select to_date(to_char(sysdate,'HH24:MI'),'HH24:MI') from dual),
            last_start_time as (select to_date((SELECT EXTRACT(HOUR FROM (select max(start_time) from prc_session where process_id=10000933)) || ':' || EXTRACT(MINUTE FROM (
                select max(start_time) from prc_session where process_id=10000933)) FROM DUAL),'HH24:MI') from dual)
                select (
                    case when (select * from current_time) between (select * from off_start) and (select * from off_end)
                        THEN (select * from current_time)
                        ELSE (select * from last_start_time) END) from dual)) < (select * from check_time)
                    THEN 'FAIL'
                    ELSE 'OK' END) OUT from dual
"""
sql_fm_rule_decline = "select r.rule_id, count(r.rule_id) from T_FRAUD_TRANS_RULES r, t_fraud_trans t where t.utrnno = r.utrnno and t.ttime < sysdate-1 and t.resp_code <> '000' group by r.rule_id order by count(r.rule_id) desc"
sql_bo_svig_order_queue = "select count(1) from main.pmo_order o where decode(o.status, 'POSA0001', o.id, 'POSA5001', o.id, null) is not null and exists (select 1 from main.pmo_order t where t.id = o.template_id and t.templ_status = 'POTSVALD') and export_date <= sysdate"


#############################################################
def tablespace_discovery():
    sql = """
    select tablespace_name from user_tablespaces order by 1
    """
    cursor.execute(sql)
    result = [{"{#TSNAME}": x[0]} for x in cursor]

    print(json.dumps({"data": result}), file=sys.stdout)


def tablespace_discovery2():
    sql = """
    SELECT
    JSON_ARRAYAGG(
        JSON_OBJECT(TABLESPACE_NAME VALUE
            JSON_OBJECT(
                'contents'	VALUE CONTENTS,
                'file_bytes'    VALUE FILE_BYTES,
                'max_bytes'     VALUE MAX_BYTES,
                'free_bytes'    VALUE FREE_BYTES,
                'used_bytes'    VALUE USED_BYTES,
                'used_pct_max'  VALUE USED_PCT_MAX,
                'used_file_pct' VALUE USED_FILE_PCT,
                'status'        VALUE STATUS
            )
        ) RETURNING CLOB
    )
    FROM
        (
        SELECT
            df.TABLESPACE_NAME AS TABLESPACE_NAME,
            df.CONTENTS AS CONTENTS,
            NVL(SUM(df.BYTES), 0) AS FILE_BYTES,
            NVL(SUM(df.MAX_BYTES), 0) AS MAX_BYTES,
            NVL(SUM(f.FREE), 0) AS FREE_BYTES,
            SUM(df.BYTES)-SUM(f.FREE) AS USED_BYTES,
            ROUND(DECODE(SUM(df.MAX_BYTES), 0, 0, ((SUM(df.BYTES)-SUM(f.FREE)) / SUM(df.MAX_BYTES) * 100)), 2) AS USED_PCT_MAX,
            ROUND(DECODE(SUM(df.BYTES), 0, 0, (SUM(df.BYTES)-SUM(f.FREE)) / SUM(df.BYTES)* 100), 2) AS USED_FILE_PCT,
            DECODE(df.STATUS, 'ONLINE', 1, 'OFFLINE', 2, 'READ ONLY', 3, 0) AS STATUS
        FROM
            (
            SELECT
                ddf.FILE_ID,
                dt.CONTENTS,
                dt.STATUS,
                ddf.FILE_NAME,
                ddf.TABLESPACE_NAME,
                TRUNC(ddf.BYTES) AS BYTES,
                TRUNC(GREATEST(ddf.BYTES, ddf.MAXBYTES)) AS MAX_BYTES
            FROM
                DBA_DATA_FILES ddf,
                DBA_TABLESPACES dt
            WHERE
                ddf.TABLESPACE_NAME = dt.TABLESPACE_NAME
            ) df,
            (
            SELECT
                TRUNC(SUM(BYTES)) AS FREE,
                FILE_ID
            FROM
                DBA_FREE_SPACE
            GROUP BY
                FILE_ID
            ) f
        WHERE
            df.FILE_ID = f.FILE_ID (+)
        GROUP BY
            df.TABLESPACE_NAME, df.CONTENTS, df.STATUS
    UNION ALL
        SELECT
            tts.TABLESPACE_NAME AS TABLESPACE_NAME,
            tts.CONTENTS AS CONTENTS,
            tts.FILE_BYTES AS FILE_BYTES,
            tts.MAX_BYTES AS MAX_BYTES,
            dfs.FREE_BYTES AS FREE,
            tts.FILE_BYTES-dfs.FREE_BYTES AS USED_BYTES,
            ROUND(DECODE(tts.MAX_BYTES, 0, 0, (tts.FILE_BYTES-dfs.FREE_BYTES) / tts.MAX_BYTES * 100), 2) AS USED_PCT_MAX,
            ROUND(DECODE(tts.FILE_BYTES, 0, 0, (tts.FILE_BYTES-dfs.FREE_BYTES) / tts.FILE_BYTES* 100), 2) AS USED_FILE_PCT,
            TBS_STATUS
        FROM
            (
            select
            Y.NAME AS TABLESPACE_NAME,
            Y.CONTENTS AS CONTENTS,
            NVL(SUM(Y.BYTES), 0) AS FILE_BYTES,
            NVL(SUM(Y.MAX_BYTES), 0) AS MAX_BYTES,
            DECODE(y.TBS_STATUS, 'ONLINE', 1, 'OFFLINE', 2, 'READ ONLY', 3, 0) AS TBS_STATUS
            from (
            SELECT
                dt.TABLESPACE_NAME AS NAME,
                dt.CONTENTS,
                dt.STATUS AS TBS_STATUS,
                dtf.BYTES AS BYTES,
                CASE
                    WHEN dtf.MAXBYTES = 0 THEN dtf.BYTES
                    ELSE dtf.MAXBYTES
                END AS MAX_BYTES
            FROM
                sys.DBA_TEMP_FILES dtf,
                sys.DBA_TABLESPACES dt
            WHERE
                dtf.TABLESPACE_NAME = dt.TABLESPACE_NAME ) Y
        GROUP BY
            Y.NAME, Y.CONTENTS, Y.TBS_STATUS ) tts ,
                (
                SELECT
                    f.TABLESPACE_NAME TABLESPACE_NAME,
                    ((f.TOTAL_BLOCKS - s.TOT_USED_BLOCKS) * vp.VALUE) FREE_BYTES
                FROM
                    (
                    SELECT
                        TABLESPACE_NAME, SUM(USED_BLOCKS) TOT_USED_BLOCKS
                    FROM
                        GV$SORT_SEGMENT
                    WHERE
                        TABLESPACE_NAME != 'DUMMY'
                    GROUP BY
                        TABLESPACE_NAME) s, (
                    SELECT
                        TABLESPACE_NAME, SUM(BLOCKS) TOTAL_BLOCKS
                    FROM
                        DBA_TEMP_FILES
                    WHERE
                        TABLESPACE_NAME != 'DUMMY'
                    GROUP BY
                        TABLESPACE_NAME) f, (
                    SELECT
                        VALUE
                    FROM
                        V$PARAMETER
                    WHERE
                        NAME = 'db_block_size') vp
                WHERE
                    f.TABLESPACE_NAME = s.TABLESPACE_NAME
                ) dfs
                where dfs.tablespace_name = tts.tablespace_name
        )

    """
    result = ""
    cursor.execute(sql)
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        result = row[0].read().replace("\":.", "\":0.")
    print(result)


def tablespace_size(tbs_name, fkey):
    # noinspection SpellCheckingInspection
    if "pfree" in fkey:
        sql = sql_free_p
    elif "pused" in fkey:
        sql = sql_used_p

    cursor.execute(sql, [tbs_name, ])
    result = cursor.fetchone()[0]
    print(result, file=sys.stdout)


def query_app(app_key, fkey):
    """
    Query for application with set schema
    :param app_key: Application key as fe/bo/fm/ecom
    :param fkey:
    :return:
    """
    if "fm" in app_key:
        if "rule_decline" in fkey:
            sql = sql_fm_rule_decline
    elif "bo" in app_key:
        if "svig_order_queue" in fkey:
            sql = sql_bo_svig_order_queue
        elif "start_container" in fkey:
            sql = sql_bo_container
    cursor.execute(set_schema)
    cursor.execute(sql)
    result = cursor.fetchone()[0]
    print(result, file=sys.stdout)


def query(fkey):
    """
    Simple query
    :param fkey:
    :return:
    """
    if "invalid" in fkey:
        sql = sql_invalid
    elif "lock" in fkey:
        sql = sql_lock
    elif "instance" in fkey:
        sql = sql_info_instance
    elif "host" in fkey:
        sql = sql_info_host
    elif "version" in fkey:
        sql = sql_info_version
    elif "status" in fkey:
        sql = sql_info_status
    elif "database" in fkey:
        sql = sql_info_database
    elif "instance" in fkey:
        sql = sql_info_instance
    elif "session_active" in fkey:
        sql = sql_session_active
    elif "session_total" in fkey:
        sql = sql_session_total
    elif "backup_date" in fkey:
        sql = sql_backup_status

    cursor.execute(sql)
    result = cursor.fetchone()[0]
    print(result, file=sys.stdout)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        if "discovery_tbs" in sys.argv[1]:
            tablespace_discovery()
        elif "tbs2" in sys.argv[1]:
            tablespace_discovery2()
        elif "size" in sys.argv[1]:
            tablespace_size(sys.argv[2], sys.argv[3])
        elif "query" in sys.argv[1]:
            query(sys.argv[2])
        elif "app" in sys.argv[1]:
            query_app(sys.argv[2], sys.argv[3])
