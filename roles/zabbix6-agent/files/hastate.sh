#/bin/bash

SYS=`uname -n`
HASTATE=`sudo /opt/VRTSvcs/bin/hagrp -display $1 -sys $SYS -attribute State | grep -v "#" | awk '{print $4}'`
HAFROZEN=`sudo /opt/VRTSvcs/bin/hagrp -display $1 -attribute TFrozen | grep -v "#" | awk '{print $4}'`

if [ $HASTATE = "|ONLINE|" ] && [ $HAFROZEN != "0" ]; then
        echo $HASTATE"FROZEN|"
else
        echo $HASTATE
fi
