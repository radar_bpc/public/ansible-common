select owner
     , object_name
     , object_type
  from dba_objects
 where status='INVALID'
   and object_type not in ('MATERIALIZED VIEW', 'SYNONYM')
