SELECT '' ||s2.username||' ('||s2.sid||', '||s2.serial#||', @'||s2.inst_id||') @'||substr(s2.program, 1)||' from '||s2.machine||'/'||s2.osuser||' is blocking ' ||s1.username||' ('||s1.sid||', '||s1.serial#||', @'||s1.inst_id||') @'||substr(s1.program, 1)||' from '||s1.machine||'/'||s1.osuser||'' as DB_LOCK,
 ''|| to_char(trunc(sysdate) + NUMTODSINTERVAL(l1.ctime,'second'),'hh24:mi:ss')||' seconds ' DURATION
 FROM gv$session s2
 join gv$session s1 ON (s1.blocking_session=s2.sid)
 join gv$lock l2 on (s2.sid=l2.sid)
 left join gv$lock l1 on (s1.sid=l1.sid and l1.type = l2.type AND l1.id1 = l2.id1 AND l1.id2 = l2.id2)
 where l2.LMODE=6
 and l1.ctime > :1
 order by 2 desc;
