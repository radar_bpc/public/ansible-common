select username as "USER NAME",
       trunc(expiry_date - sysdate) "Expires in Days",
       trunc(expiry_date) as "EXPIRE DATE",
       account_status
from dba_users
where expiry_date < sysdate+15
and account_status IN ( 'OPEN', 'EXPIRED(GRACE)', 'EXPIRED' )
order by account_status, expiry_date, username
