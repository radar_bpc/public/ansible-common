with p as 
  (select distinct 
          c.container_process_id
        , c_name.text            as container_name
        , t.is_holiday_skipped
        , t.crontab_value
        , p.inst_id
        , i_name.text            as inst_name
        , c_desc.text            as description
        , t.switch_off_period
        , replace(regexp_substr(t.switch_off_period, '\d{2}:\d{2}',1,1), ':','') bhour_char
        , replace(regexp_substr(t.switch_off_period, '\d{2}:\d{2}',1,2), ':','') ehour_char
     from main.prc_container c
        , main.prc_task      t
        , main.prc_process   p
        , main.com_i18n      c_name
        , main.com_i18n      c_desc
        , main.com_i18n      i_name
    where c.container_process_id = t.process_id
      and p.id                   = c.container_process_id
      and t.is_active            = 1
      and c_name.object_id       = c.container_process_id
      and c_name.table_name      = 'PRC_PROCESS'
      and c_name.column_name     = 'NAME'
      and c_desc.object_id       = t.id
      and c_desc.table_name      = 'PRC_TASK'
      and c_desc.column_name     = 'NAME'
      and i_name.object_id       = p.inst_id
      and i_name.table_name      = 'OST_INSTITUTION'
      and i_name.column_name     = 'NAME'     
      and not exists (select 1 from main.prc_container ct where ct.id = c.id and ct.process_id in (10000836, 10000891, 10000892, 70000085)) -- process application/unload merchants/terminals
      )
, schedule as 
     (
     select p.*
          , cast(t.column_value as timestamp) as planned_Date
       from p
          , table(main.prc_ui_task_pkg.get_exec_time(
                      i_date               => trunc(sysdate, 'dd')
                    , i_cron               => p.crontab_value
                    , i_inst_id            => p.inst_id
                    , i_is_holiday_skipped => p.is_holiday_skipped
                  )
            ) t
      where p.switch_off_period is null or 
            (cast(t.column_value as timestamp) not between 
            case 
            when to_number(bhour_char) > to_number(ehour_char) and to_char(cast(t.column_value as timestamp), 'hh24mi') < to_number(bhour_char)
            then to_date(to_char(sysdate-1,'dd.mm.yyyy') || bhour_char,'dd.mm.yyyy hh24mi')        
            when to_number(bhour_char) > to_number(ehour_char) and to_char(cast(t.column_value as timestamp), 'hh24mi') >= to_number(bhour_char)
            then to_date(to_char(sysdate,'dd.mm.yyyy') || bhour_char,'dd.mm.yyyy hh24mi')          
            when to_number(bhour_char) < to_number(ehour_char)
            then to_date(to_char(sysdate,'dd.mm.yyyy') || bhour_char,'dd.mm.yyyy hh24mi')   
            end
            and 
            case 
            when to_number(bhour_char) > to_number(ehour_char) and to_char(cast(t.column_value as timestamp), 'hh24mi') < to_number(bhour_char)
            then to_date(to_char(sysdate,'dd.mm.yyyy') || ehour_char,'dd.mm.yyyy hh24mi')        
            when to_number(bhour_char) > to_number(ehour_char) and to_char(cast(t.column_value as timestamp), 'hh24mi') >= to_number(bhour_char)
            then to_date(to_char(sysdate+1,'dd.mm.yyyy') || ehour_char,'dd.mm.yyyy hh24mi')       
            when to_number(bhour_char) < to_number(ehour_char)
            then to_date(to_char(sysdate,'dd.mm.yyyy') || ehour_char,'dd.mm.yyyy hh24mi')   
            end)
     )
, fact_load as 
    (
    select s.id
         , s.process_id
         , s.start_time
         , s.end_time
         , s.result_code
         , s.user_id
      from main.prc_session s 
     where s.start_time between trunc(sysdate, 'dd') and trunc(sysdate, 'dd') + 1
       and s.parent_id  is      null
      -- and main.cst_util_pkg.is_ps_institution(s.inst_id) = 1) -- process application
      )
select sd.inst_id
     , sd.inst_name
     , sd.container_process_id
     , sd.container_name
     , sd.description
     , to_char(sd.planned_date, 'dd.mm.yyyy hh24:mi:ss') scheduled_time
     , fl.id fact_session_id
     , to_char(fl.start_time, 'dd.mm.yyyy hh24:mi:ss.ff6') fact_start_time
     , extract(hour from fl.end_time - fl.start_time) * 60 + extract(minute from fl.end_time - fl.start_time)  mins_in_work
     , fl.result_code 
     , main.get_article_text(fl.result_code) result_desc
     , case
       when    fl.id is null 
           and fl.start_time is null 
           and sysdate > sd.planned_date
       then 2 
       when    fl.result_code = 'PRSR0003'
           and extract(hour from systimestamp - fl.start_time) < 2
       then 2
       when    fl.result_code = 'PRSR0003'
           and extract(hour from systimestamp - fl.start_time) > 2
       then 1         
       else 0 
       end problem_proirity
/*json_arrayagg (
         json_object(
           'inst_id'         value sd.inst_id
         , 'inst_name'       value sd.inst_name
         , 'container_id'    value sd.container_process_id
         , 'container_name'  value sd.container_name
         , 'description'     value sd.description
         , 'planned_time'    value to_char(sd.planned_date, 'dd.mm.yyyy hh24:mi:ss')
         , 'fact_session_id' value fl.id
         , 'fact_start_time' value to_char(fl.start_time, 'dd.mm.yyyy hh24:mi:ss.ff6')
         , 'fact_end_time'   value to_char(fl.end_time, 'dd.mm.yyyy hh24:mi:ss.ff6')
         , 'mins_in_work'    value extract(hour from fl.end_time - fl.start_time) * 60 + extract(minute from fl.end_time - fl.start_time) 
         , 'result_code'     value fl.result_code
         , 'result_desc'     value main.get_article_text(fl.result_code)
         )
       returning clob
       )*/
  from schedule       sd
  left join fact_load fl
    on sd.container_process_id = fl.process_Id
   and abs(extract(hour from fl.start_time - sd.planned_date) * 60 + extract(minute from fl.start_time - sd.planned_date)) < 15 
order by sd.inst_id
    , sd.planned_date;
