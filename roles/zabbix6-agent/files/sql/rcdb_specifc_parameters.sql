select SID, NAME, VALUE, DISPLAY_VALUE 
from v$spparameter 
where SID !='*'
and NAME not in ('thread','undo_tablespace','instance_number'); 
