select ct.inst_id   as "{#INST_ID}"
     , inm.text     as "{#INST_NAME}"
     , t.process_id as "{#CONTAINER_ID}"
     , cnm.text     as "{#CONTAINER_NAME}"
  from main.prc_task    t
  join main.prc_process ct  on  ct.id = t.process_id
  join main.com_i18n    cnm on cnm.object_id = t.process_id and cnm.table_name = 'PRC_PROCESS'     and cnm.column_name = 'NAME' and cnm.lang = 'LANGENG'
  join main.com_i18n    inm on inm.object_id = ct.inst_id   and inm.table_name = 'OST_INSTITUTION' and inm.column_name = 'NAME' and inm.lang = 'LANGENG'
 where t.is_active = 1
 order by ct.inst_id;
