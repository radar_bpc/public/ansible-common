# SQL files for Oracle Plugin

The .sql files will be placed in Zabbix Server host and are used in the oracle.custom.query[*] items to perform customized queries into the databases. 

## Naming convention

In order to identify and organize the queries, the following name convention was applied:

`component_name_of_query` E.G `fe_active_terminals.sql // Query used to obtain number of active terminals in SVFE` 