with check_time as (select to_date(to_char(sysdate - 1/24,'HH24:MI'),'HH24:MI') from dual)
    select (case when (
        select * from  (with
                off_start as (select to_date((select REGEXP_SUBSTR(SWITCH_OFF_PERIOD,'[^-]+',1,1) from prc_task where process_id=70000050),'HH24:MI') from dual),
                off_end as (select to_date((select REGEXP_SUBSTR(SWITCH_OFF_PERIOD,'[^-]+',1,2) from prc_task where process_id=70000050),'HH24:MI') from dual),
                current_time as (select to_date(to_char(sysdate,'HH24:MI'),'HH24:MI') from dual),
                last_start_time as (select to_date((select EXTRACT(HOUR FROM (select max(start_time) from prc_session where process_id=10000933)) || ':' || EXTRACT(MINUTE from (
                                                        select max(start_time) from prc_session where process_id=10000933)) FROM DUAL),'HH24:MI') from dual)
                                    select (case when (select * from current_time) between (select * from off_start) and (select * from off_end)
                                                 then (select * from current_time)
                                                else (select * from last_start_time) end) from dual)) < (select * from check_time)
            then 'FAIL'
            else 'OK' end) OUT from dual
