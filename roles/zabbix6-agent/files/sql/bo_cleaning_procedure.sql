 select case status as status
    when 'FAILED' then status ||  ': ' || substr(additional_info, 1, 60) || '...'
    else status
end as status
 from (select status
                  , additional_info
                  , rank() over (partition by job_name order by log_date desc) rnk
          from user_scheduler_job_run_details 
        where job_name='SUP_CLEAN_JOB')
 where rnk = 1;

