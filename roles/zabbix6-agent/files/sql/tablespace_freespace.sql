SELECT
    pd.name,
    dm.tablespace_name,
    round((dm.tablespace_size - dm.used_space) * ts.block_size / 1024 / 1024 / 1024) "FREE_SPACE_GB"
FROM
    cdb_tablespace_usage_metrics  dm,
    cdb_tablespaces               ts,
    v$containers                  pd
WHERE
        ts.tablespace_name = dm.tablespace_name
    AND ts.con_id   = pd.con_id
    AND dm.con_id   = pd.con_id
ORDER BY name, tablespace_name;
