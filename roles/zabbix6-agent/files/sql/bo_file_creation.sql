select container_name|| '-' || process_name ||'-'|| container_inst  as process_name, last_sess_files_count, last_run_sess_id from (
with pros as (select con.ID as ci,
                            con.container_process_id as cc,
                            con.process_id as cp,
                            listagg(PF.FILE_PURPOSE, ', ') within group (order by PF.FILE_PURPOSE) FILE_PURPOSE
                       from MAIN.prc_container con
                            left join MAIN.PRC_FILE pf on CON.PROCESS_ID=PF.PROCESS_ID
                       where pf.FILE_PURPOSE = 'FLPSOUTG'
                      group by con.ID,
                               con.container_process_id,
                               con.process_id
                     ), ses as
                    (select distinct pse.container_id,
                           pse.sess,
                           nvl(PF.FILE_PURPOSE, 'NONE') FILE_PURPOSE
                      from
                           (
                            select s.container_id,
                                   max(s.ID) as sess
                              from MAIN.prc_session s
                             group by (s.container_id)
                            ) pse
                            left join MAIN.prc_session_file psf on pse.sess=PSF.SESSION_ID
                            left join MAIN.PRC_FILE_ATTRIBUTE  pfa on PSF.FILE_ATTR_ID=pfa.ID
                            left join MAIN.PRC_FILE pf on pf.ID=PFA.FILE_ID
                    ),
             filses as
                    (select container_id fc,
                            max(ss.ID) fcs
                       from MAIN.prc_session ss
                      where exists
                                  (select null
                                     from MAIN.prc_session_file sf
                                    where ss.ID=sf.session_id)
                                    group by container_id),
            sch as
                   (
                   select SCH.PROCESS_ID
                              from MAIN.prc_task sch
                             where SCH.IS_ACTIVE=1
                   )
     select inner_id,
            process_id,
            process_name,
            container_id,
            container_name,
            container_inst,
            last_run_sess_id,
            last_sess_start_time,
            type_of_created_files_now,
            type_of_created_files_general,
 --           last_sess_with_files,
 --           last_file_id,
            last_sess_files_count,
            last_file_name,
         --   substr(sys.stragg(DISTINCT trace_level || ', '), 0, LENGTH(sys.stragg(DISTINCT trace_level || ', '))-2)  trace_level,
            --substr(sys.stragg(DISTINCT cl.NAME || ', '), 0, LENGTH(sys.stragg(DISTINCT cl.NAME || ', '))-2)  label_name,
            RUN_MODE,
            history_data HISTORY_WITH_PROC_SESSION_ID
       from  (
             select distinct pros.ci as inner_id,
                    pros.cp as process_id,
                    cop.text as process_name,
                    pros.cc as container_id,
                    coc.text as container_name,
                    prc.inst_id as container_inst,
                    ses.sess as last_run_sess_id,
                    ss1.start_time as last_sess_start_time,
                    ses.FILE_PURPOSE TYPE_OF_CREATED_FILES_NOW,
                    nvl(pros.FILE_PURPOSE, 'NONE') TYPE_OF_CREATED_FILES_GENERAL,
                    case when ses.sess=filses.fcs
                              then 'last run session'
                         else to_char(filses.fcs)
                    end as last_sess_with_files,
                    (select max(f1.ID)
                       from MAIN.prc_session_file f1
                      where f1.session_id = filses.fcs
                    ) as last_file_id,
                    (select count(f2.ID)
                       from MAIN.prc_session_file f2
                      where f2.session_id=filses.fcs
                    ) as last_sess_files_count,
                    (select f3.file_name
                       from MAIN.prc_session_file f3
                      where f3.ID=
                                  (select max(f1.ID)
                                     from MAIN.prc_session_file f1
                                    where f1.session_id = filses.fcs
                                  )
                    ) as last_file_name,
                    nvl2(SCH.PROCESS_ID, 'Automatically', 'Manually') RUN_MODE,
                    history.history_data
               from ses,
                    MAIN.com_i18n cop,
                    MAIN.com_i18n coc,
                    MAIN.prc_process prc,
                    MAIN.prc_session ss1,
                    pros
                    left join filses on filses.fc=pros.ci
                    left join sch on SCH.PROCESS_ID = pros.cc
                    left join (
                                   select listagg('[' || start_time || ' - ' || ID || ']', ', ') within group (order by '[' || start_time || ' - ' || ID || ']' desc) history_data,
                                          process_id
                                   from   (select ID,
                                                  to_char(start_time, 'YYYY-MM-DD HH24:MI:SS') start_time,
                                                  process_id,
                                                  dense_rank() over (partition by process_id order by ID desc) as val_rank
                                           from   MAIN.prc_session)
                                   where  val_rank <= 6
                                   group  by process_id
                              ) history on history.process_id=pros.cp
              where pros.ci = ses.container_id
--                AND EXISTS
--                           (SELECT NULL
--                              FROM MAIN.prc_task sch
--                             WHERE SCH.PROCESS_ID = pros.cc
--                               AND SCH.IS_ACTIVE=1
--                           )
                and pros.cp=cop.object_id
                and cop.lang = 'LANGENG'
                and cop.table_name = 'PRC_PROCESS'
                and cop.column_name = 'NAME'
                and pros.cc=coc.object_id
                and coc.lang = 'LANGENG'
                and coc.table_name = 'PRC_PROCESS'
                and coc.column_name = 'NAME'
                and pros.cc = prc.ID
                and ses.sess = ss1.ID
              ) dta,
              MAIN.TRC_LOG trc
         left join MAIN.COM_LABEL  cl on trc.label_id=CL.ID
        where dta.last_run_sess_id=trc.session_id
        group by inner_id,
                 process_id,
                 process_name,
                 container_id,
                 container_name,
                 container_inst,
                 last_run_sess_id,
                 last_sess_start_time,
                 type_of_created_files_now,
                 type_of_created_files_general,
 --                last_sess_with_files,
--                 last_file_id,
                 last_sess_files_count,
                 last_file_name,
                 run_mode,
                 history_data
        order by container_id,
                 last_run_sess_id);
