SELECT ID,PROCESS_ID,START_TIME FROM MAIN.prc_session WHERE
RESULT_CODE='PRSR0001'
AND to_char(START_TIME, 'YYYYMMDD')=to_char(SYSDATE-1, 'YYYYMMDD')
AND parent_id IS NULL
AND end_time IS NULL
AND process_id NOT IN (60000129,60000130,70000053,60000128,70000066,60000021,70000064);
