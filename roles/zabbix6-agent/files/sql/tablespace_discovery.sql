SELECT
    pd.name "{#CON_NAME}",
    dm.tablespace_name "{#TABLESPACE}"
FROM
    cdb_tablespace_usage_metrics  dm,
    cdb_tablespaces               ts,
    v$containers                  pd
WHERE
        ts.tablespace_name = dm.tablespace_name
    AND ts.con_id   = pd.con_id
    AND dm.con_id   = pd.con_id;
