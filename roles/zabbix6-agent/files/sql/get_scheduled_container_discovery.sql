select t.inst_id        as "{#INST_ID}"
     , t.inst_name      as "{#INST_NAME}"
     , t.container_id   as "{#CONTAINER_ID}"
     , t.container_name as "{#CONTAINER_NAME}"
  from table(main.cst_api_monitoring_pkg.get_scheduled_container_discovery) t;
