select round((sysdate + to_dsinterval(coalesce(value,'+00 00:00:00')) - sysdate) * 3600 * 24) apply_lag_seconds 
from (select value from v$dataguard_stats where name='apply lag') s
right join v$database on database_role='PHYSICAL STANDBY';
