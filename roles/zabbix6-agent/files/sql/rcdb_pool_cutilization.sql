(
  SELECT 
    resource_name, 
    current_utilization
  FROM 
    v$resource_limit 
  WHERE 
    TRIM(initial_allocation) NOT IN ('UNLIMITED', '0') 
    and resource_name in (&1)
);

