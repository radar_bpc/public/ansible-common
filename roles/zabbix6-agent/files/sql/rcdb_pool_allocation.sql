(
  SELECT 
    resource_name, 
    to_number(initial_allocation) initial_allocation
  FROM 
    v$resource_limit 
  WHERE 
    TRIM(initial_allocation) NOT IN ('UNLIMITED', '0') 
    and resource_name in (&1)
);
