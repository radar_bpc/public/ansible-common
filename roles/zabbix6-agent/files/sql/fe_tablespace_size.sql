SELECT
    a.pct_free, a.pct_used
FROM
    (
        select /*+ALL_ROWS */
            alloc.tablespace_name,
            alloc.total_maxspace_mb,
            (alloc.total_allocspace_mb - free.free_space_mb) as used_space_mb,
            free.free_space_mb+(alloc.total_maxspace_mb-alloc.total_allocspace_mb) as free_space_mb,
            free.free_space_mb as free_space_ext_mb,
            ((alloc.total_allocspace_mb - free.free_space_mb)/alloc.total_maxspace_mb)*100 pct_used,
            ((free.free_space_mb+(alloc.total_maxspace_mb-alloc.total_allocspace_mb))/alloc.total_maxspace_mb)*100 pct_free
        FROM (SELECT tablespace_name,
                     ROUND(SUM(CASE WHEN maxbytes = 0 THEN bytes ELSE maxbytes END)/1048576) total_maxspace_mb,
                     ROUND(SUM(bytes)/1048576) total_allocspace_mb
              FROM dba_data_files
              WHERE file_id NOT IN (SELECT FILE# FROM v$recover_file)
              GROUP BY tablespace_name) alloc,
             (SELECT tablespace_name,
                     SUM(bytes)/1048576 free_space_mb
              FROM dba_free_space
              WHERE file_id NOT IN (SELECT FILE# FROM v$recover_file)
              GROUP BY tablespace_name) free
        WHERE alloc.tablespace_name = free.tablespace_name (+)
        ORDER BY pct_used DESC) a where a.TABLESPACE_NAME = :1
