(
  SELECT
    resource_name,
    current_utilization,
    to_number(initial_allocation) initial_allocation,
    round(
      current_utilization / to_number(initial_allocation) * 100
    ) usage_pct
  FROM
    v$resource_limit
  WHERE
    TRIM(initial_allocation) NOT IN ('UNLIMITED', '0')
    and resource_name in (&1)
);
[root@c02p1-zbxs01 sql] <> # cat rcdb_pool_discovery.sql
(
  SELECT
    resource_name as "{#RESOURCE_NAME}"
  FROM
    v$resource_limit
  WHERE
    TRIM(initial_allocation) NOT IN ('UNLIMITED', '0')
	);

