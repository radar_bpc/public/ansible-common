--- PostgreSQL

create database c02_tst_pgwall;


--connect to new database
create user apigate with password '*****';
create domain clob as text;

CREATE TABLE T_APIGATE_REQUEST_LOG
(
    ID                VARCHAR(32)           NOT NULL,
    API_COMMAND       VARCHAR(256)          NOT NULL,
    REQUEST_DATE      DATE                  NOT NULL,
    RESPONSE_CODE     NUMERIC(3),
    RESPONSE_CONTENT  CLOB,
    CONSTRAINT T_APIGATE_REQUEST_LOG_PK PRIMARY KEY(ID);
);


-- Oracle Database
CREATE TABLE T_APIGATE_REQUEST_LOG
(
    ID                VARCHAR2(32 CHAR)           NOT NULL,
    API_COMMAND       VARCHAR2(256 BYTE)          NOT NULL,
    REQUEST_DATE      DATE                        NOT NULL,
    RESPONSE_CODE     NUMBER(3),
    RESPONSE_CONTENT  CLOB
);

CREATE UNIQUE INDEX T_APIGATE_REQUEST_LOG_PK ON T_APIGATE_REQUEST_LOG (ID);

ALTER TABLE T_APIGATE_REQUEST_LOG ADD (
    CONSTRAINT T_APIGATE_REQUEST_LOG_PK
        PRIMARY KEY
            (ID)
            USING INDEX T_APIGATE_REQUEST_LOG_PK);
