# New apigate role

## Full
```shell
## setup all apigates
aplaybook -i inventories/oci02 -l uat_gate_app playbooks/apigate-app.yml
## setup specific apigate
aplaybook -i inventories/oci02 -l uat_gate_app playbooks/apigate-app.yml -e "app_name=apigate-seba"

## configure
aplaybook -i inventories/oci02 -l uat_gate_app playbooks/apigate-app.yml -t config --diff -e "app_name=apigate-seba"

## configure cron for cleaning logs
aplaybook -i inventories/oci02 -l uat_gate_app playbooks/apigate-app.yml -t cron --diff
```

apigate properties path:
* "{{ app_file_path }}/apigate/{{ project }}/{{ apigate_app_name }}/{{ env_name }}/apigate.properties"
* "{{ app_file_path }}/apigate/{{ project }}/{{ apigate_app_name }}/apigate.properties"
* "{{ app_file_path }}/apigate/{{ apigate_app_name }}/apigate.properties"
* files(templates)/apigate.properties

tags: install, config, cron

## Vars
app_name: apigate, apigate-bred, apigate-credorax, apigate-seba
