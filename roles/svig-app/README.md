# SVIG application

```shell
ansible-playbook -i inventories/prod -l uat_svig playbooks/svig-app.yml -b --ask-vault-password
```

## Update application
```shell
ansible-playbook -i inventories/prod -l uat_svig playbooks/svig-app.yml -b --ask-vault-password -e "release=R22.50 svig_app=ru.bpc.paymentsense.svig.ui-0.1.0-SNAPSHOT-20201118-0642-ef4a0b59.jar"
```
