## WLST Custom Class

This project automates the deployment and configuration of a WebLogic Server (WLS) environment using a custom WLST class.

## Introduction

The project revolves around a custom WebLogic Scripting Tool (WLST) class designed to manage various aspects of the WebLogic environment. From creating domains and managed servers to managing datasources, the class provides comprehensive automation capabilities.

## WLST Custom Class

Here's an overview of the class:

- Establishes a connection to a WebLogic domain.
- Provides methods for:
  - Domain creation.
  - Managed server creation.
  - Datasource configuration.
  - Application deployment.
  - ... (and more).

You can review the complete class in the `wl_main.py` file.

The primary driver of this class checks for arguments to decide which action/method to execute:

```python
if "main" in __name__:
    if len(sys.argv) > 2:
        manage = sys.argv[2]
    ...
    if sys.argv[1] == "create_domain":
        wlstc.create_domain()
    ...
```

The class leverages configurations defined in YAML, which you can review in the `domain_config.yml` (example name) file.

## Ansible Tasks

The primary tasks for this project are:

1. **Send scripts**: Uploads the `wl_main.py` WLST script to the remote machine.
2. **Run script**: Executes the WLST script on the remote machine based on the `wlst_run_job` variable.
3. **Remove tmp data**: Cleans up any temporary data created during the execution.

## Usage

1. Ensure that the desired configuration is set in the `domain_config.yml` file. This file defines all the required parameters like managed servers, datasources, etc.
2. Configure which operation you wish to run by setting the `wlst_run_job` variable in `defaults/main.yml`. By default, the job set is `get_jdbc_password`. Comment out unnecessary jobs and leave only the one you want to run.

### Example: Create One Managed Server and Its Datasource

To create just one managed server and its datasource:

1. Ensure the `domain_config.yml` file contains only the entries for the desired managed server and datasource.
2. Uncomment the `wlst_run_job` variable corresponding to the server configuration in `defaults/main.yml`.

```yaml
wlst_run_job: domain_provision
```

3. Run the Ansible playbook.

**Note**: Ensure that any other `wlst_run_job` entries in `defaults/main.yml` are commented out to prevent unintended actions.

### Examples

1. Get all jdbc passwords:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv -e wlst_run_job=get_jdbc_password
```

2. Domain creation:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv -e wlst_run_job=create_domain
```

3. Servers AND datasources configuration:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv -e wlst_run_job=domain_provision
```

4. ONLY datasources configuration:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv -e wlst_run_job=configure_datasources
```

5. ONLY datasources configuration:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv -e wlst_run_job=configure_datasources
```

6. Datasource removing (independent of its configuration in domain config file):

Set the `wlst_run_job` in the role defaults/main.yml:
```yaml
wlst_run_job: "destroy_datasource BOSQLAuth"
```

Then run the playbook without `-e` parameter:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv
```

7. List short datasource info (independent of its configuration in domain config file):

Set the `wlst_run_job` in the role defaults/main.yml:
```yaml
wlst_run_job: "list_datasource FE"
```

Then run the playbook without `-e` parameter:
```sh
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/wlst_run.yml -bKv
```

## Conclusion

This project simplifies and automates the often complex process of setting up and configuring a WebLogic environment. Ensure you have the correct configurations in place and are selecting the right `wlst_run_job` to avoid any unwanted changes.
