import java.io.FileReader as fileReader
import javax.json.Json as json

reader = json.createReader(fileReader("config.json"))
cfg = reader.read()


connect(str(cfg["admin_user"]).strip('"'), str(cfg["admin_password"]).strip('"'), str(cfg["wl_endpoint"]).strip('"'))

cd("/")
edit()
startEdit()

cd('/SecurityConfiguration/'+ str(cfg["domain_name"]).strip('"') +'/Realms/myrealm')
cmo.createAuthenticationProvider('SQLAuth', 'weblogic.security.providers.authentication.SQLAuthenticator')

cd('/SecurityConfiguration/'+ str(cfg["domain_name"]).strip('"') +'/Realms/myrealm/AuthenticationProviders/SQLAuth')
cmo.setControlFlag('OPTIONAL')
cmo.setDataSourceName('BOSQLAuth')
cmo.setSQLGetUsersPassword('SELECT ACM_API_PASSWORD_PKG.GET_PASSWORD_HASH(?) FROM DUAL')
cmo.setSQLUserExists('SELECT NAME FROM ACM_USER WHERE NAME = UPPER(?)')
cmo.setSQLListUsers('SELECT NAME FROM ACM_USER WHERE NAME LIKE ?')
cmo.setSQLIsMember('select name from acm_user where 1=1 or (\'WEB\' = UPPER(?) and name = UPPER(?))')
cmo.setSQLSetUserPassword('BEGIN ACM_API_PASSWORD_PKG.SET_PASSWORD(I_NEW_PASSWORD_HASH => ?, I_USER_NAME => ?, I_OLD_PASSWORD_HASH => NULL);COMMIT; END;')
cmo.setSQLListMemberGroups('select distinct \'WEB\' as name from acm_user where 1=1 and \'WEB\' != UPPER(?)')
cmo.setSQLListGroups('select distinct \'WEB\' as name from acm_user where 1=1 or name like ?')
cmo.setSQLCreateUser('DECLARE NAME VARCHAR2(256) := ?; PASS VARCHAR2(256) := ?; BEGIN NULL;END;')
cmo.setSQLGroupExists('select distinct \'WEB\' as name from acm_user where 1=2 or name = upper(?)')

activate()
