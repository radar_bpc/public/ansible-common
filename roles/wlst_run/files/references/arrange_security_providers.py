import java.io.FileReader as fileReader
import javax.json.Json as json

reader = json.createReader(fileReader("config.json"))
cfg = reader.read()


connect(str(cfg["admin_user"]).strip('"'), str(cfg["admin_password"]).strip('"'), str(cfg["wl_endpoint"]).strip('"'))

cd("/")
edit()
startEdit()
    
cd('/SecurityConfiguration/'+ str(cfg["domain_name"]).strip('"') +'/Realms/myrealm')
set('AuthenticationProviders',jarray.array([ObjectName('Security:Name=myrealmDefaultAuthenticator'), ObjectName('Security:Name=myrealmDefaultIdentityAsserter'), ObjectName('Security:Name=myrealmSQLAuth'), ObjectName('Security:Name=myrealmRoleMapper')], ObjectName))

activate()
