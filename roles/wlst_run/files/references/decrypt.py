# cd ~/Oracle/Middleware12_2/Oracle_Home/user_projects/domains/UAT_GUI_domain/bin/
# . ./setDomainEnv.sh
# At the level of domain (~/Oracle/Middleware/Oracle_Home_12c/user_projects/domains/test6_svng_domain):
# vim decrypt.py
# java weblogic.WLST decrypt.py </home/weblogic/path/to/domain/root/> {AES}GOzkHsx1btRPL6yey/S2AVZ1MSunsa7wuBzyeHlmLho=

# Manual case
# oracle_common/common/bin] $ ./wlst.sh 
# domain = "/hts/domains/SVGUI_base_domain"
# service = weblogic.security.internaome/weblogic/Oracle/Middleware/Oracle_Home_12_1/user_projecl.SerializedSystemIni.getEncryptionService(domain)
# encryption = weblogic.security.internal.encryption.ClearOrEncryptedService(service)
# print encryption.decrypt("{AES}WDhZb5/IP95P4eM8jwYITiZs01kawSeliV59aFog1jE=")

import weblogic.security.internal.SerializedSystemIni
import weblogic.security.internal.encryption.ClearOrEncryptedService


def decrypt(domain_home, encryptedPassword):
    encryptSrv = weblogic.security.internal.SerializedSystemIni.getEncryptionService(
        domain_home
    )
    ces = weblogic.security.internal.encryption.ClearOrEncryptedService(encryptSrv)
    password = ces.decrypt(encryptedPassword)

    print("Plaintext password is: ") + password


try:
    if len(sys.argv) == 3:
        decrypt(sys.argv[1], sys.argv[2])
    else:
        print("Read the head comment!")
except:
    print("Exception: "), sys.exc_info()[0]
    dumpStack()
    raise
