import sys
import com.oracle.cie.domain.script.jython.WLScriptContext as wlctx
import os
import jarray
from java.lang import *
from javax.management import *
import javax.management.ObjectName as ObjectName

myps1="wls:/offline>"
WLS = wlctx()
cmo = None
hideProm = "false"
recording = "false"
exitonerror = "true"
oldhook = sys.displayhook
WLSTPrompt = "true"

def command(name, *args):
  return WLS.runCmd(name, args)
def readTemplate(templateFileName, topologyProfile=None, suppressTargeting="false"):
  command("readTemplate", templateFileName, topologyProfile, suppressTargeting)
  updateCmo()
  updatePrompt()
def readTemplateForUpdate(templateFileName):
  command("readTemplateForUpdate", templateFileName)
  updateCmo()
  updatePrompt()
def selectCustomTemplate(templateFilePath):
  command("selectCustomTemplate", templateFilePath)
  updateCmo()
  updatePrompt()
def selectTemplate(templateName, templateVersion=None):
  command("selectTemplate", templateName, templateVersion)
  updateCmo()
  updatePrompt()
def unselectTemplate(templateName, templateVersion=None):
  command("unselectTemplate", templateName, templateVersion)
  updateCmo()
  updatePrompt()
def unselectCustomTemplate(templateFilePath):
  command("unselectCustomTemplate", templateFilePath)
  updateCmo()
  updatePrompt()
def loadTemplates():
  command("loadTemplates")
  updateCmo()                    
  updatePrompt()
def showTemplates(showHiddenTemplate="false"):
  return command("showTemplates", showHiddenTemplate)
def showAvailableTemplates(showHiddenTemplate="false", verbose="false", includeAppliedTemplate="false"):
  return command("showAvailableTemplates", showHiddenTemplate, verbose, includeAppliedTemplate)
def setTopologyProfile(topologyProfile):
  command("setTopologyProfile", topologyProfile)
def cd(mbeanName):
  aCmo = command("cd", mbeanName)
  updateCmo()
  updatePrompt()
  hideDisplay()
  return aCmo
def prompt(myPrompt="xx"):
  global hideProm, WLSTPrompt, myps1
  if myPrompt=="off":
    WLSTPrompt="false"
    updatePrompt()
  elif myPrompt=="on":
    WLSTPrompt="true"
    updatePrompt()
  else:
    WLSTPrompt="true"
    if hideProm=="false":
      hideProm="true"
    elif hideProm == "true":
      hideProm = "false"
    updatePrompt()

def dumpStack():
  return WLS.dumpStack()
def set(attrName, value):
  command("set", attrName,value)
  if attrName=="Name":
    updatePrompt()

def setOption(optionName, value):
  command("setOption", optionName, value)
def ls(attrName="x", returnMap="false",returnType="c"):
  obj = command("ls", attrName, returnMap, returnType)
  hideDisplay()
  return obj
def get(attrName):
  getObj = command("get", attrName)
  return getObj
def delete(name, type):
  command("delete", name, type)
def bulkDelete(names,type):
  return WLS.bulkDelete(names, type)  
def assign(srcType, srcName, destType, destName):
  command("assign", srcType, srcName, destType, destName)
def unassign(srcType, srcName, destType, destName):
  command("unassign", srcType, srcName, destType, destName)
def assignAll(srcType, destType, destName):
  command("assignAll", srcType, destType, destName)
def unassignAll(srcType, destType, destName):
  command("unassignAll", srcType, destType, destName)
def validateConfig(optionName):
  command("validateConfig", optionName)
def writeDomain(domainDir):
  command("writeDomain", domainDir)
  updatePrompt()
def writeTemplate(templateName, timeout=120000):
  if isConnected() == 'true':
    _previousTree_ = currentTree()
    try:
      domainConfig()
      if WLS.isOnlineConfigHelperAvailable(mbs):
        WLS.writeTemplate(mbs, templateName, timeout)
      else:
        print 'CIE ConfigHelper online service is not available.'
    except Exception, err:
      setDumpStackThrowable(err)
    _previousTree_()
  else:
    command("writeTemplate", templateName)
    updatePrompt()
def closeTemplate():
  global cmo, myps1
  command("closeTemplate")
  cmo = None
  hidePrompt = None
  myps1="wls:/offline>"
def readDomain(domainDir):
  command("readDomain", domainDir)
  updateCmo()
  updatePrompt()
def storeKeyStorePassword(domainTemplate,password):
  command("storeKeyStorePassword", domainTemplate,password)
def addTemplate(templateFile):
  command("addTemplate", templateFile)
def updateDomain():
  command("updateDomain")
def closeDomain():
  global cmo, myps1
  command("closeDomain")
  cmo = None
  hidePrompt = None
  myps1="wls:/offline>"
def createDomain(domainTemplate, domainDir, user, password, topologyProfile=None, nmUser='NONE', nmPassword='NONE'):
  if nmUser=="NONE" and nmPassword=="NONE":
    return command("createDomain", domainTemplate, domainDir, user, password, topologyProfile)
  else:
    return command("createDomain", domainTemplate, domainDir, user, password, topologyProfile, nmUser, nmPassword)
def pwd():
  pwdStr = WLS.getAbsPwd()
  return pwdStr
def find(name="NONE", type="NONE", searchInstancesOnly="false"):
    return command("find", name,type,searchInstancesOnly)
def loadDB(DBVersion, ConnectionPoolName, DBCategory=None):
  command("loadDB", DBVersion, ConnectionPoolName, DBCategory)
def retrieveObject(objectKey):
  return WLS.retrieveObject(objectKey)
def storeObject(key, value):
  WLS.storeObject(key, value)
def getLog():
  return WLS.getLog()
def startRecording(fileName):
  global recording
  WLS.startRecording(fileName)
def stopRecording():
  global recording
  WLS.stopRecording()
def dumpVariables():
  WLS.dumpVariables()
def exit():
  if recording=="true":
    WLS.stopRecording()

def create(name, type, baseType="NONE"):
  if baseType=="NONE":
    return command("create", name,type)
  else:
    return command("create", name, type, baseType)
def bulkCreate(simpleNames,simpleType):
  return WLS.bulkCreate(simpleNames, simpleType)
def clone(origName,cloneName,type):
  return command("clone", origName,cloneName,type)
def updateCmo():
  global cmo
  cmo = command("getCmo")
def updatePrompt():
  global hidePrompt, WLSTPrompt, myps1
  if WLSTPrompt == "false":
    myps1=sys.ps1
    return
  if hideProm == "false":
    myps1="wls:/offline"+WLS.getAbsPwd()+">"
  elif hideProm == "true":
    myps1="wls:/offline>"

def hideDisplay():
  sys.displayhook=eatdisplay
def eatdisplay(dummy):
  pass
def restoreDisplay():
  global oldhook
  sys.displayhook=oldhook
def writeIniFile(filePath):
  WLS.writeIniFile(filePath)
def setDistDestType(resName,destType):
  command("setDistDestType", resName,destType)
def getTopologyProfile():
  return command("getTopologyProfile")
def listServerGroups(printOutput="false",dynamic="false"):
  return command("listServerGroups", printOutput, dynamic)
def getServerGroups(serverName, timeout=120000):
  if isConnected() == 'true':
    try:
      _previousTree_ = currentTree()
      domainConfig()
      _svrGroups_ = WLS.getServerGroupsOnline(mbs, serverName, timeout)
      _previousTree_()
      return _svrGroups_
    except Exception, err:
      setDumpStackThrowable(err)
  else:
    return command("getServerGroups", serverName)
def setServerGroups(serverName, serverGroups, timeout=120000, skipEdit='true'):
  if serverGroups == None:
    serverGroups = []
  if isConnected() == 'false':
    command("setServerGroups", serverName, serverGroups)
  else:
    try:
      _previousTree_ = currentTree()
      domainConfig()
      WLS.setServerGroupsOnline(mbs, serverName, serverGroups, timeout)
      _previousTree_()
      editServiceMBean = ObjectName('com.bea:Name=EditService,Type=weblogic.management.mbeanservers.edit.EditServiceMBean')
      print 'Reloading configuration changes'
      configManagerMBean = mbs.getAttribute(editServiceMBean, 'ConfigurationManager')
      params = []
      signature = []
      mbs.invoke(configManagerMBean, "reload", params, signature);
    except Exception, err:
      setDumpStackThrowable(err)
def addStartupGroup(startupGroupName, serverGroupName):
  command("addStartupGroup", startupGroupName, serverGroupName)
def deleteStartupGroup(startupGroupName):
  command("deleteStartupGroup", startupGroupName)
def setStartupGroup(serverName, startupGroupName):
  command("setStartupGroup", serverName, startupGroupName)
def getStartupGroup(serverName):
  return command("getStartupGroup", serverName)
def getNodeManagerHome():
  return command("getNodeManagerHome")
def getNodeManagerType():
  return command("getNodeManagerType")
def getOldNodeManagerHome():
  return command("getOldNodeManagerHome")
def getNodeManagerUpgradeType():
  return command("getNodeManagerUpgradeType")
def getNodeManagerUpgradeOverwriteDefault():
  return command("getNodeManagerUpgradeOverwriteDefault")
def getDatabaseDefaults(connection=None):
  return command("getDatabaseDefaults", connection)
def readDomainForUpgrade(domainName, properties=None):
  command("readDomainForUpgrade", domainName, properties)
  updateCmo()
  updatePrompt()
def isConnected():
  isConnected = 'false'
  try:
    isConnected = connected
  except NameError:
    isConnected = 'false'
  return isConnected
def setSharedSecretStoreWithPassword(sharedSecretStore,secretStorePassword):
  command("setSharedSecretStoreWithPassword", sharedSecretStore, secretStorePassword)
def createServiceTable():
  return command("createServiceTable")
def createServiceTableEndPoint(serviceid, servicetype): 
  return command("createServiceTableEndPoint", serviceid, servicetype)
def createServiceTableEndPointConnection():
  return command("createServiceTableEndPointConnection")
def createServiceTableJaxWSPolicy(policyName):
  return command("createServiceTableJaxWSPolicy", policyName)
def createServiceTableT3Policy(policyName):
  return command("createServiceTableT3Policy", policyName)
def createDBDefaultsConnection():
  return command("createDBDefaultsConnection")
def setFEHostURL(plain, ssl, isPlainDefault="true"):
  command("setFEHostURL", plain, ssl, isPlainDefault)
def deleteFEHost():
  command("deleteFEHost")
def getFEHostURL(urltype):
  return command("getFEHostURL", urltype)
def clearTempCache():
  command("clearTempCache")
def setAssociatedClusterDynamicServerGroup(clusterName,dynamicServerGroupName):
  command("setAssociatedClusterDynamicServerGroup", clusterName, dynamicServerGroupName)
def convertToExtensionTemplate(stripTopology="false"):
  command("convertToExtensionTemplate", stripTopology)
  updateCmo()
  updatePrompt()
def registerForBackup(filePath,type):
  command("registerForBackup", filePath, type)
def setConfigurationType(envValue="classic"):
  command("setConfigurationType", envValue)
def setAutoTargeting(disabled='false'):
  command("setAutoTargeting", disabled)
def isASMAutoProvConfigurable():
  return command("haGetCmd", "isASMAutoProvConfigurable")
def isASMAutoProvSet():
  return command("haGetCmd", "isASMAutoProvSet")
def isASMAutoProvEnabled():
  return command("haGetCmd", "isASMAutoProvEnabled")
def enableASMAutoProv(val):
  command("haSetCmd", "enableASMAutoProv", val)
def isASMDBBasisEnabled():
  return command("haGetCmd", "isASMDBBasisEnabled")
def enableASMDBBasis(val):
  command("haSetCmd", "enableASMDBBasis", val)

def isJTATLogPersistenceConfigurable():
  return command("haGetCmd", "isJTATLogPersistenceConfigurable")
def isJTATLogDBPersistenceSet():
  return command("haGetCmd", "isJTATLogDBPersistenceSet")
def isJTATLogDBPersistenceEnabled():
  return command("haGetCmd", "isJTATLogDBPersistenceEnabled")
def enableJTATLogDBPersistence(val):
  command("haSetCmd", "enableJTATLogDBPersistence", val)

def isJMSStorePersistenceConfigurable():
  return command("haGetCmd", "isJMSStorePersistenceConfigurable")
def isJMSStoreDBPersistenceSet():
  return command("haGetCmd", "isJMSStoreDBPersistenceSet")
def isJMSStoreDBPersistenceEnabled():
  return command("haGetCmd", "isJMSStoreDBPersistenceEnabled")
def enableJMSStoreDBPersistence(val):
  command("haSetCmd", "enableJMSStoreDBPersistence", val)
def enableServiceTable(val):
  command("enableServiceTable",val)  
def initializejdbcstores(storeNames=None, serverNames=None, ddl=None):
  cmd = command("initializejdbcstores", storeNames, serverNames, ddl);
  if (cmd != None):
    exec(cmd)
def unSet(attrName):
   command("unSet", attrName)
def isSet(attrName):
   return command("isSet", attrName)
# This primitive is introduced to fix bug # 24848063, as such ordering of other configuration elements doesn't matters so keeping 
# the primitive as internal without exposing the documentation for same.
def reorder(elementName,value):
   command("reorder",elementName,value)
