class FilterModule(object):

    def filters(self):
        return {
            'rm_substr': self.rm_substr
        }

    def rm_substr(self, value, substr):
        return value.replace(substr,'')
