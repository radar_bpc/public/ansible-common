class FilterModule(object):

    def filters(self):
        return {
            'prefix': self.prefix
        }

    def prefix(self, value, prefix):
        return value.startswith(prefix)
