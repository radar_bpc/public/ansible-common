import sys

from weblogic.descriptor import BeanAlreadyExistsException


cfg = {{ domain | from_yaml }}
ks_identity_passphrase = "{{ wl_domain_ks_identity_passphrase }}"
ks_trust_passphrase = "{{ wl_domain_ks_trust_passphrase }}"
ssl_privat_key_passphrase = "{{ wl_domain_ks_identity_passphrase }}"
wl_module_file_path = "{{ wlst_scripts_path }}"
instance = "{{ instance }}"


class WLSTCustom(object):
    def __init__(
        self,
        managed_resource
    ):
        self.domain = cfg
        self.ks_identity_passphrase = ks_identity_passphrase
        self.ks_trust_passphrase = ks_trust_passphrase
        self.ssl_privat_key_passphrase = ssl_privat_key_passphrase
        self.wl_module_file_path = wl_module_file_path
        self.managed_resource = managed_resource
        self.pool_params = "connection-pool-params"

    def _connect(self, method, edit_flag=True):
        def wrapper(self, *args, **kwargs):
            connect(self.domain["admin_user"], self.domain["admin_password"], self.domain["wl_endpoint"])
            cd("/")
            if edit_flag == True:
                edit()
                startEdit()

            result = method(*args, **kwargs)

            if edit_flag == True:
                activate()

            return result

        return wrapper

    def connect(self, method, edit_flag=True):
        m = self._connect(method, edit_flag)
        m(self)

    def make_wl_module(self):
        writeIniFile(self.wl_module_file_path + "/wl.py")

    def create_domain(self, version=12.2):
        # read_domain_template
        if version < 12.2:
            # Load the template. Versions < 12.2
            readTemplate(cfg["domain_base_template"])
        else:
            # Load the template. Versions >= 12.2
            selectTemplate("Basic WebLogic Server Domain")
            loadTemplates()

        # create_admin_server
        cd("Servers/AdminServer")
        set("ListenAddress", "")
        set("ListenPort", cfg["admin_server_port"])

        try:
            create("AdminServer", "SSL")
        except BeanAlreadyExistsException:
            pass
        cd("SSL/AdminServer")
        set("Enabled", cfg["ssl_enabled"])
        set("ListenPort", cfg["admin_server_ssl_port"])

        # define_user_password
        cd("/")
        cd("Security/base_domain/User/" + cfg["admin_user"])
        cmo.setPassword(cfg["admin_password"])

        # write_domain
        setOption("OverwriteDomain", "true")
        writeDomain(
            cfg["middleware_home"] + "/user_projects/domains/" + cfg["domain_name"]
        )
        closeTemplate()

    def _create_machine(self):
        machine_name = cfg.get("machine_name", "Machine1")
        cd("/")
        try:
            cmo.createUnixMachine(machine_name)
        except BeanAlreadyExistsException:
            pass
        cd("/Machines/" + machine_name + "/NodeManager/" + machine_name)
        cmo.setNMType("SSL")
        cmo.setListenAddress("localhost")
        cmo.setListenPort(5556)
        cmo.setDebugEnabled(False)

    def _create_server(self, server):
        server_name = server["server_name"]
        try:
            machine_name = server["machine_name"]
        except:
            machine_name = "Machine1"
        listen_port = server["listen_port"]
        listen_ssl_port = server["listen_ssl_port"]
        try:
            ssl_enabled = server["ssl_enabled"]
        except:
            ssl_enabled = True
        jvm_args = server["jvm_args"]
        try:
            maxStuckThreadTime = server["maxStuckThreadTime"]
        except:
            maxStuckThreadTime = 600

        cd("/")
        try:
            cmo.createServer(server_name)
        except BeanAlreadyExistsException:
            pass
        cd("/Servers/" + server_name)
        cmo.setListenAddress("")
        cmo.setListenPort(listen_port)
        cmo.setListenPortEnabled(True)
        cmo.setClientCertProxyEnabled(False)
        cmo.setJavaCompiler("javac")
        cmo.setMachine(getMBean("/Machines/" + machine_name))
        cmo.setCluster(None)
        cd("/Servers/" + server_name + "/SSL/" + server_name)
        cmo.setEnabled(ssl_enabled)
        cmo.setListenPort(listen_ssl_port)
        cd("/Servers/" + server_name + "/ServerDiagnosticConfig/" + server_name)
        cmo.setWLDFDiagnosticVolume("Low")
        cd("/Servers/" + server_name + "/ServerStart/" + server_name)
        cmo.setArguments(jvm_args)
        cd("/Servers/" + server_name + "/OverloadProtection/" + server_name)
        cmo.setFailureAction("no-action")
        cmo.setSharedCapacityForWorkManagers(65536)
        cmo.setPanicAction("system-exit")
        cmo.setFreeMemoryPercentLowThreshold(0)
        cmo.setFreeMemoryPercentHighThreshold(0)
        try:
            cmo.createServerFailureTrigger()
        except BeanAlreadyExistsException:
            pass
        cd(
            "/Servers/"
            + server_name
            + "/OverloadProtection/"
            + server_name
            + "/ServerFailureTrigger/"
            + server_name
        )
        if maxStuckThreadTime:
            cmo.setMaxStuckThreadTime(maxStuckThreadTime)
        cmo.setStuckThreadCount(0)

    def _setup_keystore(self, server):
        server_name = server["server_name"]
        try:
            ks_identity_passphrase = server["ks_identity_passphrase"]
        except:
            ks_identity_passphrase = self.ks_identity_passphrase
        try:
            ks_trust_passphrase = server["ks_trust_passphrase"]
        except:
            ks_trust_passphrase = self.ks_trust_passphrase
        try:
            ssl_privat_key_passphrase = server["ssl_privat_key_passphrase"]
        except:
            ssl_privat_key_passphrase = self.ssl_privat_key_passphrase
        try:
            keystores_type = server["keystores_type"]
        except:
            if server_name == "AdminServer":
                # keystores_type = "CustomIdentityAndJavaStandardTrust"
                keystores_type = "DemoIdentityAndDemoTrust"
            else:
                keystores_type = "CustomIdentityAndCustomTrust"

        if keystores_type == "CustomIdentityAndCustomTrust":
            cd("/Servers/" + server_name)
            cmo.setKeyStores(keystores_type)
            cmo.setCustomIdentityKeyStoreFileName(
                "/home/weblogic/OCI-Certs/identity.jks"
            )
            cmo.setCustomIdentityKeyStoreType("JKS")
            cmo.setCustomIdentityKeyStorePassPhrase(ks_identity_passphrase)
            cmo.setCustomTrustKeyStoreFileName("/home/weblogic/OCI-Certs/trust.jks")
            cmo.setCustomTrustKeyStoreType("JKS")
            cmo.setCustomTrustKeyStorePassPhrase(ks_trust_passphrase)
            cd("/Servers/" + server_name + "/SSL/" + server_name)
            cmo.setServerPrivateKeyAlias("weblogic")
            cmo.setServerPrivateKeyPassPhrase(ssl_privat_key_passphrase)
        if keystores_type == "CustomIdentityAndJavaStandardTrust":
            cd("/Servers/" + server_name)
            cmo.setKeyStores(keystores_type)
            cmo.setCustomIdentityKeyStoreFileName(
                "/home/weblogic/OCI-Certs/identity.jks"
            )
            cmo.setCustomIdentityKeyStoreType("JKS")
            cmo.setCustomIdentityKeyStorePassPhrase(ks_identity_passphrase)
            cmo.setJavaStandardTrustKeyStorePassPhrase("changeit")
            cd("/Servers/" + server_name + "/SSL/" + server_name)
            cmo.setServerPrivateKeyAlias("weblogic")
            cmo.setServerPrivateKeyPassPhrase(ssl_privat_key_passphrase)

    def _get_config_value(self, resource, key, default, sub_key=None):
        try:
            if sub_key:
                return resource[sub_key][key]
            else:
                return resource[key]
        except KeyError:
            return default

    def _create_generic_datasource(self, datasource):

        targets = datasource["targets"]
        db_password = datasource["db_password"]
        name_ds = datasource["name"]
        user_name = datasource["user"]
        jndi_names = datasource["jndi"]
        db_service_host = datasource["host"]
        db_service_endpoint = datasource["db_service"]

        maxCapacity = self._get_config_value(datasource, "max-capacity", 50, self.pool_params)
        driver_name = self._get_config_value(datasource, "driver_name", "oracle.jdbc.OracleDriver")
        test_table = self._get_config_value(datasource, "test-table-name", "DUAL", self.pool_params)
        phase_commit = self._get_config_value(datasource, "phase_commit", "OnePhaseCommit")
        initialCapacity = self._get_config_value(datasource, "initial-capacity", 5, self.pool_params)
        minCapacity = self._get_config_value(datasource, "min-capacity", 5, self.pool_params)
        statementCacheSize = self._get_config_value(datasource, "statement-cache-size", 10, self.pool_params)
        statementCacheType = self._get_config_value(datasource, "statement-cache-type", "LRU", self.pool_params)
        inactiveConnectionTimeoutSeconds = self._get_config_value(datasource, "inactive-connection-timeout-seconds", 10, self.pool_params)
        testConnectionsOnReserve = self._get_config_value(datasource, "test-connections-on-reserve", True, self.pool_params)
        pinnedToThread = self._get_config_value(datasource, "pinned-to-thread", False, self.pool_params)
        highestNumWaiters = self._get_config_value(datasource, "highest-num-waiters", 2147483647, self.pool_params)
        countOfRefreshFailuresTillDisable = self._get_config_value(datasource, "count-of-refresh-failures-till-disable", 2, self.pool_params)
        statementTimeout = self._get_config_value(datasource, "statement-timeout", -1, self.pool_params)
        connectionHarvestMaxCount = self._get_config_value(datasource, "connection-harvest-max-count", 1, self.pool_params)
        loginDelaySeconds = self._get_config_value(datasource, "login-delay-seconds", 0, self.pool_params)
        countOfTestFailuresTillFlush = self._get_config_value(datasource, "count-of-test-failures-till-flush", 2, self.pool_params)
        secondsToTrustAnIdlePoolConnection = self._get_config_value(datasource, "seconds-to-trust-an-idle-pool-connection", 10, self.pool_params)
        shrinkFrequencySeconds = self._get_config_value(datasource, "shrink-frequency-seconds", 900, self.pool_params)
        connectionHarvestTriggerCount = self._get_config_value(datasource, "connection-harvest-trigger-count", -1, self.pool_params)
        connectionReserveTimeoutSeconds = self._get_config_value(datasource, "connection-reserve-timeout-seconds", 10, self.pool_params)
        removeInfectedConnections = self._get_config_value(datasource, "remove-infected-connections", True, self.pool_params)
        ignoreInUseConnectionsEnabled = self._get_config_value(datasource, "ignore-in-use-connections-enabled", True, self.pool_params)
        connectionCreationRetryFrequencySeconds = self._get_config_value(datasource, "connection-creation-retry-frequency-seconds", 2, self.pool_params)
        testFrequencySeconds = self._get_config_value(datasource, "test-frequency-seconds", 10, self.pool_params)
        wrapTypes =  self._get_config_value(datasource, "wrap-types", True, self.pool_params)

        cd("/")
        try:
            print("Creating " + name_ds)
            cmo.createJDBCSystemResource(name_ds)
            print("...done")
        except BeanAlreadyExistsException:
            print("...already exists")
        cd("/JDBCSystemResources/" + name_ds + "/JDBCResource/" + name_ds)
        cmo.setName(name_ds)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDataSourceParams/"
            + name_ds
        )
        set(
            "JNDINames",
            jarray.array([String("" + jndi_name) for jndi_name in jndi_names], String),
        )
        cd("/JDBCSystemResources/" + name_ds + "/JDBCResource/" + name_ds)
        cmo.setDatasourceType("GENERIC")
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDriverParams/"
            + name_ds
        )
        cmo.setUrl("jdbc:oracle:thin:@//" + db_service_host + "/" + db_service_endpoint)
        cmo.setDriverName(driver_name)
        cmo.unSet("Password")
        cmo.setPassword(db_password)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCConnectionPoolParams/"
            + name_ds
        )
        cmo.setTestTableName(test_table)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDriverParams/"
            + name_ds
            + "/Properties/"
            + name_ds
        )
        try:
            cmo.createProperty("user")
        except BeanAlreadyExistsException:
            pass
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDriverParams/"
            + name_ds
            + "/Properties/"
            + name_ds
            + "/Properties/user"
        )
        cmo.setValue(user_name)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDataSourceParams/"
            + name_ds
        )
        cmo.setGlobalTransactionsProtocol(phase_commit)
        cd("/JDBCSystemResources/" + name_ds)
        set(
            "Targets",
            jarray.array(
                [
                    ObjectName("com.bea:Name=" + target + ",Type=Server")
                    for target in targets
                ],
                ObjectName,
            ),
        )
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDriverParams/"
            + name_ds
            + "/Properties/"
            + name_ds
            + "/Properties/user"
        )
        cmo.unSet("SysPropValue")
        cmo.unSet("EncryptedValue")
        cmo.setValue(user_name)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCConnectionPoolParams/"
            + name_ds
        )
        cmo.setInitialCapacity(initialCapacity)
        cmo.setMinCapacity(minCapacity)
        cmo.setStatementCacheSize(statementCacheSize)
        cmo.setMaxCapacity(maxCapacity)
        cmo.setStatementCacheType(statementCacheType)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCDriverParams/"
            + name_ds
            + "/Properties/"
            + name_ds
            + "/Properties/user"
        )
        cmo.unSet("SysPropValue")
        cmo.unSet("EncryptedValue")
        cmo.setValue(user_name)
        cd(
            "/JDBCSystemResources/"
            + name_ds
            + "/JDBCResource/"
            + name_ds
            + "/JDBCConnectionPoolParams/"
            + name_ds
        )
        cmo.setInactiveConnectionTimeoutSeconds(inactiveConnectionTimeoutSeconds)
        cmo.setTestConnectionsOnReserve(testConnectionsOnReserve)
        cmo.setWrapTypes(wrapTypes)
        cmo.setPinnedToThread(pinnedToThread)
        cmo.setHighestNumWaiters(highestNumWaiters)
        cmo.setCountOfRefreshFailuresTillDisable(countOfRefreshFailuresTillDisable)
        cmo.setStatementTimeout(statementTimeout)
        cmo.setConnectionHarvestMaxCount(connectionHarvestMaxCount)
        cmo.setLoginDelaySeconds(loginDelaySeconds)
        cmo.setCountOfTestFailuresTillFlush(countOfTestFailuresTillFlush)
        cmo.setSecondsToTrustAnIdlePoolConnection(secondsToTrustAnIdlePoolConnection)
        cmo.setShrinkFrequencySeconds(shrinkFrequencySeconds)
        cmo.setConnectionHarvestTriggerCount(connectionHarvestTriggerCount)
        cmo.setConnectionReserveTimeoutSeconds(connectionReserveTimeoutSeconds)
        cmo.setRemoveInfectedConnections(removeInfectedConnections)
        cmo.setTestTableName(test_table)
        cmo.setIgnoreInUseConnectionsEnabled(ignoreInUseConnectionsEnabled)
        cmo.setConnectionCreationRetryFrequencySeconds(
            connectionCreationRetryFrequencySeconds
        )
        cmo.setTestFrequencySeconds(testFrequencySeconds)

    def _create_multi_datasource(self, multisource):
        targets = multisource["targets"]
        name_ds_multi = multisource["name"]
        jndis_multi = multisource["jndi"]
        dsorses = multisource["dsorses"]

        try:
            inactiveConnectionTimeoutSeconds = multisource["connection-pool-params"][
                "inactive-connection-timeout-seconds"
            ]
        except:
            pass

        cd("/")
        try:
            print("Creating " + name_ds_multi)
            cmo.createJDBCSystemResource(name_ds_multi)
            print("...done")
        except BeanAlreadyExistsException:
            print("...already exists")
        cd("/JDBCSystemResources/" + name_ds_multi + "/JDBCResource/" + name_ds_multi)
        cmo.setName(name_ds_multi)
        cd(
            "/JDBCSystemResources/"
            + name_ds_multi
            + "/JDBCResource/"
            + name_ds_multi
            + "/JDBCDataSourceParams/"
            + name_ds_multi
        )
        set(
            "JNDINames",
            jarray.array(
                [String("" + jndi_multi) for jndi_multi in jndis_multi], String
            ),
        )
        cmo.setAlgorithmType("Failover")
        cmo.setDataSourceList(",".join([dsorse for dsorse in dsorses]))
        cd("/JDBCSystemResources/" + name_ds_multi)
        set(
            "Targets",
            jarray.array(
                [
                    ObjectName("com.bea:Name=" + target + ",Type=Server")
                    for target in targets
                ],
                ObjectName,
            ),
        )
        try:
            cd(
                "/JDBCSystemResources/"
                + name_ds_multi
                + "/JDBCResource/"
                + name_ds_multi
                + "/JDBCConnectionPoolParams/"
                + name_ds_multi
            )
            cmo.setInactiveConnectionTimeoutSeconds(inactiveConnectionTimeoutSeconds)
        except NameError:
            pass

    def set_min_capacity(self, default=5):
        for datasource in self.domain["datasources"]:
            try:
                name_ds = datasource["name"]
                minCapacity = self._get_config_value(datasource, "min-capacity", default, self.pool_params)
                cd(
                    "/JDBCSystemResources/"
                    + name_ds
                    + "/JDBCResource/"
                    + name_ds
                    + "/JDBCConnectionPoolParams/"
                    + name_ds
                )
                cmo.setMinCapacity(minCapacity)
                print("Min capacity of " + datasource["name"] + " has been fixed.")
            except Exception, e:
                print("Exception in " + name_ds)
                print(e)

    def show_min_capacity(self, default=5):
        for datasource in self.domain["datasources"]:
            try:
                name_ds = datasource["name"]
                minCapacity = self._get_config_value(datasource, "min-capacity", default, self.pool_params)
                cd(
                    "/JDBCSystemResources/"
                    + name_ds
                    + "/JDBCResource/"
                    + name_ds
                    + "/JDBCConnectionPoolParams/"
                    + name_ds
                )
                cmo.getMinCapacity()
                print("Min capacity of " + datasource["name"] + " (IS - SHOULD BE) (" + str(cmo.getMinCapacity()) + " - " + str(minCapacity) + ")")
            except Exception, e:
                print("Exception in " + name_ds)
                print(e)

    def destroy_datasource(self):
        try:
            print("Destroying " + self.managed_resource)
            cmo.destroyJDBCSystemResource(
                getMBean("/JDBCSystemResources/" + self.managed_resource)
            )
            print("...Done")
        except NameError:
            print("...Not found")
    
    def list_datasource(self):
        try:
            print("List " + self.managed_resource)
            ls("JDBCSystemResources/" 
               + self.managed_resource 
               + "/JDBCResource/" 
               + self.managed_resource 
               + "/JDBCDataSourceParams/" 
               + self.managed_resource)
            ls("JDBCSystemResources/" 
               + self.managed_resource 
               + "/JDBCResource/" 
               + self.managed_resource 
               + "/JDBCDriverParams/" 
               + self.managed_resource)
        except NameError:
            print("...Not found")

    def _deploy_application(self, app, is_library="false"):
        name = app["name"]
        path = app["artifact"]
        targets = app["targets"]
        try:
            stage = app["stage"]
        except:
            stage = "stage"
        try:
            is_library = app["is_library"]
        except:
            pass

        deploy(
            name,
            path,
            targets,
            stage,
            libraryModule=is_library,
            block=False,
            upload=True,
            remote=False,
        )

    def _enable_debug(self, server):
        cd(
            "/Servers/"
            + server["server_name"]
            + "/ServerDebug/"
            + server["server_name"]
        )
        try:
            cmo.createDebugScope("weblogic.jdbc.sql")
        except BeanAlreadyExistsException:
            pass
        cd(
            "/Servers/"
            + server["server_name"]
            + "/ServerDebug/"
            + server["server_name"]
            + "/DebugScopes/weblogic.jdbc.sql"
        )
        cmo.setEnabled(True)
        cd(
            "/Servers/"
            + server["server_name"]
            + "/ServerDebug/"
            + server["server_name"]
        )
        cmo.setDebugJDBCSQL(True)

    def _set_server_log(self, server):
        cd("/Servers/" + server["server_name"] + "/Log/" + server["server_name"])
        cmo.setLogFileRotationDir(
            "/home/weblogic/archive/" + domainName + "/" + server["server_name"]
        )
        cmo.setDateFormatPattern("MMM d, yyyy h:mm:ss,SSS a z")
        cmo.setFileName("logs/%yyyyMMddhhmm%-" + server["server_name"] + ".log")
        cmo.setDomainLogBroadcastSeverity("Notice")
        cmo.setStdoutFormat("standard")
        cmo.setStdoutSeverity("Debug")
        cmo.setRotationType("bySize")
        cmo.setLoggerSeverity("Debug")
        cmo.setLogFileSeverity("Trace")
        cmo.setRedirectStderrToServerLogEnabled(True)
        cmo.setRedirectStdoutToServerLogEnabled(True)
        cmo.setNumberOfFilesLimited(True)
        cmo.setLogMonitoringEnabled(True)
        cmo.setRotateLogOnStartup(False)
        cmo.setStdoutLogStack(True)
        cmo.setLogMonitoringMaxThrottleMessageSignatureCount(1000)
        cmo.setLogMonitoringThrottleMessageLength(50)
        cmo.setLogMonitoringThrottleThreshold(1500)
        cmo.setDomainLogBroadcasterBufferSize(10)
        cmo.setLogMonitoringIntervalSecs(30)
        cmo.setStacktraceDepth(5)
        cmo.setFileMinSize(50000)
        cmo.setBufferSizeKB(8)
        cmo.setFileCount(365)

    def _set_web_server_log(self, server):
        cd(
            "/Servers/"
            + server["server_name"]
            + "/WebServer/"
            + server["server_name"]
            + "/WebServerLog/"
            + server["server_name"]
        )
        cmo.setFileName("logs/%yyyyMMddhhmm%-" + server["server_name"] + "-access.log")
        cmo.setLogFileRotationDir(
            "/home/weblogic/archive/" + domainName + "/" + server["server_name"]
        )
        cmo.setELFFields("date time cs-method cs-uri sc-status")
        cmo.setLogFileFormat("common")
        cmo.setRotationType("bySize")
        cmo.setNumberOfFilesLimited(False)
        cmo.setRotateLogOnStartup(False)
        cmo.setLoggingEnabled(True)
        cmo.setLogTimeInGMT(False)
        cmo.setFileMinSize(50000)
        cmo.setBufferSizeKB(8)
        cmo.setFileCount(365)

    def _set_datasource_log(self, server):
        cd(
            "/Servers/"
            + server["server_name"]
            + "/DataSource/"
            + server["server_name"]
            + "/DataSourceLogFile/"
            + server["server_name"]
        )
        cmo.setLogFileRotationDir(
            "/home/weblogic/archive/" + domainName + "/" + server["server_name"]
        )
        cmo.setFileName(
            "logs/%yyyyMMddhhmm%-" + server["server_name"] + "-datasource.log"
        )
        cmo.setRotationType("bySize")
        cmo.setNumberOfFilesLimited(False)
        cmo.setRotateLogOnStartup(False)
        cmo.setFileMinSize(50000)
        cmo.setFileCount(100)

    def _enable_ssl(self, server):
        server_name = server["server_name"]
        listen_ssl_port = int(server["listen_ssl_port"])
        ssl_enabled = server["ssl_enabled"]
        jvm_args = server["jvm_args"]

        cd("/")
        cd("/Servers/" + server_name)
        cmo.setClientCertProxyEnabled(False)
        cd("/Servers/" + server_name + "/SSL/" + server_name)
        cmo.setEnabled(ssl_enabled)
        cmo.setListenPort(listen_ssl_port)
        cd("/Servers/" + server_name + "/ServerStart/" + server_name)
        cmo.setArguments(jvm_args)

    def _enable_production_mode(self):
        cd("/")
        cmo.setProductionModeEnabled(True)

    def get_jdbc_password(self):
        connect(self.domain["admin_user"], self.domain["admin_password"], self.domain["wl_endpoint"])

        domain_name = get("Name")
        ds_list = ls("/JDBCSystemResources")
        ds_list = [l.strip() for l in ds_list.split("\ndr--") if l != "" and 'MULTI' not in l and 'Multi' not in l and 'multi' not in l]

        edit()
        startEdit()

        cd("/SecurityConfiguration/" + domain_name)
        set("ClearTextCredentialAccessEnabled", "true")
        activate()

        passwords = {}
        for i in ds_list:
            
            try:
                cd("/JDBCSystemResources/"+ i +"/JDBCResource/"+ i +"/JDBCDriverParams/"+ i)
                passwords[i] = get("Password")
            except Exception, e:
                print(e)
                print("Path of object with error: /JDBCSystemResources/"+ i +"/JDBCResource/"+ i +"/JDBCDriverParams/"+ i)

        edit()
        startEdit()
        cd("/SecurityConfiguration/" + domain_name)
        set("ClearTextCredentialAccessEnabled", "false")
        activate()

        print(passwords)

    def _set_jms_smartx(self):
        try:
            cmo.createJMSServer("JMSServer-0")
        except BeanAlreadyExistsException:
            pass

        cd("/JMSServers/JMSServer-0")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SMARTX-QCOMM,Type=Server")], ObjectName
            ),
        )

        cd("/")
        try:
            cmo.createJMSSystemResource("SystemModule-0", "jms/systemmodule-0-jms.xml")
        except BeanAlreadyExistsException:
            pass

        cd("/JMSSystemResources/SystemModule-0")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SMARTX-QCOMM,Type=Server")], ObjectName
            ),
        )

        try:
            cmo.createSubDeployment("SmartVistaConnectionFactory-subdeployment")
        except BeanAlreadyExistsException:
            pass

        cd("/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0")
        try:
            cmo.createConnectionFactory("SmartVistaConnectionFactory")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/ConnectionFactories/SmartVistaConnectionFactory/SecurityParams/SmartVistaConnectionFactory"
        )
        cmo.setAttachJMSXUserId(false)

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/ConnectionFactories/SmartVistaConnectionFactory/ClientParams/SmartVistaConnectionFactory"
        )
        cmo.setClientIdPolicy("Restricted")
        cmo.setSubscriptionSharingPolicy("Exclusive")
        cmo.setMessagesMaximum(10)

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/ConnectionFactories/SmartVistaConnectionFactory/TransactionParams/SmartVistaConnectionFactory"
        )
        cmo.setXAConnectionFactoryEnabled(true)

        cd(
            "/JMSSystemResources/SystemModule-0/SubDeployments/SmartVistaConnectionFactory-subdeployment"
        )
        set(
            "Targets",
            jarray.array(
                [
                    ObjectName("com.bea:Name=SMARTX-QCOMM,Type=Server"),
                    ObjectName("com.bea:Name=JMSServer-0,Type=JMSServer"),
                ],
                ObjectName,
            ),
        )

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/ConnectionFactories/SmartVistaConnectionFactory"
        )
        cmo.setSubDeploymentName("SmartVistaConnectionFactory-subdeployment")

        cd("/JMSSystemResources/SystemModule-0")
        try:
            cmo.createSubDeployment("SmartVistaQueue-Subdeployment")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/ConnectionFactories/SmartVistaConnectionFactory"
        )
        cmo.setDefaultTargetingEnabled(false)
        cmo.setJNDIName("jms/SmartVistaQM")

        cd("/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0")
        try:
            cmo.createQueue("SVSMXCARD_IN")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/Queues/SVSMXCARD_IN"
        )
        cmo.setJNDIName("jms/SmartVistaCardResponse")
        cmo.setSubDeploymentName("SmartVistaQueue-Subdeployment")

        cd(
            "/JMSSystemResources/SystemModule-0/SubDeployments/SmartVistaQueue-Subdeployment"
        )
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=JMSServer-0,Type=JMSServer")], ObjectName
            ),
        )

        cd("/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0")
        try:
            cmo.createQueue("SVSMXCARD_OUT")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/Queues/SVSMXCARD_OUT"
        )
        cmo.setJNDIName("jms/SmartVistaCardRequest")
        cmo.setSubDeploymentName("SmartVistaQueue-Subdeployment")

        cd(
            "/JMSSystemResources/SystemModule-0/SubDeployments/SmartVistaQueue-Subdeployment"
        )
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=JMSServer-0,Type=JMSServer")], ObjectName
            ),
        )

        cd("/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0")
        try:
            cmo.createQueue("SVSMXTOPUP_IN")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/Queues/SVSMXTOPUP_IN"
        )
        cmo.setJNDIName("jms/SmartVistaTopupRequest")
        cmo.setSubDeploymentName("SmartVistaQueue-Subdeployment")

        cd(
            "/JMSSystemResources/SystemModule-0/SubDeployments/SmartVistaQueue-Subdeployment"
        )
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=JMSServer-0,Type=JMSServer")], ObjectName
            ),
        )

        cd("/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0")
        try:
            cmo.createQueue("SVSMXTOPUP_OUT")
        except BeanAlreadyExistsException:
            pass

        cd(
            "/JMSSystemResources/SystemModule-0/JMSResource/SystemModule-0/Queues/SVSMXTOPUP_OUT"
        )
        cmo.setJNDIName("jms/SmartVistaTopupResponse")
        cmo.setSubDeploymentName("SmartVistaQueue-Subdeployment")

        cd(
            "/JMSSystemResources/SystemModule-0/SubDeployments/SmartVistaQueue-Subdeployment"
        )
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=JMSServer-0,Type=JMSServer")], ObjectName
            ),
        )

    def _set_jms_fe(self):
        cd("/")
        try:
            cmo.createJMSServer("SVWI_auto_1")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSServers/SVWI_auto_1")
        set(
            "Targets",
            jarray.array([ObjectName("com.bea:Name=FE,Type=Server")], ObjectName),
        )
        cd("/")
        try:
            cmo.createJMSServer("SVWI_auto_2")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSServers/SVWI_auto_2")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=AdminServer,Type=Server")], ObjectName
            ),
        )
        cd("/")
        try:
            cmo.createJMSSystemResource("Audit")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSSystemResources/Audit")
        set(
            "Targets",
            jarray.array(
                [
                    ObjectName("com.bea:Name=AdminServer,Type=Server"),
                    ObjectName("com.bea:Name=FE,Type=Server"),
                ],
                ObjectName,
            ),
        )
        try:
            cmo.createSubDeployment("SVWI2144412650")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSSystemResources/Audit/SubDeployments/SVWI2144412650")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SVWI_auto_2,Type=JMSServer")], ObjectName
            ),
        )
        cd("/JMSSystemResources/Audit")
        try:
            cmo.createSubDeployment("SVWI992068369")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSSystemResources/Audit/SubDeployments/SVWI992068369")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SVWI_auto_1,Type=JMSServer")], ObjectName
            ),
        )
        cd("/JMSSystemResources/Audit/JMSResource/Audit")
        try:
            cmo.createConnectionFactory("AuditConnectionFactory")
        except BeanAlreadyExistsException:
            pass
        cd(
            "/JMSSystemResources/Audit/JMSResource/Audit/ConnectionFactories/AuditConnectionFactory"
        )
        cmo.setJNDIName("jms/svwi/AuditConnectionFactory")
        cd(
            "/JMSSystemResources/Audit/JMSResource/Audit/ConnectionFactories/AuditConnectionFactory/SecurityParams/AuditConnectionFactory"
        )
        cmo.setAttachJMSXUserId(False)
        cd(
            "/JMSSystemResources/Audit/JMSResource/Audit/ConnectionFactories/AuditConnectionFactory/ClientParams/AuditConnectionFactory"
        )
        cmo.setClientIdPolicy("Restricted")
        cmo.setSubscriptionSharingPolicy("Exclusive")
        cmo.setMessagesMaximum(10)
        cd(
            "/JMSSystemResources/Audit/JMSResource/Audit/ConnectionFactories/AuditConnectionFactory/TransactionParams/AuditConnectionFactory"
        )
        cmo.setXAConnectionFactoryEnabled(True)
        cd(
            "/JMSSystemResources/Audit/JMSResource/Audit/ConnectionFactories/AuditConnectionFactory"
        )
        cmo.setDefaultTargetingEnabled(True)
        cd("/JMSSystemResources/Audit/JMSResource/Audit")
        try:
            cmo.createQueue("AuditQueue_auto_2")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSSystemResources/Audit/JMSResource/Audit/Queues/AuditQueue_auto_2")
        cmo.setJNDIName("jms/svwi/AuditQueue")
        cmo.setSubDeploymentName("SVWI992068369")
        cd("/JMSSystemResources/Audit/SubDeployments/SVWI992068369")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SVWI_auto_1,Type=JMSServer")], ObjectName
            ),
        )
        cd("/JMSSystemResources/Audit/JMSResource/Audit")
        try:
            cmo.createQueue("AuditQueue_auto_3")
        except BeanAlreadyExistsException:
            pass
        cd("/JMSSystemResources/Audit/JMSResource/Audit/Queues/AuditQueue_auto_3")
        cmo.setJNDIName("jms/svwi/AuditQueue")
        cmo.setSubDeploymentName("SVWI2144412650")
        cd("/JMSSystemResources/Audit/SubDeployments/SVWI2144412650")
        set(
            "Targets",
            jarray.array(
                [ObjectName("com.bea:Name=SVWI_auto_2,Type=JMSServer")], ObjectName
            ),
        )

    def _set_key_access(self):
        storeUserConfig(cfg.get("user_config_file"), cfg.get("user_key_file"))

    def start_managed_server(self):
        start(self.managed_resource, 'Server')

    def stop_managed_server(self):
        shutdown(self.managed_resource, 'Server', force='true')

    def _advanced_config_admin_server(self):

        cd("/Servers/AdminServer/SSL/AdminServer")
        cmo.setExportKeyLifespan(500)
        cmo.setUseServerCerts(False)
        cmo.setSSLRejectionLoggingEnabled(True)
        cmo.setAllowUnencryptedNullCipher(False)
        cmo.setInboundCertificateValidation("BuiltinSSLValidationOnly")
        cmo.setOutboundCertificateValidation("BuiltinSSLValidationOnly")
        cmo.setHostnameVerificationIgnored(True)
        cmo.setHostnameVerifier(None)
        cmo.setTwoWaySSLEnabled(False)
        cmo.setClientCertificateEnforced(False)
        
    def configure_datasources(self):
        for datasource in self.domain["datasources"]:
            if "destroy" not in datasource:
                self._create_generic_datasource(datasource)
            else:
                if datasource["destroy"] != True:
                    self._create_generic_datasource(datasource)
                else:
                    print("...not created because of destroy=True")
                
        for multisource in self.domain["multisources"]:
            if "destroy" not in multisource:
                self._create_multi_datasource(multisource)
            else:
                if multisource["destroy"] != True:
                    self._create_multi_datasource(multisource)
                else:
                    print("...not created because of destroy=True")
    
    def configure_servers(self):
        for server in self.domain["servers"]:
            self._create_server(server)
            self._setup_keystore(server)
            print("Server created:", server["server_name"])
            self._set_server_log(server)
            self._set_web_server_log(server)
            self._set_datasource_log(server)
            print("Logrotate configured for server:", server["server_name"])
        
        self._advanced_config_admin_server()
        self._setup_keystore({"server_name": "AdminServer"})
        
        for server in self.domain["servers"]:
            if server["server_name"] == "FE":
                self._set_jms_fe()
    
    def domain_provision(self):
        self._set_key_access()
        self._create_machine()
        
        for server in self.domain["servers"]:
            self._create_server(server)
            self._setup_keystore(server)
            print("Server created:", server["server_name"])
            self._set_server_log(server)
            self._set_web_server_log(server)
            self._set_datasource_log(server)
            print("Logrotate configured for server:", server["server_name"])

        self._advanced_config_admin_server()
        self._setup_keystore({"server_name": "AdminServer"})
        
        self.configure_datasources()
        
        for application in self.domain["applications"]:
            self._deploy_application(application)
        
        if "libraries" in self.domain:
            for lib in self.domain["libraries"]:
                self._deploy_application(lib, is_library="true")
            
        for server in self.domain["servers"]:
            if server["server_name"] == "FE":
                self._set_jms_fe()

        if instance == "prod" or instance == "uat":
            self._enable_production_mode()

    def configure_keystores(self):
        for server in self.domain["servers"]:
            self._setup_keystore(server)
            
        self._setup_keystore({"server_name": "AdminServer"})


if "main" in __name__:
    
    if len(sys.argv) > 2:
        manage = sys.argv[2]
    else:
        manage = None

    wlstc = WLSTCustom(manage)
    
    if sys.argv[1] == "make_wl_module":
        wlstc.connect(wlstc.make_wl_module, edit_flag=False)

    elif sys.argv[1] == "create_domain":
        wlstc.create_domain()
    
    elif sys.argv[1] == "configure_servers":
        wlstc.connect(wlstc.configure_servers)
    
    elif sys.argv[1] == "domain_provision":
        wlstc.connect(wlstc.domain_provision)

    elif sys.argv[1] == "configure_datasources":
        wlstc.connect(wlstc.configure_datasources)
    
    elif sys.argv[1] == "destroy_datasource":
        wlstc.connect(wlstc.destroy_datasource)
    
    elif sys.argv[1] == "list_datasource":
        wlstc.connect(wlstc.list_datasource)
    
    elif sys.argv[1] == "start_managed_server":
        wlstc.connect(wlstc.start_managed_server, edit_flag=False)
    
    elif sys.argv[1] == "stop_managed_server":
        wlstc.connect(wlstc.stop_managed_server, edit_flag=False)
    
    elif sys.argv[1] == "get_jdbc_password":
        wlstc.get_jdbc_password()

    elif sys.argv[1] == "set_min_capacity":
        wlstc.connect(wlstc.set_min_capacity)

    elif sys.argv[1] == "show_min_capacity":
        wlstc.connect(wlstc.show_min_capacity)

    elif sys.argv[1] == "configure_keystores":
        wlstc.connect(wlstc.configure_keystores)
