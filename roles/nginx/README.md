nginx installing
===

## Variables

All variables specified in [main.yml](./defaults/main.yml)

| Name                    | Type | Default                  | Description                                        |
|-------------------------|------|--------------------------|----------------------------------------------------|
| `selinux_commands`      | list | `{{ def_selinux_rule }}` | Rules for nginx                                    |
| `cert_name`             | str  | `nginx`                  | The name that will be assigned to the certificate  |


## Tags

| Tag      | Description                                                   |
|----------|---------------------------------------------------------------|
| install  | installing and creating nginx directories/rules               |
| ssl      | issue OCI cert and install it                                 |
