# `weblogic-domain` Ansible Role

This Ansible role is designed to automate the deployment and configuration of a WebLogic Server (WLS) domain, leveraging the capabilities of the WLST Custom class.

## Overview

The `weblogic-domain` role performs several crucial steps to establish and configure a WebLogic domain:

- Downloads necessary artifacts.
- Adjusts hostname settings.
- Executes the domain creation using the `wlst_run` role.
- Starts the newly created domain.
- Configures security directories and settings.
- Optionally, sets up and configures the server for production mode.
- Ensures essential security configurations for domain files.

The role relies on a custom WLST class, which provides a comprehensive set of operations for the WebLogic environment.

## Role Tasks

Here's a brief outline of the tasks performed by the role:

1. **Download artifacts**: Ensures the necessary directories exist and fetches the required artifacts.
2. **Hostname Configuration**: Temporarily adjusts the hostname to `localhost` for domain creation.
3. **Run Domain Creation**: Executes the `wlst_run` role to create the WebLogic domain using the `create_domain` operation.
4. **Revert Hostname**: Reverts the hostname change post domain creation.
5. **Start Domain**: Initiates the new WebLogic domain.
6. **Configure Security**: Establishes the security directory required for encryption purposes.
7. **Run Servers Configuration**: Executes the `wlst_run` role to perform server-specific configurations using the `domain_provision` operation.
8. **Restart for Production Mode**: If the environment is set to `prod` or `uat`, the domain is restarted to enable production mode.
9. **Ensure Security**: Ensures the necessary files are securely configured as per `wl_domain_files_to_secure`.

## Usage

1. Include the `weblogic-domain` role in your playbook or another role.
2. Ensure you've provided the necessary variables, especially those related to domain configuration, WLST jobs, and server details.
3. Execute your playbook.

## Example: Configure WebLogic Domain for Production

To set up the WebLogic domain for a production instance:

1. Ensure the `instance` variable is set to `prod` within the provided configurations.
2. Uncomment (if commented) the restart block for production mode in the `tasks/main.yml` file.
3. Execute the playbook containing the `weblogic-domain` role.

**Note**: Make sure to provide the correct configurations and verify all dependencies (like `wlst_run` role) are available before running the playbook.

## Example: For run with artifacts download

You will need two variables as shown below:
```sh
ansible-playbook -i inventories/ocx02 -l prod_acs_core_app playbooks/weblogic-domain.yml -bv -e download_artifacts=true -e release=R24.20 --ask-vault-password
```

Also in domain configuragion in the applications part two additional variables `release` and `app` should be set like this:
```yaml
applications:
    [
        {
            "name": "ACS-CORE-R24.20",
            "targets": "ACS-CORE",
            "artifact": "/home/weblogic/SV_Patches/R24.20/acs/acs_core-wl14-2.2.25.R-ACS2023.10.1-cb-20240119-6efec8c3.ear",
            "release": "R24.20",
            "app": "acs",
        },
    ]
```
