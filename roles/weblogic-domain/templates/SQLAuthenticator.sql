-- Copyright (c) 1996, 2009, Oracle and/or its affiliates. All rights reserved.


-- *** Empty all records from table USERS/GROUPS/GROUPMEMBERS ***
DELETE FROM GROUPS;
DELETE FROM GROUPMEMBERS;
DELETE FROM USERS;

-- SQLAuthenticator initial groups
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('AdminChannelUsers', 'AdminChannelUsers can access the admin channel.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('Administrators',    'Administrators can view and modify all resource attributes and start and stop servers.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('AppTesters',        'AppTesters group.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('CrossDomainConnectors',     'CrossDomainConnectors can make inter-domain calls from foreign domains.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('Deployers', 'Deployers can view all resource attributes and deploy applications.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('Monitors',  'Monitors can view and modify all resource attributes and perform operations not restricted by roles.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('Operators', 'Operators can view and modify all resource attributes and perform server lifecycle operations.')
;
INSERT INTO GROUPS (G_NAME, G_DESCRIPTION) values ('OracleSystemGroup', 'Oracle application software system group.')
;

-- SQLAuthenticator initial users
INSERT INTO USERS (U_NAME, U_PASSWORD, U_DESCRIPTION) values ('OracleSystemUser', '{SHA-1}ZrTPS9wN1VMTM1vyjRF5fL8NVqE=', 'Oracle application software system user.')
;
INSERT INTO USERS (U_NAME, U_PASSWORD, U_DESCRIPTION) values ('weblogic', '{SHA-1}ZrTPS9wN1VMTM1vyjRF5fL8NVqE=', 'This user is the default administrator.')
;

-- SQLAuthenticator initial group memberships
INSERT INTO GROUPMEMBERS (G_NAME, G_MEMBER) values ('OracleSystemGroup', 'OracleSystemUser')
;
INSERT INTO GROUPMEMBERS (G_NAME, G_MEMBER) values ('Administrators', 'weblogic')
;
