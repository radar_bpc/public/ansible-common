# Ansible role for install SV Installer

## Command
```shell
ansible-playbook -i inventories/oci02 -l c02t1-wlsa01 playbooks/sv_installer.yml --vault-password-file vault.file -v
```
## Additional vars

user - Linux user for install, defaualt weblogic
v=develop - Install develop version from develop/installer.zip

### Example
```shell
ansible-playbook -i inventories/oci02 -l prod_gate_app playbooks/sv_installer.yml --vault-password-file vault.file -v -e "user=ansible,v=develop"
```
