#!/bin/bash

# create topics
./kafka-topics.sh --zookeeper $1 --create --topic dpc1xmlgate1 --partitions 1 --replication-factor #NODE_COUNT#
./kafka-topics.sh --zookeeper $1 --create --topic dpc1xmlgate2 --partitions 1 --replication-factor #NODE_COUNT#
./kafka-topics.sh --zookeeper $1 --create --topic dpc2xmlgate1 --partitions 1 --replication-factor #NODE_COUNT#
./kafka-topics.sh --zookeeper $1 --create --topic dpc2xmlgate2 --partitions 1 --replication-factor #NODE_COUNT#


