#!/bin/bash

# prepare structure
./zookeeper-shell.sh $1 create /fraud fraud

#  lists
./zookeeper-shell.sh $1 create /fraud/lists lists
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList notBackupToOtherDPCTableList
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/T_WEB_APPLICATION_PARAMETERS T_WEB_APPLICATION_PARAMETERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/T_WEB_QRTZ_JOB_DETAILS T_WEB_QRTZ_JOB_DETAILS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/T_WEB_QRTZ_SIMPROP_TRIGGERS T_WEB_QRTZ_SIMPROP_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/T_WEB_QRTZ_SIMPLE_TRIGGERS T_WEB_QRTZ_SIMPLE_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/T_WEB_QRTZ_TRIGGERS T_WEB_QRTZ_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/TC_QRTZ_SIMPROP_TRIGGERS TC_QRTZ_SIMPROP_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/TC_QRTZ_TRIGGERS TC_QRTZ_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToOtherDPCTableList/TC_QRTZ_JOB_DETAILS TC_QRTZ_JOB_DETAILS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList notBackupToAnyDbTableList
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_BLOB_TRIGGERS T_WEB_QRTZ_BLOB_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_CALENDARS T_WEB_QRTZ_CALENDARS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_CRON_TRIGGERS T_WEB_QRTZ_CRON_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_FIRED_TRIGGERS T_WEB_QRTZ_FIRED_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_LOCKS T_WEB_QRTZ_LOCKS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_PAUSED_TRIGGER_GRPS T_WEB_QRTZ_PAUSED_TRIGGER_GRPS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/T_WEB_QRTZ_SCHEDULER_STATE T_WEB_QRTZ_SCHEDULER_STATE
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_BLOB_TRIGGERS TC_QRTZ_BLOB_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_CALENDARS TC_QRTZ_CALENDARS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_CRON_TRIGGERS TC_QRTZ_CRON_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_FIRED_TRIGGERS TC_QRTZ_FIRED_TRIGGERS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_LOCKS TC_QRTZ_LOCKS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_PAUSED_TRIGGER_GRPS TC_QRTZ_PAUSED_TRIGGER_GRPS
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_SCHEDULER_STATE TC_QRTZ_SCHEDULER_STATE
./zookeeper-shell.sh $1 create /fraud/lists/notBackupToAnyDbTableList/TC_QRTZ_SIMPLE_TRIGGERS TC_QRTZ_SIMPLE_TRIGGERS

./zookeeper-shell.sh $1 create /fraud/sender sender
./zookeeper-shell.sh $1 create /fraud/sender/properties properties
./zookeeper-shell.sh $1 create /fraud/sender/properties/bootstrap.servers ""
./zookeeper-shell.sh $1 create /fraud/sender/properties/acks "all"
./zookeeper-shell.sh $1 create /fraud/sender/properties/retries 0
./zookeeper-shell.sh $1 create /fraud/sender/properties/batch.size 16384
./zookeeper-shell.sh $1 create /fraud/sender/properties/linger.ms 1000
./zookeeper-shell.sh $1 create /fraud/sender/properties/buffer.memory 33554432
./zookeeper-shell.sh $1 create /fraud/sender/properties/max.request.size 10485760
./zookeeper-shell.sh $1 create /fraud/sender/properties/key.serializer "org.apache.kafka.common.serialization.StringSerializer"
./zookeeper-shell.sh $1 create /fraud/sender/properties/value.serializer "org.apache.kafka.common.serialization.ByteArraySerializer"
./zookeeper-shell.sh $1 create /fraud/sender/queueList queueList
./zookeeper-shell.sh $1 create /fraud/sender/queueList/dpc1xmlgate1 ""
./zookeeper-shell.sh $1 create /fraud/sender/queueList/dpc1xmlgate2 ""
./zookeeper-shell.sh $1 create /fraud/sender/queueList/dpc2xmlgate1 ""
./zookeeper-shell.sh $1 create /fraud/sender/queueList/dpc2xmlgate2 ""

./zookeeper-shell.sh $1 create /fraud/receivers receivers
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1 dpc1
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/dataSourceQueueMapping dataSourceQueueMapping
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/dataSourceQueueMapping/xmlgate1 ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/dataSourceQueueMapping/xmlgate2 ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties properties
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/bootstrap.servers ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/enable.auto.commit false
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/auto.offset.reset "latest"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/session.timeout.ms 30000
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/key.deserializer "org.apache.kafka.common.serialization.StringDeserializer"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/value.deserializer "org.apache.kafka.common.serialization.ByteArrayDeserializer"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc1/properties/max.partition.fetch.bytes 10485760
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2 dpc2
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/dataSourceQueueMapping dataSourceQueueMapping
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/dataSourceQueueMapping/xmlgate1 ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/dataSourceQueueMapping/xmlgate2 ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties properties
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/bootstrap.servers ""
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/enable.auto.commit false
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/auto.offset.reset "latest"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/session.timeout.ms 30000
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/key.deserializer "org.apache.kafka.common.serialization.StringDeserializer"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/value.deserializer "org.apache.kafka.common.serialization.ByteArrayDeserializer"
./zookeeper-shell.sh $1 create /fraud/receivers/dpc2/properties/max.partition.fetch.bytes 10485760

./zookeeper-shell.sh $1 create /fraud/dataSources dataSources

#  datasource 1
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1 xmlgate1
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/type "javax.sql.DataSource"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/factory "org.apache.commons.dbcp.BasicDataSourceFactory"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/username "FRAUD"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/password "FRAUD"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/driverClassName ""
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/url ""
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/initialSize "3"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/maxActive "50"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/maxIdle "50"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/minIdle "3"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/suspectTimeout "60"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/timeBetweenEvictionRunsMillis "30000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/minEvictableIdleTimeMillis "60000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/testOnBorrow "true"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/validationQuery "SELECT 1 from dual"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/validationInterval "30000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/removeAbandoned "true"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/core core
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/core/removeAbandonedTimeout "30"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/ui ui
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/ui/removeAbandonedTimeout "30"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/available true
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/enabled true
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/serviceDataSource serviceDataSource
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/serviceDataSource/maxActive "2"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/serviceDataSource/maxIdle "2"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate1/serviceDataSource/minIdle "1"

#  datasource 2
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2 xmlgate2
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/type "javax.sql.DataSource"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/factory "org.apache.commons.dbcp.BasicDataSourceFactory"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/username "FRAUD"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/password "FRAUD"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/driverClassName ""
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/url ""
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/initialSize "3"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/maxActive "50"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/maxIdle "50"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/minIdle "3"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/suspectTimeout "60"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/timeBetweenEvictionRunsMillis "30000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/minEvictableIdleTimeMillis "60000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/testOnBorrow "true"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/validationQuery "SELECT 1 from dual"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/validationInterval "30000"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/removeAbandoned "true"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/removeAbandonedTimeout "30"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/core core
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/ui ui
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/available true
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/enabled true
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/serviceDataSource serviceDataSource
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/serviceDataSource/maxActive "2"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/serviceDataSource/maxIdle "2"
./zookeeper-shell.sh $1 create /fraud/dataSources/xmlgate2/serviceDataSource/minIdle "1"

./zookeeper-shell.sh $1 create /fraud/primaryDataSourceName xmlgate1
