#!/bin/bash

FRAUD_HOME="/home/tomcat"
KAFKA_HOME="$FRAUD_HOME/kafka"
KAFKA_DATA_DIR="$FRAUD_HOME/data/kafka"
ZOOKEEPER_DATA_DIR="$FRAUD_HOME/data/zookeeper"
KAFKA_ZOOKEEPER_CONNECTION_STRING="zookeeper_host1:2181,zookeeper_host2:2181"
UFM_HOME="$FRAUD_HOME/ufm_home"
UFM_CONF="$UFM_HOME/conf"

# Node ID
read -p "Enter Node ID (1-255): " NODE_ID
read -p "Enter count of nodes: " NODE_COUNT

case "$NODE_ID" in
  [0-9]*)
    echo "Using Node ID $NODE_ID"
  ;;
  *)
    echo "Incorrect Node ID"
    exit 1
  ;;
esac

echo "Creating bin directory"
mkdir -p "$FRAUD_HOME/bin"

echo "Creating data/zookeeper directory"
mkdir -p "$FRAUD_HOME/data/zookeeper"

echo "Creating data/kafka directory"
mkdir -p "$FRAUD_HOME/data/kafka"

cp "$UFM_CONF/fault_tolerance/zookeeper.properties" "$UFM_CONF/fault_tolerance/zookeeper.properties_inst"
sed -e "s|#zookeeper_data_dir#|${ZOOKEEPER_DATA_DIR}|g" "$UFM_CONF/fault_tolerance/zookeeper.properties_inst" > "$UFM_CONF/zookeeper.properties"
rm "$UFM_CONF/fault_tolerance/zookeeper.properties_inst"

cp "$UFM_CONF/fault_tolerance/kafka.properties" "$UFM_CONF/fault_tolerance/kafka.properties_inst"
sed -e "s|#kafka_zookeeper_connection_string#|${KAFKA_ZOOKEEPER_CONNECTION_STRING}|g; s|#kafka_data_dir#|${KAFKA_DATA_DIR}|g; s|#id#|${NODE_ID}|g" "$UFM_CONF/fault_tolerance/kafka.properties_inst" > "$UFM_CONF/kafka.properties"
rm "$UFM_CONF/fault_tolerance/kafka.properties_inst"

echo $NODE_ID > "$ZOOKEEPER_DATA_DIR/myid"

echo "LOG_DIR=$UFM_HOME/logs/kafka" > "$FRAUD_HOME/bin/zookeeper_start.sh"
echo "export LOG_DIR" >> "$FRAUD_HOME/bin/zookeeper_start.sh"
echo "$KAFKA_HOME/bin/zookeeper-server-start.sh $UFM_CONF/zookeeper.properties &" >> "$FRAUD_HOME/bin/zookeeper_start.sh"
chmod +x "$FRAUD_HOME/bin/zookeeper_start.sh"

echo "$KAFKA_HOME/bin/zookeeper-server-stop.sh" > "$FRAUD_HOME/bin/zookeeper_stop.sh"
chmod +x "$FRAUD_HOME/bin/zookeeper_stop.sh"

echo "LOG_DIR=$UFM_HOME/logs/kafka" > "$FRAUD_HOME/bin/kafka_start.sh"
echo "export LOG_DIR" >> "$FRAUD_HOME/bin/kafka_start.sh"
echo "$KAFKA_HOME/bin/kafka-server-start.sh $UFM_CONF/kafka.properties &" >> "$FRAUD_HOME/bin/kafka_start.sh"
chmod +x "$FRAUD_HOME/bin/kafka_start.sh"

echo "$KAFKA_HOME/bin/kafka-server-stop.sh" > "$FRAUD_HOME/bin/kafka_stop.sh"
chmod +x "$FRAUD_HOME/bin/kafka_stop.sh"

cp $UFM_CONF/fault_tolerance/init_zookeeper.sh $KAFKA_HOME/bin
chmod +x "$KAFKA_HOME/bin/init_zookeeper.sh"

sed -e "s|#NODE_COUNT#|${NODE_COUNT}|g; s|\r$||g" "$UFM_CONF/fault_tolerance/init_kafka.sh" > "$KAFKA_HOME/bin/init_kafka.sh"
chmod +x "$KAFKA_HOME/bin/init_kafka.sh"

mv "$KAFKA_HOME/config/log4j.properties" "$KAFKA_HOME/config/log4j.properties_inst"
sed -e "s|log4j.rootLogger=INFO, stdout, kafkaAppender|log4j.rootLogger=INFO, kafkaAppender|" "$KAFKA_HOME/config/log4j.properties_inst" > "$KAFKA_HOME/config/log4j.properties"

cp "$UFM_CONF/fault_tolerance/backup_cluster.xml" "$UFM_CONF/backup_cluster.xml"
cp "$UFM_CONF/fault_tolerance/jgroups-relay2-8.xml" "$UFM_CONF/jgroups-relay2-8.xml"
cp "$UFM_CONF/fault_tolerance/jgroups.xml" "$UFM_CONF/jgroups.xml"
cp "$UFM_CONF/fault_tolerance/relay2-8.xml" "$UFM_CONF/relay2-8.xml"

echo "-----USAGE------"
echo "To run zookeeper server use $FRAUD_HOME/bin/zookeeper_start.sh"
echo "To stop zookeeper use $FRAUD_HOME/bin/zookeeper_stop.sh"
echo "To run kafka server use $FRAUD_HOME/bin/kafka_start.sh"
echo "To stop kafka server use $FRAUD_HOME/bin/kafka_stop.sh"

exit 0