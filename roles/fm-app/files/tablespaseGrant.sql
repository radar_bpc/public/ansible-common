alter user FRAUD quota unlimited on FRAUD_TABLES;
--grant select, insert, update, delete on FRAUD_TABLES to FRAUD;

alter user FRAUD quota unlimited on FRAUD_IDX;
--grant select, insert, update, delete on FRAUD_IDX to FRAUD;

alter user FRAUD quota unlimited on FRAUD_TRANSACTIONS;
--grant select, insert, update, delete on FRAUD_TRANSACTIONS to FRAUD;

alter user FRAUD quota unlimited on OTHERS;
--grant select, insert, update, delete on OTHERS to FRAUD;

alter user FRAUD quota unlimited on FRAUDM_LIMIT;
--grant select, insert, update, delete on TEMP to FRAUD;
