set serveroutput on

declare
    type audit_data_id is record (
        action_id number,
        param_id number,
        param_value varchar(256 byte)
    );

    type audit_data_table is table of audit_data_id;
    param_ids_tab audit_data_table;
    param_id_rec audit_data_id;

    limit_count number:=5000;
    current_count number:=0;

    tz_date timestamp with time zone;
    dt date;
    tm number(15, 0);

    checkChangeset number (1,0);
begin
    execute immediate 'ALTER SESSION SET NLS_LANGUAGE=AMERICAN';

    DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Start ');

    select count(*) into checkChangeset from databasechangelog where (id = '2021-09-01_13:01:00' or id = '2021-09-01_13:02:00') and author = 'Alex Eysner' and exectype='EXECUTED';
    if checkChangeset>0 then
        DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Update already done');
    return;
    end if;

    select
        ACTION_ID,
        PARAM_ID,
        param_value
    bulk collect into
        param_ids_tab from t_web_audit_data
    where
        param_id in (
            select
                id
            from
                t_web_audit_param
            where
                locale_object_id in ('web_audit_param_old_rule_date_in', 'web_audit_param_new_rule_date_in', 'web_audit_param_old_rule_date_out', 'web_audit_param_new_rule_date_out')
        );

    DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Need update ' || param_ids_tab.count || ' rows');

    for idx in 1 .. param_ids_tab.count loop
        param_id_rec:=param_ids_tab(idx);
        DBMS_OUTPUT.put_line(to_char(param_id_rec.param_value));
        tz_date:=to_timestamp_tz(param_id_rec.param_value, 'DY MON DD HH24:MI:SS TZD YYYY');
        dt:=cast(tz_date at time zone 'GMT' as date);
        tm := (dt - to_date('01-JAN-1970', 'DD-MM-YYYY'))*86400000;

        update t_web_audit_data set param_value=tm where action_id=param_id_rec.action_id and param_id = param_id_rec.param_id;

        current_count:=current_count+1;
        if current_count = limit_count then
            commit;
            DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Commited ' || current_count || ' changes');
        end if;
    end loop;

    insert into
        databasechangelog (
            ID,
            AUTHOR,
            FILENAME,
            DATEEXECUTED,
            ORDEREXECUTED,
            EXECTYPE,
            DESCRIPTION,
            LIQUIBASE
        ) values (
            '2021-09-01_13:02:00',
            'Alex Eysner',
            'liquibase/patch/db.changelog-6.35.0.xml',
            localtimestamp,
            (select max(ORDEREXECUTED) +1 from databasechangelog),
            'EXECUTED',
            'FM-14606. Executed from script',
            '3.5.3');
    commit;


    DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Commited ' || current_count || ' changes');
    DBMS_OUTPUT.put_line(to_char(sysdate, 'HH24:MI:SS DD.MM.YYYY') || ': Updating issue FM-14606: Done');
end;
/
