SHOW PDBS;
PROMPT
PROMPT specify PDB to create teblespaces:
PROMPT
ALTER SESSION SET CONTAINER = &Container;

alter profile default limit password_life_time unlimited;
CREATE USER FRAUD
IDENTIFIED BY "xxxxxxxxx"
DEFAULT TABLESPACE OTHERS
TEMPORARY TABLESPACE TEMP
PROFILE DEFAULT
ACCOUNT UNLOCK;
GRANT CREATE SESSION TO FRAUD;
GRANT SELECT ANY SEQUENCE TO FRAUD;
GRANT INSERT ANY TABLE TO FRAUD;
GRANT LOCK ANY TABLE TO FRAUD;
GRANT EXECUTE ANY PROCEDURE TO FRAUD;
GRANT SELECT ANY TABLE TO FRAUD;
GRANT ALTER ANY PROCEDURE TO FRAUD;
GRANT DROP ANY SYNONYM TO FRAUD;
GRANT DELETE ANY TABLE TO FRAUD;
GRANT CREATE ANY TABLE TO FRAUD;
GRANT DROP ANY TRIGGER TO FRAUD;
GRANT DROP ANY PROCEDURE TO FRAUD;
GRANT ALTER ANY TABLE TO FRAUD;
GRANT CREATE ANY SEQUENCE TO FRAUD;
GRANT CREATE ANY SYNONYM TO FRAUD;
GRANT DROP ANY TABLE TO FRAUD;
GRANT ALTER ANY TRIGGER TO FRAUD;
GRANT CREATE ANY TRIGGER TO FRAUD;
GRANT CREATE ANY PROCEDURE TO FRAUD;
GRANT CREATE ROLE TO FRAUD;
GRANT DROP ANY SEQUENCE TO FRAUD;
GRANT CREATE ANY VIEW TO FRAUD;
GRANT DROP ANY VIEW TO FRAUD;
GRANT SELECT ON SYS.DBA_RECYCLEBIN TO FRAUD;
