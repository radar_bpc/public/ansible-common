## This role does the following:

- Gets the **domain** configuration from project variables (vars_project_all), looks up ["servers"] and loads client certificates from Oracle Object Storage (OOS) into the directory prepared by the first required steps.
- If set by the `config` tag, the role also calls the WLST script and configures the servers that are defined in the **domain**.

Defaults for **domain** see in the weblogic-domain role.

An example of a server description with the parameters required for this role:
```
{
    "server_name": "CBS-GATE",
    "machine_name": "Machine1",
    "listen_port": 7620,
    "listen_ssl_port": 7002,
    "ssl_enabled": True,
    "jvm_args": "-Dcom.bpcbt.cbsgate.home={{ wl_home }}/cbsgate_home -Dcom.bpcbt.cbsgate.profiles=EPAY -Djavax.net.ssl.keyStore={{ wl_home }}/CERTs/CBS-GATE/SEBA/seba-prd-clientauth.p12 -Djavax.net.ssl.keyStoreType=pkcs12 -Djavax.net.ssl.keyStorePassword={{ demo_wl_cl_keystore_password }} -Djavax.net.ssl.trustStore={{ wl_home }}/CERTs/CBS-GATE/SEBA/trust.jks -Djavax.net.ssl.trustStoreType=jks",
    "keystores_type": "DemoIdentityAndDemoTrust",
    "ks_identity_passphrase": "{AES}FI8UyaBWldzxK6JPFlAj23iNE3hj8Tov/U1AWgPRV6I=",
    "ks_trust_passphrase": "{AES}rql5d39VY8BYJQh64sfwVnh5ZWKeHlhXB/ZkbPhMeAk=",
    "ssl_privat_key_passphrase": "{AES}HFJo84P8AdX5IJOhWdEsbUHXb81s+m+rnsLIr0CvV9w=",
    "client_certs": [
    SEBA/cert1,
    SEBA/cert2,
    SEBA/cert3,
    ]
},
```

The `client_certs` key is a list of certificate records (or any files) that need to be downloaded from the OOS and saved to the appropriate path in the file system. This path is `{{ wl_home }}/CERTs/{{ domain.server.serve_rname }}/<list entry>`. Notice the `jvm_args` in this example, they contain the full description of the paths and files the server should apply.

Keep in mind that the configuration applies to all servers in the **domain** description. If you don't want this, you can temporarily comment out the other servers in the configuration that you don't need to change. If the configuration in the **domain** description matches the real **domain**, then you have nothing to worry about.

Parameters from the **domain** configuration that will be configured by this role for each server:
- ClientCertProxyEnabled
- ListenPort (SSL)
- Arguments (jvm_args)

Arguments is a string for the Start Server task containing parameters to start the jvm machine. This is the `jvm_args` setting in the server configuration in the **domain** description.

The actual configuration is done by the WLST script `ansible-common/files/scripts/wlst/config_server_certs.py`.

Usage:
```
ansible-playbook -i inventories/oci02 -l c02t1-ptrk01 playbooks/clients-certs.yml -bK --tags config
```