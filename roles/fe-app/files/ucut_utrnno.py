#!/usr/bin/python

import os
import gzip, shutil
import sys, getopt
import subprocess
from datetime import datetime
import traceback
import tarfile
from datetime import timedelta, datetime

utrnno = ""
directory_mask = ""


archive_current_day_searched = 0
archive_next_day_searched = 0
today_folder_search = 0
tarfileActivated = 0

full_file_or_mask = "*"


def query_files():
    global utrnno
    global directory_mask
    global archive_current_day_searched
    global archive_next_day_searched
    global today_folder_search
    global tarfileActivated
    global full_file_or_mask

    cmd = "zgrep -l " + utrnno + " " + directory_mask
    print("Running command: " + cmd)
    process = subprocess.Popen(
        cmd, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    result_lines = process.stdout.readlines()

    print("Current directory: " + directory_mask)
    if "/home/svfe" in directory_mask:
        today_folder_search = 1
    elif (
        "/appdata/archive/outfiles/20"
        + utrnno[0:2]
        + "/"
        + utrnno[2:4]
        + "/"
        + utrnno[4:6]
        in directory_mask
    ):
        archive_current_day_searched = 1
    else:
        archive_next_day_searched = 1

    if result_lines == []:
        return 0
    print("Found files to ucut:" + str(result_lines))
    for line in result_lines:
        if len(line) >= 1:
            line = line.replace("\n", "")
            file_in = []
            if line.endswith(".tar"):
                try:
                    file_in = tarfile.open(line)

                    file_in.extract_all()
                    file_in.close()
                except:
                    print(
                        "File:"
                        + line
                        + " could not be loaded. Error: "
                        + traceback.format_exc()
                    )
            elif line.endswith(".gz"):
                try:

                    with gzip.open(line, "rb") as f_out:
                        with open(line.replace(".gz", ".tmp"), "wb") as f_in:
                            shutil.copyfileobj(f_out, f_in)
                            file_in = f_in

                except:
                    print(
                        "File:"
                        + line
                        + " could not be loaded. Error: "
                        + traceback.format_exc()
                    )
            else:
                try:
                    file_in = open(line, "r")
                except:
                    print("Cannot open file " + line)
                    e = traceback.format_exc()
                    print("Exception " + e)

            if file_in == []:
                print("Ignoring file: " + line)
                break
            print("Executing file: " + file_in.name)
            ucut_cmd = "ucut " + utrnno + " " + file_in.name

            p1 = subprocess.check_output(ucut_cmd, shell=True)
            print(p1)
            if "0 matches" not in p1:
                ucut_file = file_in.name + "." + utrnno + ".txt"
                cwd = os.getcwd()
                try:
                    mv_cmd = "mv " + ucut_file + " " + cwd
                    subprocess.check_output(mv_cmd, shell=True)

                except:
                    print("Cannot move file " + ucut_cmd)
                    print("Exception e=" + traceback.format_exc())

            rm_cmd = "rm " + file_in.name
            subprocess.check_output(rm_cmd, shell=True)

    print("Archive activated: " + str(tarfileActivated))
    if tarfileActivated == 1:
        file_name_tar = "tar_" + utrnno + ".tar"
        if not (os.path.exists(file_name_tar)):
            tar_cmd = "tar -cvf " + file_name_tar + " *" + utrnno + "*.txt"
            print(tar_cmd)
            subprocess.check_output(tar_cmd, shell=True)
        else:
            print("Tar file already exists: " + file_name_tar)

    return 1


def swap_directories():
    global utrnno
    global directory_mask
    global archive_current_day_searched
    global archive_next_day_searched
    global today_folder_search
    global full_file_or_mask

    if (
        datetime.now().strftime("%y%m%d") == utrnno[0:6]
        and today_folder_search == 0
        and archive_current_day_searched == 1
    ):
        directory_mask = "/home/svfe/output/outfiles/" + full_file_or_mask
        today_folder_search = 1
    elif (
        datetime.now().strftime("%y%m%d") == utrnno[0:6]
        and today_folder_search == 1
        and archive_current_day_searched == 0
    ):
        directory_mask = (
            "/appdata/archive/outfiles/20"
            + utrnno[0:2]
            + "/"
            + utrnno[2:4]
            + "/"
            + utrnno[4:6]
            + "/"
            + full_file_or_mask
        )
        archive_current_day_searched = 1
    elif (
        datetime.now().strftime("%y%m%d") != utrnno[0:6]
        and archive_current_day_searched == 0
    ):
        today_folder_search = 1
        archive_current_day_searched = 1
        directory_mask = (
            "/appdata/archive/outfiles/20"
            + utrnno[0:2]
            + "/"
            + utrnno[2:4]
            + "/"
            + utrnno[4:6]
            + "/"
            + full_file_or_mask
        )
    else:
        try:
            increment_date = str(
                datetime.strftime(
                    datetime.strptime(utrnno[0:6], "%y%m%d") + timedelta(days=1),
                    "%y%m%d",
                )
            )
            directory_mask.replace(
                utrnno[0:2] + "/" + utrnno[2:4] + "/" + utrnno[4:6],
                increment_date[0:2]
                + "/"
                + increment_date[2:4]
                + "/"
                + increment_date[4:6],
            )
        except:
            print("Cannot further increment date")

        today_folder_search = 1
        archive_current_day_searched = 1
        archive_next_day_searched = 1


def main(argv):
    global utrnno
    global directory_mask
    global archive_current_day_searched
    global archive_next_day_searched
    global today_folder_search
    global tarfileActivated
    global full_file_or_mask

    try:
        opts, args = getopt.getopt(
            argv, "hh:u:f:a:", ["help", "utrnno=", "file_mask=", "archive="]
        )
    except getopt.GetoptError:
        print("Invalid options. Type -h for help.")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("Usage script -u <utrnno> -f <file-mask-optional> -a 1")
            print(
                "Usage script --utrnno <utrnno> --file_mask <directory-mask-optional> --archive 1"
            )
            sys.exit()
        elif opt in ("-u", "--utrnno"):
            try:
                utrnno = arg
                directory_mask = (
                    "/appdata/archive/outfiles/20"
                    + utrnno[0:2]
                    + "/"
                    + utrnno[2:4]
                    + "/"
                    + utrnno[4:6]
                    + "/*"
                )

                if datetime.now().strftime("%y%m%d") == utrnno[0:6]:
                    directory_mask = "/home/svfe/output/outfiles/*"
            except:
                print("Error in argument -u or --utrnno")
                sys.exit()
        elif opt in ("-f", "--file_mask"):
            try:
                if arg == "":
                    pass
                elif not (os.path.exists(os.path.dirname(arg))):
                    print("Directory " + arg + " doesn't exist")
                    sys.exit()
                else:
                    directory_mask = arg
                    full_file_or_mask = os.path.basename(arg)

            except:
                e = traceback.format_exc()
                print("Error in directory, set as current directory")
                print("Error in directory argument: Stack:" + e)
                sys.exit()

        elif opt in ("-a", "--archive"):
            tarfileActivated = 1
    if utrnno == "":
        print("Utrnno is not defined. Cannot continue. Type -h for help")
        sys.exit(0)

    print("Currently searching directory: " + directory_mask)
    print("Querying with utrnno: " + utrnno)
    if query_files() == 0:
        if search_all_days() == 1:
            print("Searched all days. Found nothing")
            sys.exit(0)

        print("Nothing found in directory")
        swap_directories()
        print("Currently searching directory: " + directory_mask)

        if query_files() == 0:
            if search_all_days() == 1:
                print("Searched all days. Found nothing")
                sys.exit(0)

            print("Nothing found in directory")
            swap_directories()
            # If results are empty, maybe in current directory
            print("Currently searching directory: " + directory_mask)
            if query_files() == 0:
                if search_all_days() == 1:
                    print("Searched all days. Found nothing")
                    sys.exit(0)
                print("Transaction not found in files. Try manually")


def search_all_days():
    global today_folder_search
    global archive_current_day_searched
    global archive_next_day_searched

    if (
        today_folder_search == 1
        and archive_current_day_searched == 1
        and archive_next_day_searched == 1
    ):
        return 1

    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
