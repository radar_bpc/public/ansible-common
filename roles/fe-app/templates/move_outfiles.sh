#!/usr/bin/env bash

{{ ansible_managed | comment }}

# move_outfiles.sh
# This script scans SRC for files, determines their date from the filename,
# and moves them into DEST directory structure without compressing.
#
# Logging and error handling included.
#
# Result: files are placed into /appdata/archive/outfiles/<year>/<month>/<day>
# but remain uncompressed.
# Version: 1.2

SRC="/home/svfe/output/outfiles"
DEST="/appdata/archive/outfiles"
LOG_FILE="/home/svfe/scripts/logs/move_outfiles.log"
NOW=$(/bin/date '+%Y-%m-%d')
NOW_S=$(/bin/date +%s)

log_info() { echo "$(/bin/date '+%Y-%m-%d %H:%M:%S') INFO $*" | /bin/tee -a "$LOG_FILE"; }
log_warning() { echo "$(/bin/date '+%Y-%m-%d %H:%M:%S') WARNING $*" | /bin/tee -a "$LOG_FILE"; }
log_err() { echo "$(/bin/date '+%Y-%m-%d %H:%M:%S') ERROR $*" | /bin/tee -a "$LOG_FILE" >&2; }

is_svfe_mounted() {
    /bin/grep -q "/home/svfe" /proc/mounts
}

declare -A MONTH_MAP=(
    [Jan]=1
    [Feb]=2
    [Mar]=3
    [Apr]=4
    [May]=5
    [Jun]=6
    [Jul]=7
    [Aug]=8
    [Sep]=9
    [Oct]=10
    [Nov]=11
    [Dec]=12
)

extract_date_from_filename() {
    local filename="$1"
    if [[ "$filename" =~ (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)([0-9]{1,2}) ]]; then
        # Checks re matches by their group number assigned by parensies sequence
        local month_str="${BASH_REMATCH[1]}"
        local day_str="${BASH_REMATCH[2]}"
        local month_num="${MONTH_MAP[$month_str]}"

        local current_year
        IFS='-' read -r current_year _ _ <<< "$NOW"
        local file_epoch
        file_epoch=$(/bin/date -d "$current_year-$month_num-$day_str" +%s)
        # If the extracted date is invalid then skip processing
        if (( $? == 1 )) ; then
            return 0
        fi
        local now
        now=$NOW_S
        if (( file_epoch > now )); then
            current_year=$((current_year-1))
        fi
        printf "%04d-%02d-%02d\n" "$current_year" "$month_num" "$day_str"
    fi
}

ensure_dir_exists() {
    local dir="$1"
    /bin/mkdir -p "$dir" 2>/dev/null || {
        log_err "Could not create directory: $dir"
        return 1
    }
    return 0
}

move_file() {
    local src_path="$1"
    local dest_dir="$2"
    local fname=$(/bin/basename "$1")
    # Always add a timestamp of this run from NOW_S global variable
    /bin/mv "$src_path" "$dest_dir/$fname.$NOW_S" 2>/dev/null || {
        log_err "Failed to move $src_path to $dest_dir"
        return 1
    }
    return 0
}

scan_and_move_files() {
    /bin/find -H "$SRC" -maxdepth 1 -type f ! -name ".*" 2>/dev/null | while read -r f; do
        local fname=$(/bin/basename "$f")
        if ! /usr/sbin/fuser -s "$f"; then
            local date_str=$(extract_date_from_filename "$fname")
            if [[ -z "$date_str" ]]; then
                # No date, move to unknown_date
                local current_year current_month current_day
                IFS='-' read -r current_year current_month current_day <<< "$NOW"
                # If this is a coredump file, move it into its special directory
                if [[ $fname == core* ]]; then
                    local default_dest_dir="$DEST/$current_year/$current_month/$current_day/core_files"
                else
                    local default_dest_dir="$DEST/$current_year/$current_month/$current_day/unknown_date"
                fi
                ensure_dir_exists "$default_dest_dir" || continue
                if move_file "$f" "$default_dest_dir"; then
                    log_warning "Moved file without date $f to $default_dest_dir/$fname.$NOW_S"
                fi
            else
                local year month day
                IFS='-' read -r year month day <<< "$date_str"
                local dest_dir="$DEST/$year/$month/$day"
                ensure_dir_exists "$dest_dir" || continue
                if move_file "$f" "$dest_dir"; then
                    log_info "Moved file $f to $dest_dir/$fname.$NOW_S"
                fi
            fi
        else
            # Skip file silently, or skip with log record if last modification time greater than 24h (43200s)
            if (( $NOW_S-$(stat -c %Y $f) > 43200 )); then
                log_warning "file $fname skipped cause holded by process and last modification time more than 24h"
            fi
        fi
    done
}

run() {
    is_svfe_mounted || { log_err "SVFE is down here. Nothing to do."; exit 255; }
    log_info "==========Starting move_files script=========="
    scan_and_move_files
    log_info "==========Completed move_files script=========="
}

run
