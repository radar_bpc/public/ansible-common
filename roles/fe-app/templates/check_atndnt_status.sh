#!/bin/bash

# Define the lock file
LOCK_FILE="/tmp/pingpro_atndnt.lock"
STATUS_FILE="/tmp/status_atndnt.txt"

# Use flock to ensure single execution
flock -n "$LOCK_FILE" -c "
    # Execute the command and analyze the result
    RESULT=\$({{ svfe_bin_path }}/pingpro atndnt 2>/dev/null)
    # Check for strict match of OK as a standalone word
    if echo "\$RESULT" | grep -qE \"(^|[[:space:]])OK$\"; then
        echo "OK" > "$STATUS_FILE"
    else
        echo "FAIL" > "$STATUS_FILE"
    fi
"

exit 0
