#!/bin/bash

# For tests of outriles_arch.py work
# Script to create test files with date markers and 1MB of random data

# Define the output directory
OUTPUT_DIR="/home/svfe/output/outfiles"

# Create the directory if it doesn't exist
mkdir -p "$OUTPUT_DIR"

# Array of date markers with and without leading zeros
DATE_MARKERS=("Nov1" "Nov01" "Nov2" "Nov02" "Dec1" "Dec01" "Dec2" "Dec02" "Jan1" "Jan01" "Jan2" "Jan02")

# Loop through each date marker and create a file
for DATE in "${DATE_MARKERS[@]}"; do
    FILE_NAME="testfile_${DATE}.txt"
    FILE_PATH="$OUTPUT_DIR/$FILE_NAME"

    # Create a file with 1MB of random data
    head -c 1M </dev/urandom > "$FILE_PATH"

    # Confirm file creation
    if [ -f "$FILE_PATH" ]; then
        echo "Created file: $FILE_PATH"
    else
        echo "Failed to create file: $FILE_PATH"
    fi
done

