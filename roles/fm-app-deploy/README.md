# Deploy Fraud Monitoring

## Run playbook
```shell
ansible-playbook -i inventories/oci02 -l tst_fm_app playbooks/fm-app-deploy.yml -e "release=6.47" -v --ask-vault-password
```
