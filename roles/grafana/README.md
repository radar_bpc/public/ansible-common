## Grafane role

- Install Grafana from OOS .rpm 
- Install plugins from web or from OOS .zip
- Configure datasources
- Configure dashboards

## Dashboards

This role uses jsonnet for store dashboards in the code in human readable format.
So for converting .jsonnet files to json you need to install jsonnet:
```
python3 -m pip install jsonnet
```

or (preferred):
```
go install github.com/google/go-jsonnet/cmd/jsonnet@latest
sudo ln -s $HOME/go/bin/jsonnet /usr/local/go/bin/jsonnet
```

Then you require the following updated library for properly working with zabbix datasources:
```
git clone https://github.com/lvsv/grafonnet-lib
```

Export path of new library to the JSONNET_PATH variable:
```
export JSONNET_PATH=/path/to/grafonnet-lib
```

Now you ready to go!