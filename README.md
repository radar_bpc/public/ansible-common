# Ansible common

## Attach reposotory to project

### Set URL
`git submodule add -b main https://gitlab.com/radar_bpc/public/ansible-common.git`

### Init submodule
`git submodule update`


## Get last update for ansible-common

```bash
git submodule update --remote
git add ansible-common
git commit -m "Update ansible-common"
git push
```


